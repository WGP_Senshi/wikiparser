To-Do-Liste:
High Priority:
- Get maximum range by reading out ObjectTemplate.projectileTemplate -> according projectile file TimeToLive and multiply with velocity.
- Get damage by reading out projectileTemplate as well, then checking the materialmanager file for it.
- Get a checker for ROF if more than one ROF values are available (currently |rof1=VA \n |rof1=SA, should be |rof1=VA SA)


Low Priority:
- Add Save/Open Template functions
- Only have filenames displayed instead of full paths in list for better accessibility

17 June 2015:
- processInclude:
| Need a way to properly hand over original file path when switching between includes/runs and breaking them
| Currently, the origin filename (global var) is overwritten once include launches the new processing flow. This will mess up subsequent includes in the same file. 