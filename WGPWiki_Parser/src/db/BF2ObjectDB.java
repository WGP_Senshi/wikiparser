package db;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

// import com.thoughtworks.xstream.io.xml.StaxDriver;

public class BF2ObjectDB {

	private List<BF2Object> db;

	public BF2ObjectDB(ArrayList<BF2Object> data) {
		this.setDB(data);
	}

	/**
	 * Returns the weapon object from the DB by name.
	 * 
	 * Returns null if name is not found.
	 */
	public BF2Object getByCode(String code) {
		for (int i = 0; i < getDB().size(); i++) {
			if (getDB().get(i).getCode().equalsIgnoreCase(code))
				return getDB().get(i);
		}
		// System.out.println("ERROR: " + code + " not found in " + this.getClass().getSimpleName());
		return null;
	}

	public List<BF2Object> getDB() {
		return db;
	}

	public void setDB(List<BF2Object> db) {
		this.db = db;
	}

	public static BF2ObjectDB readDB() {
		File db_bf2obj = new File("db/bf2objects.xml");

		ArrayList<BF2Object> db = new ArrayList<BF2Object>();

		XStream xstream = new XStream(new DomDriver());
		xstream.alias("bf2object", BF2Object.class);
		xstream.alias("genericfirearm", GenericFireArm.class);
		xstream.alias("engine", Engine.class);
		xstream.alias("genericfirearm", GenericFireArm.class);
		xstream.alias("kit", Kit.class);
		xstream.alias("landinggear", LandingGear.class);
		xstream.alias("playercontrolobject", PlayerControlObject.class);
		xstream.alias("rotationalbundle", RotationalBundle.class);
		xstream.alias("projectile", Projectile.class);

		try {
			BufferedReader fr = new BufferedReader(new FileReader(db_bf2obj));
			ObjectInputStream in = xstream.createObjectInputStream(fr);
			while (true) {
				BF2Object bf2obj = (BF2Object) in.readObject();
				// System.out.println(projectile.getName() + " --- " + projectile.getDmg());
				db.add(bf2obj);
			}
		} catch (EOFException e) {
			// System.out.println("End of projectiles DB reached.");
		} catch (FileNotFoundException e) {
			System.err.println("WARNING: DB BF2Objects does not exist! Please build it in the SETTINGS tab or stuff WILL NOT WORK!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("ERROR: An XML object in the BF2 database is not formatted properly.");
			e.printStackTrace();
		} catch (com.thoughtworks.xstream.io.StreamException e) {
			System.err.println("ERROR: The BF2Objects database is corrupted. Rebuilding from scratch.");
			db_bf2obj.delete();
			readDB();
		}

		return new BF2ObjectDB(db);
	}
}
