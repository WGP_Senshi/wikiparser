package db;

import java.util.ArrayList;
import java.util.List;

public class Vehicle {

	private String name;
	private String code;
	private int crew = 0;
	private int passengers = 0;
	private int hitpoints = 0;
	private int hitpoints_crit = 0;
	private int spinup = 0;
	private String extras;
	private String speed;
	private int tickets = 0;
	private List<String> maps;
	private List<BF2Object> subtemplates= new ArrayList<BF2Object>();
	private String type;

	public Vehicle(String code) {
		this.setCode(code);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getCrew() {
		return crew;
	}

	public void setCrew(int crew) {
		this.crew = crew;
	}

	public int getPassengers() {
		return passengers;
	}

	public void setPassengers(int passengers) {
		this.passengers = passengers;
	}

	public int getHitpoints() {
		return hitpoints;
	}

	public void setHitpoints(int hitpoints) {
		this.hitpoints = hitpoints;
	}

	public int getHitpoints_crit() {
		return hitpoints_crit;
	}

	public void setHitpoints_crit(int hitpoints_crit) {
		this.hitpoints_crit = hitpoints_crit;
	}

	public int getSpinup() {
		return spinup;
	}

	public void setSpinup(int spinup) {
		this.spinup = spinup;
	}

	public String getExtras() {
		return extras;
	}

	public void setExtras(String extras) {
		this.extras = extras;
	}

	public int getTickets() {
		return tickets;
	}

	public void setTickets(int tickets) {
		this.tickets = tickets;
	}

	public String getSpeed() {
		return speed;
	}

	public void setSpeed(String speed) {
		this.speed = speed;
	}

	public List<String> getMaps() {
		return maps;
	}

	public void setMaps(List<String> maps) {
		this.maps = maps;
	}

	
	public List<BF2Object> getSubTemplates() {
		return subtemplates;
	}

	public void setSubTemplates(List<BF2Object> newSubTemplates) {
		subtemplates = newSubTemplates;
	}

	public void addTemplate(BF2Object string) {
		subtemplates.add(string);

	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void addSubTemplate(BF2Object subTemplate) {
		this.subtemplates.add(subTemplate);

	}

}
