package db;

import java.util.ArrayList;
import java.util.List;

public class Projectile implements BF2Object {

	private String code;
	private int expl_mat;
	private float expl_radius;
	private float expl_force;
	private float expl_dmg;
	private int expl_maxdepth;
	private float grav_mod;
	private int mat;
	private float TTL;
	private String tracer_name;
	private int tracer_interval;
	private float dmg_min;
	private float dmg;
	private int disttostartlosedmg;
	private int disttomindmg;
	private List<String> subtemplates;

	public Projectile(String code) {
		this.setCode(code);
	    this.subtemplates = new ArrayList<String>();
	}

	public int getExpl_mat() {
		return expl_mat;
	}

	public void setExpl_mat(int expl_mat) {
		this.expl_mat = expl_mat;
	}

	public float getExpl_radius() {
		return expl_radius;
	}

	public void setExpl_radius(float expl_radius) {
		this.expl_radius = expl_radius;
	}

	public float getExpl_force() {
		return expl_force;
	}

	public void setExpl_force(float expl_force) {
		this.expl_force = expl_force;
	}

	public float getExpl_dmg() {
		return expl_dmg;
	}

	public void setExpl_dmg(float expl_dmg) {
		this.expl_dmg = expl_dmg;
	}

	public int getExpl_maxdepth() {
		return expl_maxdepth;
	}

	public void setExpl_maxdepth(int expl_maxdepth) {
		this.expl_maxdepth = expl_maxdepth;
	}

	@Override
	public String getCode() {
		return code;
	}

	public void setCode(String name) {
		this.code = name;
	}

	public float getGrav_mod() {
		return grav_mod;
	}

	public void setGrav_mod(float grav_mod) {
		this.grav_mod = grav_mod;
	}

	public int getMat() {
		return mat;
	}

	public void setMat(int mat) {
		this.mat = mat;
	}

	public float getTTL() {
		return TTL;
	}

	public void setTTL(float tTL) {
		TTL = tTL;
	}

	public int getTracer_interval() {
		return tracer_interval;
	}

	public void setTracer_interval(int tracer_interval) {
		this.tracer_interval = tracer_interval;
	}

	public String getTracer_name() {
		return tracer_name;
	}

	public void setTracer_name(String tracer_name) {
		this.tracer_name = tracer_name;
	}

	public float getDmg_min() {
		return dmg_min;
	}

	public void setDmg_min(float dmg_min) {
		this.dmg_min = dmg_min;
	}

	public float getDmg() {
		return dmg;
	}

	public void setDmg(float dmg) {
		this.dmg = dmg;
	}

	public int getDisttostartlosedmg() {
		return disttostartlosedmg;
	}

	public void setDisttostartlosedmg(int disttostartlosedmg) {
		this.disttostartlosedmg = disttostartlosedmg;
	}

	public int getDisttomindmg() {
		return disttomindmg;
	}

	public void setDisttomindmg(int disttomindmg) {
		this.disttomindmg = disttomindmg;
	}

	@Override
	public List<String> getSubTemplates() {
		return this.subtemplates;
	}

	@Override
	public void setSubTemplates(List<String> newsubs) {
		this.subtemplates = newsubs;

	}

	@Override
	public void addSubTemplate(String subTemplate) {
		this.subtemplates.add(subTemplate);
	}
}
