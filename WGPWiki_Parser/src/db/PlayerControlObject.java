package db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlayerControlObject implements BF2Object {

    String code;
    private List<String> subtemplates = new ArrayList<String>();
    private HashMap<Integer,String> materials;
    
    
    private boolean castsdynamicshadow;
    // Armor settings
    private int maxhitpoints;
    private int hitpoints;
    private int defaultmaterial;
    private float hplostwhileupsidedown;
    private float hplostwhileinwater;
    private float hplostwhileindeepwater;
    private float hplostwhilecriticaldamage;
    private float waterdamagedelay;
    private float deepwaterdamagedelay;
    private float waterlevel;
    private float deepwaterlevel;
    private float explosionforce;
    private float explosionforcemax;
    private float explosiondamage;
    private float explosionradius;
    private int explosionmaterial;
    private float wreckexplosionforce;
    private float wreckexplosionforcemax;
    private float wreckexplosiondamage;
    private float wreckexplosionradius;
    private int wreckexplosionmaterial;
    private float wreckhitpoints;
    private float timetostayaswreck;
    private float criticaldamage;
    
    // Vehicle HUD
    private String hudName;
    private String typeicon;
    private String minimapicon;
    private String spottedicon;
    private String minimapiconleadersize;
    private int guiIndex;
    private float[] vehicleiconpos;
    private boolean useselectionicons;
    private String vehicleicon;
    
    // Physics
    private float drag;
    private float mass;
    private float gravitymodifier;
    private float floaterMod;
    private boolean hasmobilephysics;
    private boolean hascollisionphysics;
    private String physicstype;
    private float exitspeedmod;
    private String vehicletype;
    private float sprintRecoverTime;
    private float sprintDissipationTime;
    private float sprintLimit;
    private float sprintFactor;
    
    private float listenerobstruction;
    private float groundcontactvolume;
    private float damagedambientsoundlimit;
    private boolean hasrestrictedexit;
    
    
    public PlayerControlObject() {
    	System.exit(1);
    }

    public PlayerControlObject(String code) {
        this.code = code;
    }
    
    @Override
	public List<String> getSubTemplates() {
        return subtemplates;
    }
    @Override
	public void setSubTemplates(List<String> subTemplates) {
        this.subtemplates = subTemplates;
    }
    
    @Override
	public void addSubTemplate(String subTemplate) {
        this.subtemplates.add(subTemplate);
    }
    public void getAddTemplate(int i) {
        this.subtemplates.get(i);
    }

    public String getMaterial(int key) {
        return materials.get(key);
    }
    public void setMaterial(int key, String value) {
        this.materials.put(key, value);
    }
    public HashMap<Integer, String> getMaterials() {
        return materials;
    }

    @Override
    public String getCode() {
        return this.code;
    }

	public boolean isHascollisionphysics() {
		return hascollisionphysics;
	}

	public void setHascollisionphysics(boolean hascollisionphysics) {
		this.hascollisionphysics = hascollisionphysics;
	}

	public boolean isCastsdynamicshadow() {
		return castsdynamicshadow;
	}

	public void setCastsdynamicshadow(boolean castsdynamicshadow) {
		this.castsdynamicshadow = castsdynamicshadow;
	}

	public int getMaxhitpoints() {
		return maxhitpoints;
	}

	public void setMaxhitpoints(int maxhitpoints) {
		this.maxhitpoints = maxhitpoints;
	}

	public int getHitpoints() {
		return hitpoints;
	}

	public void setHitpoints(int hitpoints) {
		this.hitpoints = hitpoints;
	}

	public int getDefaultmaterial() {
		return defaultmaterial;
	}

	public void setDefaultmaterial(int defaultmaterial) {
		this.defaultmaterial = defaultmaterial;
	}

	public float getHplostwhileupsidedown() {
		return hplostwhileupsidedown;
	}

	public void setHplostwhileupsidedown(float hplostwhileupsidedown) {
		this.hplostwhileupsidedown = hplostwhileupsidedown;
	}

	public float getHplostwhileinwater() {
		return hplostwhileinwater;
	}

	public void setHplostwhileinwater(float hplostwhileinwater) {
		this.hplostwhileinwater = hplostwhileinwater;
	}

	public float getHplostwhileindeepwater() {
		return hplostwhileindeepwater;
	}

	public void setHplostwhileindeepwater(float hplostwhileindeepwater) {
		this.hplostwhileindeepwater = hplostwhileindeepwater;
	}

	public float getHplostwhilecriticaldamage() {
		return hplostwhilecriticaldamage;
	}

	public void setHplostwhilecriticaldamage(float hplostwhilecriticaldamage) {
		this.hplostwhilecriticaldamage = hplostwhilecriticaldamage;
	}

	public float getWaterdamagedelay() {
		return waterdamagedelay;
	}

	public void setWaterdamagedelay(float waterdamagedelay) {
		this.waterdamagedelay = waterdamagedelay;
	}

	public float getDeepwaterdamagedelay() {
		return deepwaterdamagedelay;
	}

	public void setDeepwaterdamagedelay(float deepwaterdamagedelay) {
		this.deepwaterdamagedelay = deepwaterdamagedelay;
	}

	public float getWaterlevel() {
		return waterlevel;
	}

	public void setWaterlevel(float waterlevel) {
		this.waterlevel = waterlevel;
	}

	public float getDeepwaterlevel() {
		return deepwaterlevel;
	}

	public void setDeepwaterlevel(float deepwaterlevel) {
		this.deepwaterlevel = deepwaterlevel;
	}

	public float getExplosionforce() {
		return explosionforce;
	}

	public void setExplosionforce(float explosionforce) {
		this.explosionforce = explosionforce;
	}

	public float getExplosionforcemax() {
		return explosionforcemax;
	}

	public void setExplosionforcemax(float explosionforcemax) {
		this.explosionforcemax = explosionforcemax;
	}

	public float getExplosiondamage() {
		return explosiondamage;
	}

	public void setExplosiondamage(float explosiondamage) {
		this.explosiondamage = explosiondamage;
	}

	public float getExplosionradius() {
		return explosionradius;
	}

	public void setExplosionradius(float explosionradius) {
		this.explosionradius = explosionradius;
	}

	public int getExplosionmaterial() {
		return explosionmaterial;
	}

	public void setExplosionmaterial(int explosionmaterial) {
		this.explosionmaterial = explosionmaterial;
	}

	public float getWreckexplosionforce() {
		return wreckexplosionforce;
	}

	public void setWreckexplosionforce(float wreckexplosionforce) {
		this.wreckexplosionforce = wreckexplosionforce;
	}

	public float getWreckexplosionforcemax() {
		return wreckexplosionforcemax;
	}

	public void setWreckexplosionforcemax(float wreckexplosionforcemax) {
		this.wreckexplosionforcemax = wreckexplosionforcemax;
	}

	public float getWreckexplosiondamage() {
		return wreckexplosiondamage;
	}

	public void setWreckexplosiondamage(float wreckexplosiondamage) {
		this.wreckexplosiondamage = wreckexplosiondamage;
	}

	public float getWreckexplosionradius() {
		return wreckexplosionradius;
	}

	public void setWreckexplosionradius(float wreckexplosionradius) {
		this.wreckexplosionradius = wreckexplosionradius;
	}

	public int getWreckexplosionmaterial() {
		return wreckexplosionmaterial;
	}

	public void setWreckexplosionmaterial(int wreckexplosionmaterial) {
		this.wreckexplosionmaterial = wreckexplosionmaterial;
	}

	public float getWreckhitpoints() {
		return wreckhitpoints;
	}

	public void setWreckhitpoints(float wreckhitpoints) {
		this.wreckhitpoints = wreckhitpoints;
	}

	public float getTimetostayaswreck() {
		return timetostayaswreck;
	}

	public void setTimetostayaswreck(float timetostayaswreck) {
		this.timetostayaswreck = timetostayaswreck;
	}

	public float getCriticaldamage() {
		return criticaldamage;
	}

	public void setCriticaldamage(float criticaldamage) {
		this.criticaldamage = criticaldamage;
	}

	public String getHudName() {
		return hudName;
	}

	public void setHudName(String hudName) {
		this.hudName = hudName;
	}

	public String getTypeicon() {
		return typeicon;
	}

	public void setTypeicon(String typeicon) {
		this.typeicon = typeicon;
	}

	public String getMinimapicon() {
		return minimapicon;
	}

	public void setMinimapicon(String minimapicon) {
		this.minimapicon = minimapicon;
	}

	public String getSpottedicon() {
		return spottedicon;
	}

	public void setSpottedicon(String spottedicon) {
		this.spottedicon = spottedicon;
	}

	public String getMinimapiconleadersize() {
		return minimapiconleadersize;
	}

	public void setMinimapiconleadersize(String minimapiconleadersize) {
		this.minimapiconleadersize = minimapiconleadersize;
	}

	public int getGuiIndex() {
		return guiIndex;
	}

	public void setGuiIndex(int guiIndex) {
		this.guiIndex = guiIndex;
	}

	public float[] getVehicleiconpos() {
		return vehicleiconpos;
	}

	public void setVehicleiconpos(float[] vehicleiconpos) {
		this.vehicleiconpos = vehicleiconpos;
	}

	public boolean isUseselectionicons() {
		return useselectionicons;
	}

	public void setUseselectionicons(boolean useselectionicons) {
		this.useselectionicons = useselectionicons;
	}

	public String getVehicleicon() {
		return vehicleicon;
	}

	public void setVehicleicon(String vehicleicon) {
		this.vehicleicon = vehicleicon;
	}

	public float getDrag() {
		return drag;
	}

	public void setDrag(float drag) {
		this.drag = drag;
	}

	public float getMass() {
		return mass;
	}

	public void setMass(float mass) {
		this.mass = mass;
	}

	public float getGravitymodifier() {
		return gravitymodifier;
	}

	public void setGravitymodifier(float gravitymodifier) {
		this.gravitymodifier = gravitymodifier;
	}

	public float getFloaterMod() {
		return floaterMod;
	}

	public void setFloaterMod(float floeaterMod) {
		this.floaterMod = floeaterMod;
	}

	public boolean isHasmobilephysics() {
		return hasmobilephysics;
	}

	public void setHasmobilephysics(boolean hasmobilephysics) {
		this.hasmobilephysics = hasmobilephysics;
	}

	public String getPhysicstype() {
		return physicstype;
	}

	public void setPhysicstype(String physicstype) {
		this.physicstype = physicstype;
	}

	public float getExitspeedmod() {
		return exitspeedmod;
	}

	public void setExitspeedmod(float exitspeedmod) {
		this.exitspeedmod = exitspeedmod;
	}

	public String getVehicletype() {
		return vehicletype;
	}

	public void setVehicletype(String vehicletype) {
		this.vehicletype = vehicletype;
	}

	public float getListenerobstruction() {
		return listenerobstruction;
	}

	public void setListenerobstruction(float listenerobstruction) {
		this.listenerobstruction = listenerobstruction;
	}

	public float getGroundcontactvolume() {
		return groundcontactvolume;
	}

	public void setGroundcontactvolume(float groundcontactvolume) {
		this.groundcontactvolume = groundcontactvolume;
	}

	public float getDamagedambientsoundlimit() {
		return damagedambientsoundlimit;
	}

	public void setDamagedambientsoundlimit(float damagedambientsoundlimit) {
		this.damagedambientsoundlimit = damagedambientsoundlimit;
	}

	public boolean isHasrestrictedexit() {
		return hasrestrictedexit;
	}

	public void setHasrestrictedexit(boolean hasrestrictedexit) {
		this.hasrestrictedexit = hasrestrictedexit;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setMaterials(HashMap<Integer, String> materials) {
		this.materials = materials;
	}

	public float getSprintRecoverTime() {
		return sprintRecoverTime;
	}

	public void setSprintRecoverTime(float sprintRecoverTime) {
		this.sprintRecoverTime = sprintRecoverTime;
	}

	public float getSprintDissipationTime() {
		return sprintDissipationTime;
	}

	public void setSprintDissipationTime(float sprintDissipationTime) {
		this.sprintDissipationTime = sprintDissipationTime;
	}

	public float getSprintLimit() {
		return sprintLimit;
	}

	public void setSprintLimit(float sprintLimit) {
		this.sprintLimit = sprintLimit;
	}

	public float getSprintFactor() {
		return sprintFactor;
	}

	public void setSprintFactor(float sprintFactor) {
		this.sprintFactor = sprintFactor;
	}

}
