package db;

import gui.Main;

import java.util.ArrayList;
import java.util.List;

public class Kit implements BF2Object {

	private String code;
	private List<String> subtemplates;
	private float abilityrestorerate;
	private float abilityinvehiclestrength;
	private float abilityinvehicleradius;
	private int abilityinvehiclematerial;
	private boolean hasrepairability;
	private boolean hashealingability;
	private String minimapicon;
	private String vehicleicon;
	private String hudname;

	public Kit(String x) {
		this.setCode(x);
		this.subtemplates = new ArrayList<String>();

	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public float getAbilityrestorerate() {
		return abilityrestorerate;
	}

	public void setAbilityrestorerate(float abilityrestorerate) {
		this.abilityrestorerate = abilityrestorerate;
	}

	public float getAbilityinvehiclestrength() {
		return abilityinvehiclestrength;
	}

	public void setAbilityinvehiclestrength(float abilityinvehiclestrength) {
		this.abilityinvehiclestrength = abilityinvehiclestrength;
	}

	public int getAbilityinvehiclematerial() {
		return abilityinvehiclematerial;
	}

	public void setAbilityinvehiclematerial(int abilityinvehiclematerial) {
		this.abilityinvehiclematerial = abilityinvehiclematerial;
	}

	public boolean isHasrepairability() {
		return hasrepairability;
	}

	public void setHasrepairability(boolean hasrepairability) {
		this.hasrepairability = hasrepairability;
	}

	public boolean isHashealingability() {
		return hashealingability;
	}

	public void setHashealingability(boolean hashealingability) {
		this.hashealingability = hashealingability;
	}

	public String getMinimapicon() {
		return minimapicon;
	}

	public void setMinimapicon(String minimapicon) {
		this.minimapicon = minimapicon;
	}

	public String getVehicleicon() {
		return vehicleicon;
	}

	public void setVehicleicon(String vehicleicon) {
		this.vehicleicon = vehicleicon;
	}

	public String getHudname() {
		return hudname;
	}

	public void setHudname(String hudname) {
		this.hudname = Main.lang.getValue(hudname);
	}

	public float getAbilityinvehicleradius() {
		return abilityinvehicleradius;
	}

	public void setAbilityinvehicleradius(float abilityinvehicleradius) {
		this.abilityinvehicleradius = abilityinvehicleradius;
	}

	@Override
	public List<String> getSubTemplates() {
		return this.subtemplates;
	}

	@Override
	public void setSubTemplates(List<String> newsubs) {
		this.subtemplates = newsubs;

	}

	@Override
	public void addSubTemplate(String subtemplate) {
		this.subtemplates.add(subtemplate);
	}

	/**
	 * 
	 * @return GenericFirearm[9] - sorted by weapon slot index
	 */
	public GenericFireArm[] getWeaponsSorted() {
		GenericFireArm[] weapons = new GenericFireArm[9];
		for (String weaponcode : this.getSubTemplates()) {
			try {
				GenericFireArm w = (GenericFireArm) Main.bf2objectDB.getByCode(weaponcode);
				if (weapons[w.getIndex() - 1] != null)
					System.err.println("ERROR: Kit " + this.getCode() + " tries to assign " + weaponcode + " on index " + (w.getIndex() - 1)
							+ ", but this slot is already occupied by " + weapons[w.getIndex() - 1].getCode()
							+ "! This will cause a CtD if this kit is used in game!");
				weapons[w.getIndex() - 1] = w;
			} catch (ClassCastException e) {

			}
		}
		return weapons;
	}
}