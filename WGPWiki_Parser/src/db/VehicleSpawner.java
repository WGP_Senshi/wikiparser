package db;

import java.util.Comparator;

/**
 * Defines a Vehicle/ObjectSpawner on maps.
 * 
 * @author Senshi
 * 
 * @param name
 * @param posX
 *            width
 * @param posY
 *            altitude
 * @param posZ
 *            height
 * @param rot
 * @param team
 * @param timeToLive
 * @param teamOnVehicle
 * @param spawnDelayAtStart
 * @param spawnMax
 * @param spawnMin
 * @param object1
 * @param object2
 * @param cpid
 * @return
 * 
 * 
 * 
 * 
 */
public class VehicleSpawner {
    private String name;
    private float posX;
    private float posY;
    private float posZ;
    private float rot;
    private int team;
    private int timeToLive;
    private int teamOnVehicle;
    private boolean spawnDelayAtStart;
    private int spawnMax;
    private int spawnMin;
    private String object1;
    private String object2;
    private int cpid;

    public VehicleSpawner(String name) {

        this.setName(name);
        this.spawnDelayAtStart = false;
    }

    Comparator<VehicleSpawner> comparator = new Comparator<VehicleSpawner>() { // eigene Vergleichsoperation!
        @Override
        public int compare(VehicleSpawner v1, VehicleSpawner v2) {
            String name1 = v1.getName();
            String name2 = v2.getName();

            return name1.compareTo(name2);
        }

    };

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setObject1(String object) {
        this.object1 = object;
    }

    public String getObject1() {
        return object1;
    }

    public void setObject2(String object2) {
        this.object2 = object2;
    }

    public String getObject2() {
        return object2;
    }

    public void setPosX(float x) {
        this.posX = x;
    }

    public float getPosX() {
        return posX;
    }

    public void setPosY(float y) {
        this.posY = y;
    }

    public float getPosY() {
        return posY;
    }

    public void setTeam(int x) {
        this.team = x;
    }

    public int getTeam() {
        return team;
    }

    public void setSpawnMin(int x) {
        this.spawnMin = x;
    }

    public int getSpawnMin() {
        return spawnMin;
    }

    public void setSpawnMax(int x) {
        this.spawnMax = x;
    }

    public int getSpawnMax() {
        return spawnMax;
    }

    public void setSpawnDelayAtStart(boolean x) {
        this.spawnDelayAtStart = x;
    }

    public boolean getSpawnDelayAtStart() {
        return spawnDelayAtStart;
    }

    public void setTimeToLive(int x) {
        this.timeToLive = x;
    }

    public int getTimeToLive() {
        return timeToLive;
    }

    public void setTeamOnVehicle(int x) {
        this.teamOnVehicle = x;
    }

    public int getTeamOnVehicle(int x) {
        return teamOnVehicle;
    }

    public float getRot() {
        return rot;
    }

    public void setRot(float rot) {
        this.rot = rot;
    }

    public int getCpid() {
        return cpid;
    }

    public void setCpid(int cpid) {
        this.cpid = cpid;
    }

    @Override
	public String toString() {
        // @override
        return name + " |Pos: " + posX + "-" + posY + " |Rot: " + rot + " |Team1: " + object1 + " |Team2: " + object2;
    }

    public float getPosZ() {
        return posZ;
    }

    public void setPosZ(float posZ) {
        this.posZ = posZ;
    }

}
