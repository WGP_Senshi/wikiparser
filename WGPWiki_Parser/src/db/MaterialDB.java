package db;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class MaterialDB {

	private List<Material> db;

	public MaterialDB(ArrayList<Material> data) {
		this.db = data;
	}

	/**
	 * Returns the projectile object from the DB by name.
	 * 
	 * Returns null if name is not found.
	 */
	public Material getByName(String name) {
		for (int i = 0; i < db.size(); i++) {
			if (db.get(i).getName().equalsIgnoreCase(name)) {
				return db.get(i);
			}
		}
		System.out.println("WARNING: Material " + name
				+ " could not be found in the Material database. Please try to rebuild the database. If the issue persists, check if the material REALLY exists in the source files somewhere.");
		return null;
	}

	public static MaterialDB readDB() {
		File db_proj = new File("db/materials.xml");
		if (!db_proj.exists())
			System.out.println("DB Materials does not exist! Please build it in the SETTINGS tab or stuff WILL NOT WORK!");

		ArrayList<Material> db = new ArrayList<Material>();

		XStream xstream = new XStream(new DomDriver());
		xstream.alias("material", Material.class);
		xstream.alias("materialcell", MaterialCell.class);

		try {
			BufferedReader fr = new BufferedReader(new FileReader(db_proj));
			ObjectInputStream in = xstream.createObjectInputStream(fr);
			while (true) {
				Material material = (Material) in.readObject();
				// System.out.println(projectile.getName() + " --- " + projectile.getDmg());
				db.add(material);
			}
		} catch (EOFException e) {
			// System.out.println("End of projectiles DB reached.");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("ERROR: An XML object in the projectiles database is not formatted properly as a projectile.");
			e.printStackTrace();
		}

		/*
		 * for (File child : db_proj.listFiles()) { String xml; try { Scanner s = new Scanner(child); xml = s.useDelimiter("\\A").next(); s.close(); Projectile
		 * projectile = (Projectile) xstream.fromXML(xml); db.add(projectile); } catch (FileNotFoundException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } }
		 */
		return new MaterialDB(db);
	}
}
