package db;

import java.util.ArrayList;
import java.util.List;

public class LandingGear implements BF2Object {

    String code;
    private List<String> subtemplates = new ArrayList<String>();

    public LandingGear(String code) {
        this.code = code;
    }

    @Override
	public List<String> getSubTemplates() {
        return subtemplates;
    }
    @Override
	public void setSubTemplates(List<String> subTemplates) {
        this.subtemplates = subTemplates;
    }
    
    @Override
	public void addSubTemplate(String subTemplate) {
        this.subtemplates.add(subTemplate);
    }
    public void getAddTemplate(int i) {
        this.subtemplates.get(i);
    }

    @Override
    public String getCode() {
        return this.code;
    }

}
