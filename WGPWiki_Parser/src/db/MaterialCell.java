package db;

public class MaterialCell {

    private int target;
    private float damagemod;
    private String effecttemplate;
    private String soundtemplate;
    
    public MaterialCell(int target ) {
        this.setTarget(target);
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public float getDamagemod() {
        return damagemod;
    }

    public void setDamagemod(float damagemod) {
        this.damagemod = damagemod;
    }

    public String getEffecttemplate() {
        return effecttemplate;
    }

    public void setEffecttemplate(String effecttemplate) {
        this.effecttemplate = effecttemplate;
    }

    public String getSoundtemplate() {
        return soundtemplate;
    }

    public void setSoundtemplate(String soundtemplate) {
        this.soundtemplate = soundtemplate;
    }
}
