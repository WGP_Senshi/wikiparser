package db;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * An object as defined by the BF2 engine, denoted by a "ObjectTemplate.create" term in the data.
 * 
 * The following attributes always should be defined:
 * 
 * @param type
 *            : String - The first token after ObjectTemplate.create
 * @param code
 *            : String - The second token after ObjectTemplate.create (this is the unique identifier)
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "@class")
@JsonSubTypes({ @Type(value = Engine.class, name = "engine"),
				@Type(value = GenericFireArm.class, name = "genericfirearm"),
				@Type(value = Kit.class, name = "kit"),
				@Type(value = LandingGear.class, name = "landinggear"),
				@Type(value = PlayerControlObject.class, name = "playercontrolobject"),
				@Type(value = Projectile.class, name = "projectile"),
				@Type(value = RotationalBundle.class, name = "rotationalbundle") })
public interface BF2Object {
	/**
	 * 
	 * @return the unique codename of this object
	 */
	public String getCode();

	/**
	 * 
	 * @return subtemplates - a List<String> of all object codes this object makes use of using OT.addTemplate
	 */
	public List<String> getSubTemplates();

	public void setSubTemplates(List<String> newsubs);

	public void addSubTemplate(String subTemplate);

	boolean equals(Object obj);

}