package db;

import java.util.ArrayList;
import java.util.List;

public class Material {

	private int id;
	private String name;
	private int type;
	private float friction;
	private float elasticity;
	private float resistance;
	private float penDeviation;
	private float damageLoss;
	private float minDamageLoss;
	private float maxDamageLoss;
	private float projectileCollisionHardness;
	private boolean hasWaterPhysics;
	private boolean overrideNeverPenetrate;
	private boolean isOneSided;
	private List<MaterialCell> cells;

	public Material(int id) {
		this.setId(id);
		this.setCells(new ArrayList<MaterialCell>());

	}

	public Material() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public float getFriction() {
		return friction;
	}

	public void setFriction(float friction) {
		this.friction = friction;
	}

	public float getElasticity() {
		return elasticity;
	}

	public void setElasticity(float elasticity) {
		this.elasticity = elasticity;
	}

	public float getResistance() {
		return resistance;
	}

	public void setResistance(float resistance) {
		this.resistance = resistance;
	}

	public float getPenDeviation() {
		return penDeviation;
	}

	public void setPenDeviation(float penDeviation) {
		this.penDeviation = penDeviation;
	}

	public float getDamageLoss() {
		return damageLoss;
	}

	public void setDamageLoss(float damageLoss) {
		this.damageLoss = damageLoss;
	}

	public float getMinDamageLoss() {
		return minDamageLoss;
	}

	public void setMinDamageLoss(float minDamageLoss) {
		this.minDamageLoss = minDamageLoss;
	}

	public float getMaxDamageLoss() {
		return maxDamageLoss;
	}

	public void setMaxDamageLoss(float maxDamageLoss) {
		this.maxDamageLoss = maxDamageLoss;
	}

	public float getProjectileCollisionHardness() {
		return projectileCollisionHardness;
	}

	public void setProjectileCollisionHardness(float projectileCollisionHardness) {
		this.projectileCollisionHardness = projectileCollisionHardness;
	}

	public boolean isOverrideNeverPenetrate() {
		return overrideNeverPenetrate;
	}

	public void setOverrideNeverPenetrate(boolean overrideNeverPenetrate) {
		this.overrideNeverPenetrate = overrideNeverPenetrate;
	}

	public boolean isOneSided() {
		return isOneSided;
	}

	public void setOneSided(boolean isOneSided) {
		this.isOneSided = isOneSided;
	}

	public boolean isHasWaterPhysics() {
		return hasWaterPhysics;
	}

	public void setHasWaterPhysics(boolean hasWaterPhysics) {
		this.hasWaterPhysics = hasWaterPhysics;
	}

	public List<MaterialCell> getCells() {
		return cells;
	}

	public void setCells(List<MaterialCell> cells) {
		this.cells = cells;
	}
}
