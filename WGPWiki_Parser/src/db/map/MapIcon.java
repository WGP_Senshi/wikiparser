package db.map;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import db.PlayerControlObject;
import gui.Main;

public class MapIcon {

	private String obj1name;
	private String obj2name;
	private String iconurl;
	private String iconrot;
	private int[] iconsize = new int[] { 16, 16 };

	public MapIcon(ObjectPlacement op, PlayerControlObject obj1, PlayerControlObject obj2) {
		this.iconrot = String.valueOf(op.getRotation()[0]);
		String[] obj1data = verifyName(obj1);
		String[] obj2data = verifyName(obj2);
		this.iconurl = "";
		if (obj1data != null) {
			this.obj1name = obj1data[0];
			this.iconurl = obj1data[1];
			if (obj1data[2] != null)
				this.iconrot = obj1data[2];
		}
		if (obj2data != null) {
			this.obj2name = obj2data[0];
			if (obj1data == null) {
				this.iconurl = obj2data[1];
				if (obj2data[2] != null)
					this.iconrot = obj2data[2];
			}
		}
	}

	/**
	 * 
	 * @param obj
	 * @return String[] - 0: pretty name
	 *         1: icon url
	 *         2: icon rot
	 */
	private String[] verifyName(PlayerControlObject obj) {
		if (obj == null)
			return null;
		String objname = obj.getCode();
		String prettyname = Main.lang.getValue(obj.getCode());
		String iconurl = "";
		String iconrot = null;
		if (prettyname == null) {
			if (objname.equalsIgnoreCase("dummy") || objname.equalsIgnoreCase("para_supply_crate")) {
				return null;
			} else if (objname.contains("vehicle_depot")) {
				iconurl = "icons/mini_map/repairdepot.png";
				iconrot = "0";
				prettyname = "Vehicle Depot";
			} else if (objname.contains("_acv_")) {
				iconurl = "icons/mini_map/uavtrailer.png";
				prettyname = "UAV Terminal";
				iconrot = "0";
			} else if (objname.equalsIgnoreCase("ammocache")) {
				iconurl = "icons/spotted_map/icon_objectivespotted.png";
				prettyname = "Weapon Cache";
			} else if (objname.toLowerCase().contains("bipod")) {
				iconurl = "icons/vehicles_map/mini_smgsmall.png";
				if (objname.toLowerCase().contains("ru_bipod"))
					prettyname = "Stationary PKM";
				else
					prettyname = "Stationary " + objname.substring(objname.lastIndexOf("_") + 1).toUpperCase();
			} else if (objname.toLowerCase().contains("fixed_supply_crate")) {
				prettyname = "Permanent supply crate";
				iconurl = "icons/mini_map/icon_supply.png";
				iconrot = "0";
			} else if (objname.toLowerCase().contains("rallypoint")) {
				iconurl = "icons/player_map/mini_squad.png";
				iconrot = "0";
				if (objname.toLowerCase().contains("_noexpire"))
					prettyname = "Permanent spawnpoint";
				else
					prettyname = "Temporary spawnpoint";
			} else if (objname.toLowerCase().equals("paradrop")) {
				iconurl = "icons/player_map/mini_squad.png";
				iconrot = "0";
				prettyname = "Temporary paradrop spawn";
			} else if (objname.toLowerCase().contains("firebase")) {
				iconurl = "icons/vehicles_map/firebase_mini_white.png";
				iconrot = "0";
				prettyname = "Firebase";
			} else if (objname.toLowerCase().contains("commandpost")) {
				iconurl = "icons/vehicles_map/firebase_mini_white.png";
				iconrot = "0";
				if (objname.toLowerCase().contains("commandpost_meins"))
				prettyname = "Hideout";
				else
					prettyname = "Command Post";
			} else if (objname.toLowerCase().contains("artillery")) {
				iconurl = "icons/mini_map/airdef.png";
				prettyname = "Artillery Barrage Call-In";
				iconrot = "0";
			} else if (objname.toLowerCase().contains("jdam_team")) {
				iconurl = "icons/mini_map/airdef.png";
				prettyname = "JDAM Call-In";
				iconrot = "0";
			} else if (objname.toLowerCase().contains("mortar_team")) {
				iconurl = "icons/mini_map/airdef.png";
				prettyname = "Mortar Barrage Call-In";
				iconrot = "0";
			}
		}
		if (prettyname == null)
			prettyname = obj.getCode();
		if (iconurl == "") {
			String minimapicon = obj.getMinimapicon();

			if (minimapicon != null) {
				minimapicon = minimapicon.replaceAll("\\\\", "/");
				if (minimapicon.contains("vehicle")) {
					iconurl = "icons/vehicles_map/";
					try {
						iconurl += minimapicon.substring(minimapicon.lastIndexOf("/") + 1, minimapicon.lastIndexOf("."));
						iconurl += ".png";
						String checksize = minimapicon.substring(0, minimapicon.lastIndexOf(".")) + ".png";
						try (ImageInputStream in = ImageIO.createImageInputStream(new File("db/atlas/menu/hud/texture/" + checksize))) {
							final Iterator<ImageReader> readers = ImageIO.getImageReaders(in);
							if (readers.hasNext()) {
								ImageReader reader = readers.next();
								try {
									reader.setInput(in);
									this.setIconsize(new int[] { reader.getWidth(0), reader.getHeight(0) });
								} finally {
									reader.dispose();
								}
							}
						}
					} catch (StringIndexOutOfBoundsException | IOException e) {
						System.err.println("ERROR: Minimapicon " + minimapicon + " not found!");
					}
				}
			}
		}
		return new String[] { prettyname, iconurl, iconrot };

	}

	public String getObj1name() {
		return obj1name;
	}

	public void setObj1name(String obj1name) {
		this.obj1name = obj1name;
	}

	public String getObj2name() {
		return obj2name;
	}

	public void setObj2name(String obj2name) {
		this.obj2name = obj2name;
	}

	public String getIconurl() {
		return iconurl;
	}

	public void setIconurl(String iconurl) {
		this.iconurl = iconurl;
	}

	public String getIconrot() {
		return iconrot;
	}

	public void setIconrot(String iconrot) {
		this.iconrot = iconrot;
	}

	public int[] getIconsize() {
		return iconsize;
	}

	public void setIconsize(int[] iconsize) {
		this.iconsize = iconsize;
	}
}