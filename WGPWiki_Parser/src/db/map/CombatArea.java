package db.map;

import java.util.ArrayList;
import java.util.List;

public class CombatArea implements GMObject {

	String code;
	private float[] min;
	private float[] max;
	private List<float[]> areapoints = new ArrayList<float[]>(); // Each point is a tuple with two float values (XY).
	private int team;
	private int vehicles;
	private int layer;
	private boolean inverted;

	public CombatArea() {
	}

	public CombatArea(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public float[] getMin() {
		return min;
	}

	public void setMin(float[] min) {
		this.min = min;
	}

	public float[] getMax() {
		return max;
	}

	public void setMax(float[] max) {
		this.max = max;
	}

	public List<float[]> getAreapoints() {
		return areapoints;
	}

	public void setAreapoints(List<float[]> areapoints) {
		this.areapoints = areapoints;
	}

	public int getTeam() {
		return team;
	}

	public void setTeam(int team) {
		this.team = team;
	}

	public int getVehicles() {
		return vehicles;
	}

	public void setVehicles(int vehicles) {
		this.vehicles = vehicles;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public boolean isInverted() {
		return inverted;
	}

	public void setInverted(boolean inverted) {
		this.inverted = inverted;
	};
}
