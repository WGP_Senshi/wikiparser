package db.map;

import java.util.ArrayList;
import java.util.List;

public class BF2Map {

	private String code;
	private String fancyname;
	private List<GameMode> gamemodes;
	private String briefing;
	private String music;
	private int size;

	/**
	 * A map as defined by the BF2 engine
	 * 
	 * The following attributes always should be defined:
	 * 
	 * @param code
	 *            : String - The level's codename. This is equal to the map folder name.
	 */
	public BF2Map(String codename) {
		this.code = codename;
		this.gamemodes = new ArrayList<GameMode>();

	}
	
	public BF2Map() {

	}

	/**
	 * 
	 * @return the unique codename of this object
	 */
	public String getCode() {
		return this.code;
	};

	/**
	 * 
	 * @return subtemplates - a List<GameMode> of all object codes this object makes use of using OT.addTemplate
	 */
	public List<GameMode> getGameModes() {
		return this.gamemodes;
	};

	public void setGameModes(List<GameMode> newmodes) {
		this.gamemodes = newmodes;
	};

	public void addGameMode(GameMode newmode) {
		this.gamemodes.add(newmode);
	};

	public boolean equals(BF2Map obj) {
		return this.getCode().equals(obj.getCode());
	}

	public String getFancyname() {
		return fancyname;
	}

	public void setFancyname(String fancyname) {
		this.fancyname = fancyname;
	}

	public String getBriefing() {
		return briefing;
	}

	public void setBriefing(String briefing) {
		this.briefing = briefing;
	}

	public String getMusic() {
		return music;
	}

	public void setMusic(String music) {
		this.music = music;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	};
}
