package db.map;

public class ObjectSpawner  implements GMObject {

	private String code;
	private String lastuser;
	private String objecttemplate_1;
	private String objecttemplate_2;
	private float minspawndelay;
	private float maxspawndelay;
	private boolean spawndelayatstart;
	private float timetolive;
	private float distance;
	private int team = -999;
	private int maxNrOfObjectSpawned = 1;
	private boolean teamonvehicle;

	public ObjectSpawner() {
	}

	public ObjectSpawner(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLastuser() {
		return lastuser;
	}

	public void setLastuser(String lastuser) {
		this.lastuser = lastuser;
	}

	public String getObjecttemplate_1() {
		return objecttemplate_1;
	}

	public void setObjecttemplate_1(String objecttemplate_1) {
		this.objecttemplate_1 = objecttemplate_1;
	}

	public String getObjecttemplate_2() {
		return objecttemplate_2;
	}

	public void setObjecttemplate_2(String objecttemplate_2) {
		this.objecttemplate_2 = objecttemplate_2;
	}

	public float getMinspawndelay() {
		return minspawndelay;
	}

	public void setMinspawndelay(float minspawndelay) {
		this.minspawndelay = minspawndelay;
	}

	public float getMaxspawndelay() {
		return maxspawndelay;
	}

	public void setMaxspawndelay(float maxspawndelay) {
		this.maxspawndelay = maxspawndelay;
	}

	public boolean getSpawndelayatstart() {
		return spawndelayatstart;
	}

	public void setSpawndelayatstart(boolean spawndelayatstart) {
		this.spawndelayatstart = spawndelayatstart;
	}

	public float getTimetolive() {
		return timetolive;
	}

	public void setTimetolive(float timetolive) {
		this.timetolive = timetolive;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public int getTeam() {
		return team;
	}

	public void setTeam(int team) {
		this.team = team;
	}

	public boolean isTeamonvehicle() {
		return teamonvehicle;
	}

	public void setTeamonvehicle(boolean teamonvehicle) {
		this.teamonvehicle = teamonvehicle;
	}

	public int getMaxNrOfObjectSpawned() {
		return maxNrOfObjectSpawned;
	}

	public void setMaxNrOfObjectSpawned(int maxNrOfObjectSpawned) {
		this.maxNrOfObjectSpawned = maxNrOfObjectSpawned;
	}
}
