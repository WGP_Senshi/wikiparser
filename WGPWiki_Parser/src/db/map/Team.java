package db.map;

public class Team  {

	private int id; // Either team 1 or 2. Team 0 would be Neutral. don't use that.
	private String name;
	private String language;
	private String flag;
	private String[] kits = new String[7];

	public Team() {
	}
	
	public Team(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String[] getKits() {
		return kits;
	}

	public void setKits(String[] kits) {
		this.kits = kits;
	}
}
