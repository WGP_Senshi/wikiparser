package db.map;

public class ObjectPlacement  implements GMObject {
	
	private String code;
	private float[] position = new float[]{0,0,0};
	private float[] rotation = new float[]{0,0,0};
	private int controlpointid = -999;
	private int layer = -999;
	
	
	public ObjectPlacement() {
	}	
	
	public ObjectPlacement(String code) {
		this.code = code;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public float[] getPosition() {
		return position;
	}



	public void setPosition(float[] position) {
		this.position = position;
	}



	public float[] getRotation() {
		return rotation;
	}



	public void setRotation(float[] rotation) {
		this.rotation = rotation;
	}



	public int getControlpointid() {
		return controlpointid;
	}



	public void setControlpointid(int controlpointid) {
		this.controlpointid = controlpointid;
	}



	public int getLayer() {
		return layer;
	}



	public void setLayer(int layer) {
		this.layer = layer;
	}

}
