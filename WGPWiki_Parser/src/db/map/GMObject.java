package db.map;

import java.util.List;

/**
 * An object as defined by the BF2 engine, denoted by a "ObjectTemplate.create" term in the data.
 * 
 * The following attributes always should be defined:
 * 
 * @param type
 *            : String - The first token after ObjectTemplate.create
 * @param code
 *            : String - The second token after ObjectTemplate.create (this is the unique identifier)
 */
public interface GMObject {

	/**
	 * 
	 * @return the unique codename of this object
	 */
	public String getCode();

}