package db.map;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;

public class GameMode {

	private String modetype;
	private int players;
	private String maptype;
	private boolean ai;
	private String locid;
	private List<ObjectPlacement> placements = new ArrayList<ObjectPlacement>();
	private List<ObjectSpawner> ostemplates = new ArrayList<ObjectSpawner>();
	private List<SpawnPoint> spawnpoints = new ArrayList<SpawnPoint>();
	private List<ControlPoint> controlpoints = new ArrayList<ControlPoint>();
	private List<CombatArea> combatareas = new ArrayList<CombatArea>();
	private Team team1 = new Team(1);
	private Team team2 = new Team(2);

	private int maxviewdistance;
	private int tickets_1;
	private int tickets_2;
	private float timetonextwave;
	private float ticketlossatendpermin;
	private float ticketlosspermin_1;
	private float ticketlosspermin_2;
	private String customtexturesuffix;

	private boolean combatareamanageruse; // Use combatareas at all: true/false
	private float combatareamanagerdamage = 20; // Damage when outside longer than combatareamanagerallowedoutside
	private float combatareamanagerallowedoutside = 10; // time in seconds

	private int heightmapsize; // Ingame size
	private int[] heightmap_imgsize; // Size of source image
	private float[] heightmapscale; //X, Y, Z scaling of source image to fit ingame. Y = height axis.
	private float heightmapseawaterlevel;

	private float[] fogstartendandbase;

	/**
	 * 
	 * @param type
	 *            : Defines the game mode. Translates to the folder gamemodes\TYPE
	 * @param players
	 *            : Defines the sub-game mode (not necessarily the player limit). Translates to the folder gamemodes\TYPE\PLAYERS
	 * @param ai
	 *            : Defines if the map has AI support (Coop mode). Not sure if relevant. TODO: Figure it out! Optional for now.
	 * @param locid
	 *            : Defines the localization string of the gamemode description. Optional.
	 */
	public GameMode(String type, int players, boolean ai, String locid) {
		this.modetype = type;
		this.players = players;
		this.ai = ai;
		this.locid = locid;
	}

	
	public GameMode() {
		
	}
	/**
	 * 
	 * @param type
	 *            : Defines the game mode. Translates to the folder gamemodes\TYPE
	 * @param players
	 *            : Defines the sub-game mode (not necessarily the player limit). Translates to the folder gamemodes\TYPE\PLAYERS
	 */
	public GameMode(String modetype, int players, String maptype) {
		this.modetype = modetype;
		this.players = players;
		this.maptype = maptype;
	}

	public String getModeType() {
		return modetype;
	}

	public void setModeType(String type) {
		this.modetype = type;
	}

	public int getPlayers() {
		return players;
	}

	public void setPlayers(int players) {
		this.players = players;
	}

	public boolean isAi() {
		return ai;
	}

	public void setAi(boolean ai) {
		this.ai = ai;
	}

	public String getLocid() {
		return locid;
	}

	public void setLocid(String locid) {
		this.locid = locid;
	}

	public List<ObjectPlacement> getPlacements() {
		return placements;
	}

	public void setPlacements(List<ObjectPlacement> placements) {
		this.placements = placements;
	}

	public List<ObjectSpawner> getOstemplates() {
		return ostemplates;
	}

	public void setOstemplates(List<ObjectSpawner> ostemplates) {
		this.ostemplates = ostemplates;
	}

	public List<SpawnPoint> getSpawnpoints() {
		return spawnpoints;
	}

	public void setSpawnpoints(List<SpawnPoint> spawnpoints) {
		this.spawnpoints = spawnpoints;
	}

	public List<ControlPoint> getControlpoints() {
		return controlpoints;
	}

	public void setControlpoints(List<ControlPoint> controlpoints) {
		this.controlpoints = controlpoints;
	}

	public List<CombatArea> getCombatareas() {
		return combatareas;
	}

	public void setCombatareas(List<CombatArea> combatareas) {
		this.combatareas = combatareas;
	}

	public Team getTeam1() {
		return team1;
	}

	public void setTeam1(Team team1) {
		this.team1 = team1;
	}

	public Team getTeam2() {
		return team2;
	}

	public void setTeam2(Team team2) {
		this.team2 = team2;
	}

	public int getMaxviewdistance() {
		return maxviewdistance;
	}

	public void setMaxviewdistance(int maxviewdistance) {
		this.maxviewdistance = maxviewdistance;
	}

	public int getTickets_1() {
		return tickets_1;
	}

	public void setTickets_1(int tickets_1) {
		this.tickets_1 = tickets_1;
	}

	public int getTickets_2() {
		return tickets_2;
	}

	public void setTickets_2(int tickets_2) {
		this.tickets_2 = tickets_2;
	}

	public float getTimetonextwave() {
		return timetonextwave;
	}

	public void setTimetonextwave(float timetonextwave) {
		this.timetonextwave = timetonextwave;
	}

	public float getTicketlossatendpermin() {
		return ticketlossatendpermin;
	}

	public void setTicketlossatendpermin(float ticketlossatendpermin) {
		this.ticketlossatendpermin = ticketlossatendpermin;
	}

	public float getTicketlosspermin_1() {
		return ticketlosspermin_1;
	}

	public void setTicketlosspermin_1(float ticketlosspermin_1) {
		this.ticketlosspermin_1 = ticketlosspermin_1;
	}

	public float getTicketlosspermin_2() {
		return ticketlosspermin_2;
	}

	public void setTicketlosspermin_2(float ticketlosspermin_2) {
		this.ticketlosspermin_2 = ticketlosspermin_2;
	}

	public String getCustomtexturesuffix() {
		return customtexturesuffix;
	}

	public void setCustomtexturesuffix(String customtexturesuffix) {
		this.customtexturesuffix = customtexturesuffix;
	}

	public String getMaptype() {
		return maptype;
	}

	public void setMaptype(String maptype) {
		this.maptype = maptype;
	}

	public String getModetype() {
		return modetype;
	}

	public void setModetype(String modetype) {
		this.modetype = modetype;
	}

	public boolean isCombatareamanageruse() {
		return combatareamanageruse;
	}

	public void setCombatareamanageruse(boolean combatareamanageruse) {
		this.combatareamanageruse = combatareamanageruse;
	}

	public float getCombatareamanagerdamage() {
		return combatareamanagerdamage;
	}

	public void setCombatareamanagerdamage(float combatareamanagerdamage) {
		this.combatareamanagerdamage = combatareamanagerdamage;
	}

	public float getCombatareamanagerallowedoutside() {
		return combatareamanagerallowedoutside;
	}

	public void setCombatareamanagerallowedoutside(float combatareamanagerallowedoutside) {
		this.combatareamanagerallowedoutside = combatareamanagerallowedoutside;
	}

	public int getHeightmapsize() {
		return heightmapsize;
	}

	public void setHeightmapsize(int heightmapsize) {
		this.heightmapsize = heightmapsize;
	}

	public int[] getHeightmap_imgsize() {
		return heightmap_imgsize;
	}

	public void setHeightmap_imgsize(int[] heightmap_imgsize) {
		this.heightmap_imgsize = heightmap_imgsize;
	}

	public float[] getHeightmapscale() {
		return heightmapscale;
	}

	public void setHeightmapscale(float[] heightmapscale) {
		this.heightmapscale = heightmapscale;
	}

	public float getHeightmapseawaterlevel() {
		return heightmapseawaterlevel;
	}

	public void setHeightmapseawaterlevel(float heightmapseawaterlevel) {
		this.heightmapseawaterlevel = heightmapseawaterlevel;
	}

	public float[] getFogstartendandbase() {
		return fogstartendandbase;
	}

	public void setFogstartendandbase(float[] fogstartendandbase) {
		this.fogstartendandbase = fogstartendandbase;
	}

}
