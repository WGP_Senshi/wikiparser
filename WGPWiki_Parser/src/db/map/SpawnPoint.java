package db.map;

public class SpawnPoint  implements GMObject {

	private String code;
	private float[] spawnpositionoffset;
	private int group;
	private int controlpointid;
	private boolean allowspawnclosetovehicle;
	private boolean onlyforhuman;

	
	public SpawnPoint() {
	}

	
	public SpawnPoint(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public float[] getSpawnpositionoffset() {
		return spawnpositionoffset;
	}

	public void setSpawnpositionoffset(float[] spawnpositionoffset) {
		this.spawnpositionoffset = spawnpositionoffset;
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

	public int getControlpointid() {
		return controlpointid;
	}

	public void setControlpointid(int controlpointid) {
		this.controlpointid = controlpointid;
	}

	public boolean isAllowspawnclosetovehicle() {
		return allowspawnclosetovehicle;
	}

	public void setAllowspawnclosetovehicle(boolean allowspawnclosetovehicle) {
		this.allowspawnclosetovehicle = allowspawnclosetovehicle;
	}

	public boolean isOnlyforhuman() {
		return onlyforhuman;
	}

	public void setOnlyforhuman(boolean onlyforhuman) {
		this.onlyforhuman = onlyforhuman;
	}
}
