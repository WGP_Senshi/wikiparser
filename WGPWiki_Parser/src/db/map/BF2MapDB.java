package db.map;

import gui.Main;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

// import com.thoughtworks.xstream.io.xml.StaxDriver;

public class BF2MapDB {

	private List<BF2Map> db;

	public BF2MapDB(ArrayList<BF2Map> data) {
		this.setDB(data);
	}

	/**
	 * Returns the weapon object from the DB by name.
	 * 
	 * Returns null if name is not found.
	 */
	public BF2Map getByCode(String code) {
		for (int i = 0; i < getDB().size(); i++) {
			if (getDB().get(i).getCode().equalsIgnoreCase(code))
				return getDB().get(i);
		}
		// System.out.println("ERROR: " + code + " not found in " + this.getClass().getSimpleName());
		return null;
	}

	public List<BF2Map> getDB() {
		return db;
	}

	public void setDB(List<BF2Map> db) {
		this.db = db;
	}

	public static BF2MapDB readDB() {
		File db_map = new File("db/mapdb.json");

		ArrayList<BF2Map> db = new ArrayList<BF2Map>();

		try {

			ObjectMapper mapper = new ObjectMapper();
			db = mapper.readValue(db_map, new TypeReference<ArrayList<BF2Map>>(){});
		} catch (EOFException e) {
			e.printStackTrace();
			// System.out.println("End of projectiles DB reached.");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.err.println("WARNING: DB BF2Map does not exist! Please build it in the SETTINGS tab or stuff WILL NOT WORK!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (com.thoughtworks.xstream.io.StreamException e) {
			System.err.println("ERROR: The BF2Map database is corrupted. Rebuilding from scratch.");
			db_map.delete();
			readDB();
		}

		return new BF2MapDB(db);
	}
}
