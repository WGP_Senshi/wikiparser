package db.map.parser;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import db.map.CombatArea;

public class parseCombatArea extends parseINIT {

	public parseCombatArea(String name) {
		super(name);
		System.err.println("FATAL: Never ever use constructors of object parsers.");
		System.exit(1);
		// TODO Never ever use this...
	}

	static CombatArea parse(String[] tokens, CombatArea component) throws IOException {
		if (tokens[0].equalsIgnoreCase("CombatArea.min")) {
			String[] tokens2 = tokens[1].split("/");
			component.setMin(new float[] { pfloat(tokens2[0]), pfloat(tokens2[1]) });
		} else if (tokens[0].equalsIgnoreCase("CombatArea.max")) {
			String[] tokens2 = tokens[1].split("/");
			component.setMax(new float[] { pfloat(tokens2[0]), pfloat(tokens2[1]) });
		} else if (tokens[0].equalsIgnoreCase("CombatArea.addAreaPoint")) {
			String[] tokens2 = tokens[1].split("/");
			List<float[]> cass = component.getAreapoints();
			float[] newpoint = new float[] { pfloat(tokens2[0]), pfloat(tokens2[1]) };
			cass.add(newpoint);
			component.setAreapoints(cass);
		} else if (tokens[0].equalsIgnoreCase("CombatArea.team"))
			component.setTeam(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("CombatArea.vehicles"))
			component.setVehicles(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("CombatArea.layer"))
			component.setLayer(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("CombatArea.inverted"))
			if (pint(tokens[1]) == 1)
				component.setInverted(true);
			else
				component.setInverted(false);
		return component;
	}
}
