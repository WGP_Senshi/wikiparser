package db.map.parser;

import java.io.IOException;
import java.util.Arrays;

import db.map.ObjectPlacement;

public class parseObjectPlacement extends parseINIT {

	public parseObjectPlacement(String name) {
		super(name);
		System.err.println("FATAL: Never ever use constructors of object parsers.");
		System.exit(1);
		// TODO Never ever use this...
	}

	static ObjectPlacement parse(String[] tokens, ObjectPlacement component) throws IOException {
		// System.out.println("X: " + Arrays.toString(tokens));
		if (tokens[0].equalsIgnoreCase("Object.absolutePosition")) {
			String[] tokens2 = tokens[1].split("/");
			if (tokens2.length != 3) {
				System.err.println("Malformed position in Object.create " + component.getCode());
				System.err.println(Arrays.toString(tokens));
				return component;
			}
			component.setPosition(new float[] { pfloat(tokens2[0]), pfloat(tokens2[1]), pfloat(tokens2[2]) });
		} else if (tokens[0].equalsIgnoreCase("Object.rotation")) {
			String[] tokens2 = tokens[1].split("/");
			if (tokens2.length != 3) {
				System.err.println("Malformed rotation in Object.create " + component.getCode());
				System.err.println(Arrays.toString(tokens));
				return component;
			}
			component.setRotation(new float[] { pfloat(tokens2[0]), pfloat(tokens2[1]), pfloat(tokens2[2]) });
		} else if (tokens[0].equalsIgnoreCase("Object.layer"))
			component.setLayer(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("Object.setControlPointId"))
			component.setControlpointid(pint(tokens[1]));
		return component;
	}
}
