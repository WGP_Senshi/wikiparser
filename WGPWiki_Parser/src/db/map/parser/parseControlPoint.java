package db.map.parser;

import java.io.IOException;

import db.map.ControlPoint;

public class parseControlPoint extends parseINIT {

	public parseControlPoint(String name) {
		super(name);
		System.err.println("FATAL: Never ever use constructors of object parsers.");
		System.exit(1);
		// TODO Never ever use this...
	}

	static ControlPoint parse(String[] tokens, ControlPoint component) throws IOException {
		// System.out.println("X: " + Arrays.toString(tokens));
		if (tokens[0].equalsIgnoreCase("ObjectTemplate.unableToChangeTeam"))
			if (pint(tokens[1]) == 1)
				component.setUnabletochangeteam(true);
			else
				component.setUnabletochangeteam(false);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.physicsType"))
			component.setPhysicsType(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setControlPointName"))
			component.setName(gui.Main.lang.getValue(tokens[1], tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.team"))
			component.setTeam(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.controlPointId"))
			component.setControlpointid(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.radius"))
			component.setRadius(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.areaValueTeam1"))
			component.setAreavalueteam1(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.areaValueTeam2"))
			component.setAreavalueteam2(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.timetogetcontrol"))
			component.setTimetogetcontrol(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.timeToLoseControl"))
			component.setTimetolosecontrol(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.supplygroupid"))
			component.setSupplygroupid(pint(tokens[1]));
		return component;
	}
}
