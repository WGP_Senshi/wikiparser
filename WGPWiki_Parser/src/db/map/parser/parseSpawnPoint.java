package db.map.parser;

import java.io.IOException;

import db.map.SpawnPoint;

public class parseSpawnPoint extends parseINIT {

	public parseSpawnPoint(String name) {
		super(name);
		System.err.println("FATAL: Never ever use constructors of object parsers.");
		System.exit(1);
		// TODO Never ever use this...
	}

	static SpawnPoint parse(String[] tokens, SpawnPoint component) throws IOException {
		// System.out.println("X: " + Arrays.toString(tokens));
		if (tokens[0].equalsIgnoreCase("ObjectTemplate.setSpawnPositionOffset")) {
			String[] tokens2 = tokens[1].split("/");
			component.setSpawnpositionoffset(new float[] { pfloat(tokens2[0]), pfloat(tokens2[1]), pfloat(tokens2[2]) });
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setControlPointId"))
			component.setControlpointid(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.group"))
			component.setGroup(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setAllowSpawnCloseToVehicle"))
			if (pint(tokens[1]) == 1)
				component.setAllowspawnclosetovehicle(true);
			else
				component.setAllowspawnclosetovehicle(false);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setOnlyForHuman"))
			if (pint(tokens[1]) == 1)
				component.setOnlyforhuman(true);
			else
				component.setOnlyforhuman(false);
		return component;
	}
}
