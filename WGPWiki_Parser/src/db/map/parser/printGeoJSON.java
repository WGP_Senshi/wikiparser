package db.map.parser;

import gui.Main;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;

import db.PlayerControlObject;
import db.map.BF2Map;
import db.map.BF2MapDB;
import db.map.CombatArea;
import db.map.ControlPoint;
import db.map.GameMode;
import db.map.MapIcon;
import db.map.ObjectPlacement;
import db.map.ObjectSpawner;
import db.map.SpawnPoint;
import db.map.Team;

public class printGeoJSON {

	static BufferedWriter brr;

	public static void print(BF2MapDB mapdb) throws IOException {

		BufferedWriter bw_maplist = new BufferedWriter(new FileWriter(new File("db/map_viewer/maplist.json")));
		bw_maplist.write("[");
		for (int i = 0; i < mapdb.getDB().size(); i++) {
			BF2Map curmap = mapdb.getDB().get(i);
			if (curmap.getCode().equalsIgnoreCase("kokan_sp")) //Skip this stupid map :)
				continue;
			String curmapname = "";
			curmapname = Main.lang.getValue(curmap.getCode());
			if (curmapname == null)
				curmapname = Main.lang.getValue("HUD_LEVELNAME_" + curmap.getCode(), curmap.getCode());
			File target = new File("db/map_viewer/" + curmap.getCode() + "/listgm.json");
			target.getParentFile().mkdirs();
			BufferedWriter bw_list = new BufferedWriter(new FileWriter(target));
			bw_list.write("[");

			if (i > 0)
				bw_maplist.write("\n  },  {");
			else
				bw_maplist.write("\n  {");
			bw_maplist.write("\n    \"code\" : \"" + curmap.getCode() + "\",");
			bw_maplist.write("\n    \"name\" : \"" + curmapname + "\",");
			bw_maplist.write("\n    \"listgm\" : \"map_json/" + curmap.getCode() + "/listgm.json\",");
			int heightmapsize = 0;
			/*
			 * Iterate over each GameMode
			 */
			for (int j = 0; j < curmap.getGameModes().size(); j++) {

				GameMode curmode = curmap.getGameModes().get(j);

				heightmapsize = curmode.getHeightmapsize();
				List<ObjectSpawner> cuross = curmode.getOstemplates();
				List<ObjectPlacement> curplaces = curmode.getPlacements();
				List<ControlPoint> curcps = curmode.getControlpoints();
				List<CombatArea> curcas = curmode.getCombatareas();
				File target_gm = new File("db/map_viewer/" + curmap.getCode() + "/" + curmode.getModetype() + "_" + curmode.getPlayers() + ".json");
				brr = new BufferedWriter(new FileWriter(target_gm));

				if (j > 0)
					bw_list.write(",\n  \"" + curmode.getModetype() + "_" + curmode.getPlayers() + "\"");
				else
					bw_list.write("\n  \"" + curmode.getModetype() + "_" + curmode.getPlayers() + "\"");

				/*
				 * General map info. Nothing extraordinary.
				 */
				brr.write("{");
				writeLine("  \"team1\":\"" + curmode.getTeam1().getName() + "\",");
				writeLine("  \"team2\":\"" + curmode.getTeam2().getName() + "\",");
				writeLine("  \"team1tickets\":\"" + curmode.getTickets_1() + "\",");
				writeLine("  \"team2tickets\":\"" + curmode.getTickets_2() + "\",");
				writeLine("  \"mapsize\":\"" + curmode.getHeightmapsize() + "\",");
				writeLine("  \"viewdistance\":\"" + curmode.getMaxviewdistance() + "\",");
				writeLine("  \"combatareause\":\"" + curmode.isCombatareamanageruse() + "\",");
				writeLine("  \"combatareadamage\":\"" + curmode.getCombatareamanagerdamage() + "\",");
				writeLine("  \"combatareaallowoutside\":\"" + curmode.getCombatareamanagerallowedoutside() + "\",");
				writeLine("  \"routes\": [");
				System.out.println(curmap.getFancyname() + ": " + curmode.getModeType() + " " + curmode.getPlayers());
				List<List<List<ControlPoint>>> AASroutes = determineAASRoutes(curcps);
				int objcounter = 0;
				for (int i2 = 0; i2 < AASroutes.size(); i2++) {
					if (objcounter > 0)
						brr.write(",");
					objcounter++;
					writeLine("    [");
					int obj2counter = 0;
					for (int j2 = 0; j2 < AASroutes.get(i2).size(); j2++) {
						if (obj2counter > 0)
							brr.write(",");
						obj2counter++;
						writeLine("      [");
						int obj3counter = 0;
						for (ControlPoint z : AASroutes.get(i2).get(j2)) {
							if (obj3counter > 0)
								brr.write(",");
							obj3counter++;
							writeLine("        \"" + z.getCode() + "\"");
						}
						writeLine("      ]");
					}
					writeLine("    ]");
				}
				writeLine("  ],");

				writeLine("  \"type\": \"FeatureCollection\",");
				writeLine("  \"features\": [");

				/*
				 * Iterating over all object placements (Object.create). These include Objectspawnertemplates, ControlPoints, and spawnpoints.
				 * The necessary objectspawnertemplates are looked up individually.
				 * It also looks at controlpoints to determine ownership (team).
				 */

				objcounter = 0;
				for (int k = 0; k < curplaces.size(); k++) {
					ObjectPlacement curplace = curplaces.get(k);
					if (getOSTbyCode(cuross, curplace.getCode()) != null) {
						ObjectSpawner curplaceobjectspawner = getOSTbyCode(cuross, curplace.getCode());
						PlayerControlObject curplaceobject1 = null;
						PlayerControlObject curplaceobject2 = null;

						try {
								curplaceobject1 = (PlayerControlObject) Main.bf2objectDB.getByCode(curplaceobjectspawner.getObjecttemplate_1());
								curplaceobject2 = (PlayerControlObject) Main.bf2objectDB.getByCode(curplaceobjectspawner.getObjecttemplate_2());
						} catch (ClassCastException e) {
							continue;
						}

						if (curplaceobject1 != null || curplaceobject2 != null) {

							MapIcon icon = new MapIcon(curplace, curplaceobject1, curplaceobject2);
							float x = curplace.getPosition()[0];
							float y = curplace.getPosition()[1];
							float z = curplace.getPosition()[2];
							//System.out.println(factorx);
							// Battlefield coordinate system starts at the CENTER of the map. As the final tiles will all be 256 "units" wide/high, 128 is half.
							// Battlefield also goes right in X-direction and up in Y-direction. Also, Y in BF is "up/down", while Z is "north/south".
							// Leaflet has origin at topleft and increases right in X and down in Y. Hence the chaos conversion :) . Leaflet also only knows X (west-east) and Y (north-south).
							x = curmode.getHeightmapsize() / 2 + x;
							z = -curmode.getHeightmapsize() / 2 + z;

							// Placements can have a private team setting, if they are used without a cpid.
							// If both a private team setting and a cpid exist, the cpid's team overrides the private one.
							int team = -999;
							String icon_title = "";
							for (ControlPoint cp : curcps)
								if (curplace.getControlpointid() == cp.getControlpointid()) {
									team = cp.getTeam();
									if (cp.isUnabletochangeteam())
										if (team == 1)
											icon_title = icon.getObj1name();
										else
											icon_title = icon.getObj2name();
									break;
								}
							if (team == -999)
								team = curplaceobjectspawner.getTeam();

							if (icon.getObj1name() == null) {
								icon_title = icon.getObj2name();
								team = 2;
							} else if (icon.getObj2name() == null) {
								icon_title = icon.getObj1name();
								team = 1;
							} else {
								icon_title = icon.getObj1name() + " | " + icon.getObj2name();
							}
							if (objcounter > 0)
								brr.write(",");
							objcounter++;
							writeLine("    {");
							writeLine("      \"type\": \"Feature\",");
							writeLine("      \"geometry\": {");
							writeLine("        \"type\": \"Point\",");
							writeLine("        \"coordinates\": [" + (x - icon.getIconsize()[0]/2) + ", " + (z- icon.getIconsize()[1]/2) + "],");
							writeLine("        \"size\": [" + icon.getIconsize()[0] + ", " + icon.getIconsize()[1] + "]");
							writeLine("      },");
							writeLine("      \"properties\": {");
							writeLine("        \"iconurl\": \"" + icon.getIconurl() + "\",");
							writeLine("        \"iconrotate\": \"" + icon.getIconrot() + "\",");
							writeLine("        \"popupContent\": \"Amazing information about " + icon.getObj1name() + " will be found here!\"");
							writeLine("      },");
							writeLine("      \"bf2props\": {");
							writeLine("        \"name\": \"" + curplace.getCode() + "\",");
							writeLine("        \"class\": \"" + curplaceobjectspawner.getClass().getSimpleName() + "\",");
							writeLine("        \"name_object\": \"" + icon_title + "\",");
							writeLine("        \"team\": \"" + team + "\",");
							writeLine("        \"obj1\": \"" + curplaceobjectspawner.getObjecttemplate_1() + "\",");
							writeLine("        \"obj2\": \"" + curplaceobjectspawner.getObjecttemplate_2() + "\",");
							writeLine("        \"maxspawn\": \"" + curplaceobjectspawner.getMaxspawndelay() + "\",");
							writeLine("        \"minspawn\": \"" + curplaceobjectspawner.getMinspawndelay() + "\",");
							writeLine("        \"spawndelay\": \"" + curplaceobjectspawner.getSpawndelayatstart() + "\",");
							if (curplaceobjectspawner.getMaxNrOfObjectSpawned() != 1)
								writeLine("        \"maxnr\": \"" + curplaceobjectspawner.getMaxNrOfObjectSpawned() + "\",");
							writeLine("        \"ttl\": \"" + curplaceobjectspawner.getTimetolive() + "\",");
							writeLine("        \"cpid\": \"" + curplace.getControlpointid() + "\"");
							writeLine("      }");
							writeLine("    }");
						}
					} else if (getCPbyCode(curcps, curplace.getCode()) != null) {
						/*
						 * Start of CONTROL POINTS
						 */
						ControlPoint curcp = getCPbyCode(curcps, curplace.getCode());

						float x = curplace.getPosition()[0];
						float y = curplace.getPosition()[1];
						float z = curplace.getPosition()[2];

						float factorx = (float) heightmapsize / 2048;
						float factorz = (float) heightmapsize / 2048;
						// Battlefield coordinate system starts at the CENTER of the map. As the final tiles will all be 256 "units" wide/high, 128 is half.
						// Battlefield also goes right in X-direction and up in Y-direction. Also, Y in BF is "up/down", while Z is "north/south".
						// Leaflet has origin at topleft and increases right in X and down in Y. Hence the chaos conversion :) . Leaflet also only knows X (west-east) and Y (north-south).
						x = curmode.getHeightmapsize() / 2 + x;
						z = -curmode.getHeightmapsize() / 2 + z;

						// Placements can have a private team setting, if they are used without a cpid.
						// If both a private team setting and a cpid exist, the cpid's team overrides the private one.
						int team = -999;
						for (ControlPoint cp : curcps)
							if (curplace.getControlpointid() == cp.getControlpointid()) {
								team = cp.getTeam();
								break;
							}
						if (team == -999)
							team = curcp.getTeam();

						Team curteam = null;
						String minimapiconurl = "";
						if (team == 1) {
							curteam = curmode.getTeam1();
							minimapiconurl = curteam.getFlag();
						} else if (team == 2) {
							curteam = curmode.getTeam2();
							minimapiconurl = curteam.getFlag();
						}
						if (minimapiconurl.toLowerCase().contains("arf"))
							minimapiconurl = "icons/flags_map/arf/minimap_cp.png";
						if (minimapiconurl.toLowerCase().contains("arg"))
							minimapiconurl = "icons/flags_map/arg82/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("cf"))
							minimapiconurl = "icons/flags_map/cf/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("chinsurgent") || minimapiconurl.toLowerCase().contains("flag_re"))
							minimapiconurl = "icons/flags_map/chinsurgent/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("ch"))
							minimapiconurl = "icons/flags_map/ch/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("fr"))
							minimapiconurl = "icons/flags_map/fr/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("fsa"))
							minimapiconurl = "icons/flags_map/fsa/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("gb"))
							minimapiconurl = "icons/flags_map/gb/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("ger"))
							minimapiconurl = "icons/flags_map/ger/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("hamas"))
							minimapiconurl = "icons/flags_map/hamas/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("idf"))
							minimapiconurl = "icons/flags_map/idf/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("mec"))
							minimapiconurl = "icons/flags_map/mec/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("meinsurgent") || minimapiconurl.toLowerCase().equals("flag_in"))
							minimapiconurl = "icons/flags_map/meinsurgent/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("neutral"))
							minimapiconurl = "icons/flags_map/neutral/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("nl"))
							minimapiconurl = "icons/flags_map/nl/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("ru"))
							minimapiconurl = "icons/flags_map/ru/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("taliban"))
							minimapiconurl = "icons/flags_map/taliban/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("us"))
							minimapiconurl = "icons/flags_map/us/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("usa"))
							minimapiconurl = "icons/flags_map/usa/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("vnnva"))
							minimapiconurl = "icons/flags_map/vnnva/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("vnusa"))
							minimapiconurl = "icons/flags_map/vnusa/minimap_cp.png";
						else if (minimapiconurl.toLowerCase().contains("vnusmc"))
							minimapiconurl = "icons/flags_map/vnusmc/minimap_cp.png";
						else
							minimapiconurl = "icons/flags_map/neutral/minimap_cp.png";

						if (curcp.isUnabletochangeteam() && minimapiconurl != "")
							minimapiconurl = minimapiconurl.substring(0, minimapiconurl.length() - 4) + "base.png";

						if (objcounter > 0)
							brr.write(",");
						objcounter++;
						writeLine("    {");
						writeLine("      \"type\": \"Feature\",");
						writeLine("      \"geometry\": {");
						writeLine("        \"type\": \"Point\",");
						writeLine("        \"coordinates\": [" + x + ", " + z + "],");
						writeLine("        \"size\": [" + 33 + ", " + 33 + "],");
						writeLine("        \"radius\": \"" + (curcp.getRadius()) + "\"");
						writeLine("      },");
						writeLine("      \"properties\": {");
						writeLine("        \"iconurl\": \"" + minimapiconurl + "\",");
						writeLine("        \"iconrotate\": \"" + 0 + "\",");
						writeLine("        \"popupContent\": \"Amazing information about " + curcp.getName() + " will be found here!\"");
						writeLine("      },");
						writeLine("      \"bf2props\": {");
						writeLine("        \"name\": \"" + curplace.getCode() + "\",");
						writeLine("        \"class\": \"" + curcp.getClass().getSimpleName() + "\",");
						writeLine("        \"name_object\": \"" + curcp.getName() + "\",");
						writeLine("        \"team\": \"" + team + "\",");
						writeLine("        \"areavalue1\": \"" + curcp.getAreavalueteam1() + "\",");
						writeLine("        \"areavalue2\": \"" + curcp.getAreavalueteam2() + "\",");
						writeLine("        \"cpid\": \"" + curcp.getControlpointid() + "\",");
						writeLine("        \"sgid\": \"" + curcp.getSupplygroupid() + "\",");
						writeLine("        \"uncap\": \"" + curcp.isUnabletochangeteam() + "\",");
						writeLine("        \"ttgc\": \"" + curcp.getTimetogetcontrol() + "\",");
						writeLine("        \"ttlc\": \"" + curcp.getTimetolosecontrol() + "\"");
						writeLine("      }");
						writeLine("    }");
						/*
						 * End of CONTROL POINTS
						 */

					}
					/*
					 * End of OBJECT PLACEMENTS
					 */
				}
				/*
				 * Start of COMBAT AREAS
				 */
				for (int k = 0; k < curcas.size(); k++) {
					CombatArea curca = curcas.get(k);
					if (objcounter > 0)
						brr.write(",");
					objcounter++;

					writeLine("    {");
					writeLine("      \"type\": \"Feature\",");
					writeLine("      \"geometry\": {");
					writeLine("        \"type\": \"Polygon\",");
					writeLine("        \"coordinates\": [");
					writeLine("          [");
					if (!curca.isInverted()) {// If not inverted, this is a regular combatarea. This translates to a GeoJSON polygon with map bounds and the areapoints defining a hole.

						writeLine("            [0,0],");
						writeLine("            [0,-" + curmode.getHeightmapsize() + "],");
						writeLine("            [" + curmode.getHeightmapsize() + ",-" + curmode.getHeightmapsize() + "],");
						writeLine("            [" + curmode.getHeightmapsize() + ",0]");
						writeLine("          ],[");
					}
					float factorx = (float) heightmapsize / 2048;
					float factorz = (float) heightmapsize / 2048;
					// Battlefield coordinate system starts at the CENTER of the map. As the final tiles will all be 256 "units" wide/high, 128 is half.
					// Battlefield also goes right in X-direction and up in Y-direction. Also, Y in BF is "up/down", while Z is "north/south".
					// Leaflet has origin at topleft and increases right in X and down in Y. Hence the chaos conversion :) . Leaflet also only knows X (west-east) and Y (north-south).

					for (int m = 0; m < curca.getAreapoints().size(); m++) {
						if (m > 0)
							brr.write(",");
						float[] areapoint = curca.getAreapoints().get(m);
						float x = areapoint[0];
						float z = areapoint[1];

						x = curmode.getHeightmapsize() / 2 + x;
						z = -curmode.getHeightmapsize() / 2 + z;

						writeLine("            [" + x + ", " + z + "]");
					}
					writeLine("          ]");
					writeLine("        ]");
					writeLine("      },");
					writeLine("      \"bf2props\": {");
					writeLine("        \"code\": \"" + curca.getCode() + "\",");
					writeLine("        \"class\": \"" + curca.getClass().getSimpleName() + "\",");
					writeLine("        \"team\": \"" + curca.getTeam() + "\",");
					writeLine("        \"vehicles\": \"" + curca.getVehicles() + "\",");
					writeLine("        \"layer\": \"" + curca.getLayer() + "\",");
					writeLine("        \"inverted\": \"" + curca.isInverted() + "\",");
					writeLine("        \"max\": [" + curca.getMax()[0] + ", " + curca.getMax()[1] + "],");
					writeLine("        \"min\": [" + curca.getMin()[0] + ", " + curca.getMin()[1] + "]");
					writeLine("      }");
					writeLine("    }");

					/*
					 * End of COMBAT AREAS
					 */
				}
				writeLine("  ]");
				writeLine("}");
				brr.close();
				/*
				 * End of GAMEMODE
				 */
			}
			bw_list.write("\n]");
			bw_list.close();
			bw_maplist.write("\n    \"mapsize\" : \"" + heightmapsize + "\"");
			/*
			 * End of ALL
			 */
		}
		bw_maplist.write("\n  },");
		bw_maplist.write("\n]");
		bw_maplist.close();

	}

	private static ObjectSpawner getOSTbyCode(List<ObjectSpawner> ost, String code) {
		for (ObjectSpawner os : ost) {
			if (os.getCode().equalsIgnoreCase(code)) {
				return os;
			}
		}
		return null;

	}

	private static ControlPoint getCPbyCode(List<ControlPoint> ost, String code) {
		for (ControlPoint os : ost) {
			if (os.getCode().equalsIgnoreCase(code)) {
				return os;
			}
		}
		return null;

	}

	private static SpawnPoint getSPbyCode(List<SpawnPoint> sp, String code) {
		for (SpawnPoint os : sp) {
			if (os.getCode().equalsIgnoreCase(code)) {
				return os;
			}
		}
		return null;

	}

	private static ControlPoint getCPbyID(List<ControlPoint> cps, int code) {
		for (int i = 0; i < cps.size(); i++) {
			if (cps.get(i).getControlpointid() == code)
				return cps.get(i);
		}
		// System.out.println("ERROR: " + code + " not found in " + this.getClass().getSimpleName());
		return null;

	}

	private static void writeLine(String str) throws IOException {
		try {
			brr.newLine();
			brr.write(str);
		} catch (IOException e) {
		}

	}

	private static List<List<List<ControlPoint>>> determineAASRoutes(List<ControlPoint> flags) {
		List<List<List<ControlPoint>>> routes = new ArrayList<List<List<ControlPoint>>>();
		/*
		 * This segment figures out how many AAS Routes exist
		 * AASv4:
		 *   Digit 1: Indicates position along the route. Lowest number is start for team 1, highest (or negative) number is start for team 2.
		 *   Digit 2: Indicates randomization. If multiple CPs have the same digit 1, there will be randomly picked an amount equal to digit 2 that is in play. If digit 2 is equal to the number of CPs, all of them are in play (same as no digit on non-AASv4)
		 *   Digit 3: Indicates the route this flag belongs to. The route number will be chosen randomly during map loading. Only flags of the same route are ever active.
		 * Non-AASv4:
		 *   These only have one digit, indicating flag order.
		 *   There can be two digits, if randomization is in effect.
		 *   If multiple flags have the same number, all of them have to be capped before the next flag level becomes cappable.
		 */
		List<ControlPoint> negative = new ArrayList<ControlPoint>(); // If a flag has a negative index, let's make sure this one is added to all routes as the very last flag. 
		// A negative index indicates team2 mainbase. If no negative index is used on the gamemode, 
		// the flag with the highest index is the end of the route.
		List<ControlPoint> positive = new ArrayList<ControlPoint>();
		List<ControlPoint> singles = new ArrayList<ControlPoint>(); // Flags with one or two digits. These are added to all routes.
		for (ControlPoint f : flags) {
			if (f.getSupplygroupid() < 0) {
				if (f.getSupplygroupid() == -999) {
					/*
					 * This code adds CPs with no SGID to the start or end of the lines. 
					 * This is a bit troublesome, because every single map maker seems to like assiging or not-assigning SGIDs on a whim...
					 * For now, I just leave them out of the AAS routes entirely. Might not always look nice (usually it's detached main bases without AAS route connections),
					 * but it's the truest representation possible right now.
					 */
					if (f.getTeam() == 2) {
						negative.add(f);
						continue;
					} else if (f.getTeam() == 1) {
						positive.add(f);
						continue;
					} else {
						System.out.print(f.getCode() + " with SGID " + f.getSupplygroupid() + " appears to be orphaned??");
						continue;
					}

				} else if (f.getSupplygroupid() == -1) {
					negative.add(f);
					continue;
				}
			}
			String sgid = String.valueOf(f.getSupplygroupid());
			if (sgid.length() == 3) {
				int routeno = Integer.parseInt(sgid.substring(2, 3));
				if (routeno == 0) {
					singles.add(f);
				} else {
					try {
						routes.get(routeno).get(Integer.parseInt(sgid.substring(0, 1))).add(f);
					} catch (IndexOutOfBoundsException e) {
						while (routes.size() <= routeno)
							routes.add(new ArrayList<List<ControlPoint>>());
						try {
							routes.get(routeno).get(Integer.parseInt(sgid.substring(0, 1))).add(f);
						} catch (IndexOutOfBoundsException e2) {
							while (routes.get(routeno).size() <= Integer.parseInt(sgid.substring(0, 1)))
								routes.get(routeno).add(new ArrayList<ControlPoint>());
							routes.get(routeno).get(Integer.parseInt(sgid.substring(0, 1))).add(f);

						}
					}
				}
			} else {
				singles.add(f);
			}
		}
		for (int i = 0; i < routes.size(); i++) {
			List<List<ControlPoint>> x = routes.get(i);
			for (int j = 0; j < x.size(); j++) {
				if (x.get(j).size() == 0) {
					x.remove(j);
					j--;
				}
			}
			if (x.isEmpty()) {
				routes.remove(i);
				i--;
			}
		}
		if (singles.size() != 0) {
			if (routes.isEmpty()) {
				routes.add(new ArrayList<List<ControlPoint>>());
			}
			for (ControlPoint single : singles) {
				String sgidstr = String.valueOf(single.getSupplygroupid());
				for (int i = 0; i < routes.size(); i++) {
					List<List<ControlPoint>> route = routes.get(i);
					boolean foundandadded = false;
					for (int j = 0; j < route.size(); j++) {
						List<ControlPoint> step = route.get(j);
						if (step.isEmpty())
							continue;

						String sgidstr2 = String.valueOf(step.get(0).getSupplygroupid());
						if (sgidstr2.substring(0, 1).equals(sgidstr.substring(0, 1))) {
							route.get(j).add(single);
							foundandadded = true;
							break;
						}

					}
					if (!foundandadded) {
						List<ControlPoint> add = new ArrayList<ControlPoint>();
						add.add(single);
						for (int j = 0; j < route.size(); j++) {
							if (route.get(j).isEmpty())
								continue;
							if (Integer.parseInt(String.valueOf(route.get(j).get(0).getSupplygroupid()).substring(0, 1)) == Integer
									.parseInt(sgidstr.substring(0, 1))) {
								route.get(j).add(single);
								foundandadded = true;
								break;
							}
						}
						if (!foundandadded) {
							int lowest = 99;
							for (int j = 0; j < route.size(); j++) {
								if (route.get(j).isEmpty())
									continue;
								if (Integer.parseInt(String.valueOf(route.get(j).get(0).getSupplygroupid()).substring(0, 1)) < lowest) {
									if (Integer.parseInt(String.valueOf(route.get(j).get(0).getSupplygroupid()).substring(0, 1)) > Integer
											.parseInt(sgidstr.substring(0, 1)) && j < lowest)
										lowest = j;
								}
							}
							if (lowest != 99)
								if (route.size() >= lowest && lowest > 0) {
									if (route.get(lowest - 1).isEmpty())
										route.get(lowest - 1).add(single);
									else {
										route.add(lowest, add);
									}
								} else
									route.add(lowest, add);
							else
								try {
									route.add(add);
								} catch (IndexOutOfBoundsException e) {
									while (route.size() <= Integer.parseInt(sgidstr.substring(0, 1)))
										route.add(new ArrayList<ControlPoint>());
									route.get(Integer.parseInt(sgidstr.substring(0, 1))).add(single);
								}
						}
					}
					routes.set(i, route);
				}
			}
		}
		for (int i = 0; i < routes.size(); i++) {
			List<List<ControlPoint>> x = routes.get(i);
			for (int j = 0; j < x.size(); j++) {
				if (x.get(j).size() == 0) {
					x.remove(j);
					j--;
				}
			}
			if (x.isEmpty()) {
				routes.remove(i);
				i--;
			}
		}

		if (negative.size() != 0) {
			for (List<List<ControlPoint>> x : routes) {
				boolean issolo = true;
				for (List<ControlPoint> y : x) {
					for (ControlPoint z : y)
						if (z.getTeam() == 2) {
							issolo = false;
							break;
						}
					if (issolo)
						break;

				}
				if (issolo)
					x.add(negative);
			}
		}

		if (positive.size() != 0) {
			for (List<List<ControlPoint>> x : routes) {
				boolean issolo = true;
				for (List<ControlPoint> y : x) {
					for (ControlPoint z : y)
						if (z.getTeam() == 1) {
							issolo = false;
							break;
						}
					if (issolo)
						break;

				}
				if (issolo)
					x.get(0).addAll(positive);
			}
		}

		for (int i = 0; i < routes.size(); i++) {
			if (routes.get(i).size() == 2) {
				boolean bothuncaps = true;
				for (List<ControlPoint> x : routes.get(i)) {
					for (ControlPoint y : x) {
						if (!y.isUnabletochangeteam())
							bothuncaps = false;
					}
				}
				if (bothuncaps) {
					routes.remove(i);
					i--;
				}
			}
		}
		return routes;
	}

	private static List<ControlPoint[]> determineAASLinks(List<ControlPoint> flags, int routeno) {
		routeno++;
		List<ControlPoint[]> links = new ArrayList<ControlPoint[]>();

		for (int j = 0; j < flags.size(); j++) {
			if (String.valueOf(flags.get(j).getSupplygroupid()).length() == 1) { // Search first flag if there's only one digit
				int start = flags.get(j).getSupplygroupid();
				for (int k = 0; k < flags.size(); k++) { // Search following flag
					int plusone = flags.get(k).getSupplygroupid();
					if (plusone == start + 1) {
						System.out.println("Link " + routeno + " SGIDs: " + start + " " + plusone);
						links.add(new ControlPoint[] { flags.get(j), flags.get(k) });
					}

				}
			} else if (String.valueOf(flags.get(j).getSupplygroupid()).length() == 3) // AASv4, three digits 
				if (Integer.parseInt(String.valueOf(flags.get(j).getSupplygroupid()).substring(2)) != routeno) // retrieves last number
					continue;
			int start = Integer.parseInt(String.valueOf(flags.get(j).getSupplygroupid()).substring(0, 1)); // retrieves first number
			for (int k = 0; k < flags.size(); k++) { // Search following flag
				if (String.valueOf(flags.get(k).getSupplygroupid()).length() == 1) { // True for simple AAS with single number
					if (Integer.parseInt(String.valueOf(flags.get(k).getSupplygroupid())) != routeno)
						continue;
				} else if (String.valueOf(flags.get(k).getSupplygroupid()).length() == 3)
					if (Integer.parseInt(String.valueOf(flags.get(k).getSupplygroupid()).substring(2)) != routeno) // retrieves last number
						continue;
				int plusone = Integer.parseInt(String.valueOf(flags.get(k).getSupplygroupid()).substring(0, 1));
				if (plusone == start + 1) { // If routenumber actually increases by 1

					System.out.println("Link " + routeno + " SGIDs: " + flags.get(j).getSupplygroupid() + " " + flags.get(k).getSupplygroupid());
					// if (flags.get(j).getSGID() == 0 || flags.get(k).getSGID() == 0)
					// continue;
					links.add(new ControlPoint[] { flags.get(j), flags.get(k) });
				}
			}
		}
		return links;
	}
}
