package db.map.parser;

import java.io.IOException;
import java.util.Arrays;

import db.map.ObjectSpawner;

public class parseObjectSpawner extends parseINIT {

	public parseObjectSpawner(String name) {
		super(name);
		System.err.println("FATAL: Never ever use constructors of object parsers.");
		System.exit(1);
		// TODO Never ever use this...
	}

	static ObjectSpawner parse(String[] tokens, ObjectSpawner component) throws IOException {
		// System.out.println("X: " + Arrays.toString(tokens));
		if (tokens[0].equalsIgnoreCase("ObjectTemplate.setObjectTemplate")) {
			if (tokens.length == 3)
				if (pint(tokens[1]) == 1)
					component.setObjecttemplate_1(tokens[2]);
				else
					component.setObjecttemplate_2(tokens[2]);
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.minSpawnDelay"))
			component.setMinspawndelay(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.maxSpawnDelay"))
			component.setMaxspawndelay(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.spawnDelayAtStart"))
			if (pint(tokens[1]) == 1)
				component.setSpawndelayatstart(true);
			else
				component.setSpawndelayatstart(false);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.TimeToLive"))
			component.setTimetolive(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.Distance"))
			component.setDistance(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.team"))
			component.setTeam(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.teamOnVehicle"))
			if (pint(tokens[1]) == 1)
				component.setTeamonvehicle(true);
			else
				component.setTeamonvehicle(false);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.maxNrOfObjectSpawned"))
			component.setMaxNrOfObjectSpawned(pint(tokens[1]));

		return component;
	}
}
