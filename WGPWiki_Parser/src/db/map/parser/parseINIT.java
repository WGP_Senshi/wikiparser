package db.map.parser;

import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.FileVisitResult.SKIP_SUBTREE;
import gui.Main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;

import db.Engine;
import db.GenericFireArm;
import db.Kit;
import db.LandingGear;
import db.PlayerControlObject;
import db.Projectile;
import db.RotationalBundle;
import db.map.BF2Map;
import db.map.BF2MapDB;
import db.map.CombatArea;
import db.map.ControlPoint;
import db.map.GMObject;
import db.map.GameMode;
import db.map.ObjectPlacement;
import db.map.ObjectSpawner;
import db.map.SpawnPoint;
import db.map.Team;

/**
 * This one visits the levels folder and iterates over each level folder (not deeper).
 * It checks if a info\MAPNAME.desc file is present and parses it, initializing a new BF2Map object.
 * 
 * It then triggers parsing for each of the identified gamemodes of the map, filling that content in as well.
 * 
 * @author Senshi
 *
 */
public class parseINIT extends SimpleFileVisitor<Path> {
	protected static ArrayList<BF2Map> bf2Maps;
	protected final static ObjectMapper JSONmap = new ObjectMapper();
	protected static FileOutputStream out;
	protected static GMObject currentobj = null;
	static int no = 1; // Dateizaehler
	static common.PBar bar;
	final static int READ_AHEAD_LIMIT = 500; // The maximum length of a "OT.create line. I'm pretty sure 500 characters are enough.

	protected static void log(Object aObject) {
		try {
			System.out.println(String.valueOf(aObject));
		} catch (IllegalArgumentException e) {
		}
	}

	parseINIT(String name) {
		try {
			bf2Maps = new ArrayList<BF2Map>();
			out = new FileOutputStream(new File(name));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void parseSource(File dir, List log) throws IOException {
		bar = new common.PBar(SWT.SMOOTH);
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		log.add(dateFormat.format(new Date()) + " : Processing BF2 maps. This might take a while.");
		log.getParent().update();

		// int count = Utils.countFilesInDirectory(dir);
		// bar.setMaximum(count);
		// bar.setMinimum(0);
		// bar.setSelection(0);
		bar.setText("Parsing files, please be patient.");
		bar.setMaximum(common.Utils.countDirsinDir(dir));
		parseINIT pW = new parseINIT("db/mapdb.json");
		// System.out.println(dir);
		Files.walkFileTree(dir.toPath(), pW);
		bar.setText("Writing " + bf2Maps.size() + " maps...");
		bar.setMaximum(bf2Maps.size());
		JsonGenerator JSONWriter = new JsonFactory().createGenerator(out);
		JSONWriter.setCodec(JSONmap);
		JSONWriter.setPrettyPrinter(new DefaultPrettyPrinter());
		try {
			JSONWriter.writeObject(bf2Maps);
			for (int i = 0; i < bf2Maps.size(); i++) {
				BF2Map curmap = bf2Maps.get(i);
				bar.setSelection(i);
				//JSONWriter.writeObject(curmap);

				for (int j = 0; j < curmap.getGameModes().size(); j++) {
					GameMode curmode = curmap.getGameModes().get(j);
					File target = new File("db/map_viewer/" + curmap.getCode() + File.separator + curmode.getModetype() + "_" + curmode.getPlayers() + ".json");
					target.getParentFile().mkdirs();
					JSONmap.writerWithDefaultPrettyPrinter().writeValue(target, curmode);
				}
				//JSONWriter.writ.writeValue(out, (bf2Maps.get(i)));
			}
		} finally {
			JSONWriter.close();
			out.close();
		}
		bar.setText("Reading " + bf2Maps.size() + " maps...");
		Main.bf2mapdb = BF2MapDB.readDB();
		log.add(dateFormat.format(new Date()) + " : BF2Map database has been updated!");
		bar.dispose();
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		if (new File(dir + File.separator + "info" + File.separator + (dir.getFileName()) + ".desc").exists()) {
			try {
				processInfo(new File(dir + File.separator + "info" + File.separator + (dir.getFileName()) + ".desc").toPath(), null, null, null);
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return SKIP_SUBTREE; // We always skip, because we don't want to visit the map's contents here.
		}
		return CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		//processFile(file, null, null, null);
		return CONTINUE;
	}

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
		return CONTINUE;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) {
		System.err.println(exc);
		return CONTINUE;
	}

	protected static int getBF2MapIndex(BF2Map obj) {
		if (!bf2Maps.isEmpty() && obj != null) {
			// System.out.println("BFObjectssize " + BF2Maps.size());
			for (int i = 0; i < bf2Maps.size(); i++) {
				// System.out.println(i);
				if (bf2Maps.get(i).getCode().equalsIgnoreCase(obj.getCode())) {
					return i;
				}
			}
		}
		return -999;
	}

	protected static BF2Map getBF2Map(String code) {
		if (!bf2Maps.isEmpty() && code != null) {
			for (int i = 0; i < bf2Maps.size(); i++) {
				if (bf2Maps.get(i).getCode().equalsIgnoreCase(code)) {
					return bf2Maps.get(i);
				}
			}
		}
		return null;
	}

	protected static GMObject getGMObject(GameMode currentgm, String type, String code) {
		if (type.equals("objectspawner")) {
			for (int i = 0; i < currentgm.getOstemplates().size(); i++)
				if (currentgm.getOstemplates().get(i).getCode().equalsIgnoreCase(code)) {
					return currentgm.getOstemplates().get(i);
				}
		} else if (type.equals("combatarea")) {
			for (int i = 0; i < currentgm.getCombatareas().size(); i++)
				if (currentgm.getCombatareas().get(i).getCode().equalsIgnoreCase(code)) {
					return currentgm.getCombatareas().get(i);
				}
		} else if (type.equals("controlpoint")) {
			for (int i = 0; i < currentgm.getControlpoints().size(); i++)
				if (currentgm.getControlpoints().get(i).getCode().equalsIgnoreCase(code)) {
					return currentgm.getControlpoints().get(i);
				}
			/* ObjectPlacements have no type, so they are not activeSafe-accessible.
			} else if (type.equals("ObjectPlacement")) {
				for (int i = 0; i < currentgm.getPlacements().size(); i++)
					if (currentgm.getPlacements().get(i).getCode().equalsIgnoreCase(code)) {
						return currentgm.getPlacements().get(i);
					}
			 * 
			 */
		} else if (type.equals("spawnpoint")) {
			for (int i = 0; i < currentgm.getPlacements().size(); i++)
				if (currentgm.getSpawnpoints().get(i).getCode().equalsIgnoreCase(code)) {
					return currentgm.getSpawnpoints().get(i);
				}
		}
		return null;
	}

	/**
	 * 
	 * @param infofile
	 * @param vargs
	 * @param currentmap
	 *            -- Usually !null during "include" calls. The specified object will immediately be opened instead of waiting for a "create"
	 * @param modus
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	protected static void processInfo(Path infofile, String[] vargs, BF2Map currentmap, String modus) throws IOException, ParserConfigurationException,
			SAXException {
		no++;
		bar.setSelection(no);
		// Verify that the file is a correct file suffix
		Path filename = infofile.getFileName();
		bar.setText(filename.toString());

		// System.out.println(file);
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		//org.w3c.dom.Document document = docBuilder.parse(new File("G:/Project Reality BF2/mods/pr_edit/levels/albasrah/info/albasrah.desc"));
		Document document = docBuilder.parse(infofile.toFile());
		currentmap = new BF2Map(infofile.getParent().getParent().getFileName().toString());
		Element docEle = document.getDocumentElement();
		NodeList nl = docEle.getChildNodes();
		for (int i = 0; i < nl.getLength(); i++) {
			if (nl.item(i).getNodeType() == Node.ELEMENT_NODE) {
				Element subel = (Element) nl.item(i);
				if (subel.getNodeName().equals("name"))
					currentmap.setFancyname(subel.getTextContent().trim());
				else if (subel.getNodeName().equals("briefing"))
					currentmap.setBriefing(subel.getAttribute("locid").trim());
				else if (subel.getNodeName().equals("music"))
					currentmap.setMusic(subel.getTextContent().trim());
				else if (subel.getNodeName().equals("modes")) {
					NodeList subnl = subel.getChildNodes();
					for (int j = 0; j < subnl.getLength(); j++) {
						if (subnl.item(j).getNodeType() == Node.ELEMENT_NODE) {
							Element subel2 = (Element) subnl.item(j);
							if (subel2.getNodeName().equals("mode")) {
								String modetype = subel2.getAttribute("type");
								NodeList subnl2 = subel2.getChildNodes();
								for (int k = 0; k < subnl2.getLength(); k++) {
									if (subnl2.item(k).getNodeType() == Node.ELEMENT_NODE) {
										Element subel3 = (Element) subnl2.item(k);
										if (subel3.getNodeName().equals("maptype")) {
											GameMode newgm = new GameMode(modetype, Integer.valueOf(subel3.getAttribute("players")),
													subel3.getAttribute("type"));
											newgm = parseGM(newgm, infofile.getParent().getParent());
											currentmap.addGameMode(newgm);

										}

									}
								}

							}
						}
					}

				}
			}
		}
		if (currentmap != null) {
			int index = getBF2MapIndex(currentmap);
			if (index != -999)
				bf2Maps.set(index, currentmap);
			else
				bf2Maps.add(currentmap);

		}

	};

	private static GameMode parseGM(GameMode currentgm, Path levelpath) throws IOException {
		File initfile = new File(levelpath + File.separator + "init.con");
		File gmfile = new File(levelpath + File.separator + "gamemodes" + File.separator + currentgm.getModeType() + File.separator + currentgm.getPlayers()
				+ File.separator + "gameplayobjects.con");
		currentobj = null;
		currentgm = processFile(initfile.toPath(), null, currentgm);
		currentobj = null;
		currentgm = processFile(gmfile.toPath(), null, currentgm);

		storeGMObject(currentgm);
		return currentgm;

	}

	protected static GameMode processFile(Path file, String[] vargs, GameMode currentgm) throws IOException {
		if (currentgm != null) {
			// System.out.println(file);
			if (file.getParent().toString().contains("editor"))
				return currentgm;
			BufferedReader scanner = new BufferedReader(new FileReader(file.toFile()));
			try {
				String[] tokens = { "x" };
				while (tokens != null) {
					tokens = prepLine(scanner, vargs);
					currentgm = processLine(scanner, tokens, vargs, file, currentgm);
				}
			} finally {
				scanner.close();
			}
		}
		return currentgm;

	};

	/**
	 * Prepares a line for parsing: Removes leading whitespaces, splits line into tokens separated by whitespaces and returns tokens.
	 */
	protected static String[] prepLine(BufferedReader scanner, String[] varg) throws IOException {
		String line = scanner.readLine();
		if (line == null) // Eject if end of file
			return null;
		line = line.toLowerCase();
		line = line.trim();
		// String[] tokens = line.split("\\s"); // split on all spaces
		/*
		 * This is some voodoo regex to split on spaces, except if there's double quotes going around, e.g. strings with spaces.
		 * The non-escaped regex is \s(?!(\S+\s+)*\S+\\")|\s(?=") . Only marginally easier to read...
		 */
		String[] tokens = line.split("\\s(?!(\\S+\\s+)*\\S+\\\\\")|\\s(?=\\\")");
		for (int x = 0; x < tokens.length; x++)
			tokens[x] = tokens[x].replace("\"", "");
		tokens = varg_check(tokens, varg);
		// System.out.println(line);
		// System.out.println(Arrays.toString(tokens));
		return tokens;
	}

	/**
	 * Processes a single .con/ .tweak line
	 * 
	 * @throws IOException
	 */
	protected static GameMode processLine(BufferedReader scanner, String[] tokens, String[] vargs, Path path, GameMode currentgm) throws IOException {

		if (tokens == null)
			return currentgm;

		//tokens = varg_check(tokens, vargs);
		if (tokens[0].equalsIgnoreCase("beginrem"))
			processremblock(scanner, tokens, path);
		else if (tokens[0].equalsIgnoreCase("endif"))
			return currentgm;
		else if (tokens[0].equalsIgnoreCase("if") || tokens[0].equalsIgnoreCase("elseif"))
			processIf(scanner, tokens, vargs, path, currentgm);

		else if (tokens[0].equalsIgnoreCase("include") || tokens[0].equalsIgnoreCase("run")) {
			if (tokens.length > 2)
				processInclude(tokens[1], Arrays.copyOfRange(tokens, 2, tokens.length), path, currentgm);
			else
				processInclude(tokens[1], null, path, currentgm);
		}

		// Detect creation of new objects and trigger proper parsers.
		else if (tokens[0].equalsIgnoreCase("GameLogic.MaximumLevelViewDistance"))
			currentgm.setMaxviewdistance(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("gameLogic.setDefaultNumberOfTicketsEx")) {
			if (pint(tokens[1]) == currentgm.getPlayers())
				if (pint(tokens[2]) == 1)
					currentgm.setTickets_1(pint(tokens[3]));
				else
					currentgm.setTickets_2(pint(tokens[3]));
		} else if (tokens[0].equalsIgnoreCase("gameLogic.setDefaultTimeToNextAIWave"))
			currentgm.setTimetonextwave(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("gameLogic.setTicketLossAtEndPerMin"))
			currentgm.setTicketlossatendpermin(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("gameLogic.setTicketLossPerMin"))
			if (pint(tokens[1]) == 1)
				currentgm.setTicketlosspermin_1(pint(tokens[1]));
			else
				currentgm.setTicketlosspermin_2(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("texturemanager.customTextureSuffix"))
			currentgm.setCustomtexturesuffix(tokens[1]);
		// Heightmap data
		else if (tokens[0].equalsIgnoreCase("heightmapcluster.setHeightmapSize"))
			currentgm.setHeightmapsize(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("heightmap.setSize"))
			currentgm.setHeightmap_imgsize(new int[] { pint(tokens[1]), pint(tokens[2]) });
		else if (tokens[0].equalsIgnoreCase("heightmap.setScale")) {
			String[] tokens2 = tokens[1].split("/");
			currentgm.setHeightmapscale(new float[] { pfloat(tokens2[0]), pfloat(tokens2[1]), pfloat(tokens2[2]) });
		} else if (tokens[0].equalsIgnoreCase("heightmapcluster.setSeaWaterLevel"))
			currentgm.setHeightmapseawaterlevel(pfloat(tokens[1]));

		// Sky.con
		else if (tokens[0].equalsIgnoreCase("Renderer.fogStartEndAndBase")) {
			String[] tokens2 = tokens[1].split("/");
			currentgm.setFogstartendandbase(new float[] { pfloat(tokens2[0]), pfloat(tokens2[1]), pfloat(tokens2[2]), pfloat(tokens2[3]) });
		}

		// Factions init
		else if (tokens[0].equalsIgnoreCase("gameLogic.setTeamName"))
			if (pint(tokens[1]) == 1)
				currentgm.getTeam1().setName(tokens[2]);
			else
				currentgm.getTeam2().setName(tokens[2]);
		else if (tokens[0].equalsIgnoreCase("gameLogic.setTeamLanguage"))
			if (pint(tokens[1]) == 1)
				currentgm.getTeam1().setLanguage(tokens[2]);
			else
				currentgm.getTeam2().setLanguage(tokens[2]);
		else if (tokens[0].equalsIgnoreCase("gameLogic.setTeamFlag"))
			if (pint(tokens[1]) == 1)
				currentgm.getTeam1().setFlag(tokens[2]);
			else
				currentgm.getTeam2().setFlag(tokens[2]);
		else if (tokens[0].equalsIgnoreCase("gameLogic.setKit"))
			if (pint(tokens[1]) == 1)
				currentgm.getTeam1().getKits()[pint(tokens[2])] = (tokens[3]);
			else
				currentgm.getTeam2().getKits()[pint(tokens[2])] = (tokens[3]);

		// CombatAreaManager
		else if (tokens[0].equalsIgnoreCase("CombatAreaManager.use"))
			if (pint(tokens[1]) == 1)
				currentgm.setCombatareamanageruse(true);
			else
				currentgm.setCombatareamanageruse(false);
		else if (tokens[0].equalsIgnoreCase("CombatAreaManager.damage"))
			currentgm.setCombatareamanagerdamage(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("CombatAreaManager.timeAllowedOutside"))
			currentgm.setCombatareamanagerallowedoutside(pfloat(tokens[1]));

		// Object Placements
		else if (tokens[0].equalsIgnoreCase("Object.create")) {
			storeGMObject(currentgm);
			if (tokens.length != 2) {
				System.err.println("ERROR: File " + path + " has a malformed create call. Look for: " + Arrays.toString(tokens));
				return null;
			}
			currentobj = new ObjectPlacement(tokens[1]);
		}
		// ObjectSpawn Templates
		else if (tokens[0].equalsIgnoreCase("objecttemplate.create")) {
			storeGMObject(currentgm);
			if (tokens.length < 3) {
				System.err.println("ERROR: File " + path + " has a malformed create call. Look for: " + Arrays.toString(tokens));
				return null;
			}
			// tokens[1] == classname
			// tokens[2] == object codename
			if (tokens[1].equalsIgnoreCase("objectspawner")) {
				currentobj = new ObjectSpawner(tokens[2]);
			} else if (tokens[1].equalsIgnoreCase("controlpoint")) {
				currentobj = new ControlPoint(tokens[2]);
			} else if (tokens[1].equalsIgnoreCase("spawnpoint")) {
				currentobj = new SpawnPoint(tokens[2]);
			}
			// CombatArea
		} else if (tokens[0].equalsIgnoreCase("CombatArea.create")) {
			storeGMObject(currentgm);
			currentobj = new CombatArea(tokens[1]);
		} else if (tokens[0].equalsIgnoreCase("objecttemplate.activesafe")) {
			storeGMObject(currentgm);
			if (tokens.length < 3) {
				System.err.println("ERROR: File " + path + " has a malformed activeSafe call. Look for: " + Arrays.toString(tokens));
				return null;
			}

			currentobj = getGMObject(currentgm, tokens[1], tokens[2]);
			// if (currentobj == null)
			// System.out.println("WARNING: There was an attempt to activesafe " + tokens[2] + " BEFORE it was being created in file " + path); // This will pop
			// up often unless all objects are parsed, so we skip it for now. TODO
			return currentgm;
		} else if (currentobj != null) {
			//if (currentobj.getClass() != ObjectPlacement.class && currentobj.getClass() != SpawnPoint.class)
			//System.out.println(currentobj.getClass().getSimpleName());
			if (currentobj.getClass() == ObjectSpawner.class)
				currentobj = db.map.parser.parseObjectSpawner.parse(tokens, (ObjectSpawner) currentobj);
			else if (currentobj.getClass() == CombatArea.class)
				currentobj = db.map.parser.parseCombatArea.parse(tokens, (CombatArea) currentobj);
			else if (currentobj.getClass() == ControlPoint.class)
				currentobj = db.map.parser.parseControlPoint.parse(tokens, (ControlPoint) currentobj);
			else if (currentobj.getClass() == ObjectPlacement.class)
				currentobj = db.map.parser.parseObjectPlacement.parse(tokens, (ObjectPlacement) currentobj);
			else if (currentobj.getClass() == SpawnPoint.class)
				currentobj = db.map.parser.parseSpawnPoint.parse(tokens, (SpawnPoint) currentobj);
		}

		return currentgm;

	}

	private static void storeGMObject(GameMode currentgm) {
		try {
			if (currentobj == null)
				return;
			if (currentobj.getClass() == ObjectSpawner.class) {
				for (int i = 0; i < currentgm.getOstemplates().size(); i++)
					if (currentgm.getOstemplates().get(i).getCode().equalsIgnoreCase(currentobj.getCode())) {
						currentgm.getOstemplates().set(i, (ObjectSpawner) currentobj);
						return;
					}
				currentgm.getOstemplates().add((ObjectSpawner) currentobj);
			} else if (currentobj.getClass() == CombatArea.class) {
				// Sanity check, because mappers like to leave their messed up combatareas lying around...
				CombatArea curca = (CombatArea) currentobj;
				if (curca.getAreapoints().isEmpty())
					return;
				for (int i = 0; i < currentgm.getCombatareas().size(); i++)
					if (currentgm.getCombatareas().get(i).getCode().equalsIgnoreCase(currentobj.getCode())) {
						currentgm.getCombatareas().set(i, (db.map.CombatArea) currentobj);
						return;
					}

				currentgm.getCombatareas().add((CombatArea) currentobj);
			} else if (currentobj.getClass() == ControlPoint.class) {
				for (int i = 0; i < currentgm.getControlpoints().size(); i++)
					if (currentgm.getControlpoints().get(i).getCode().equalsIgnoreCase(currentobj.getCode())) {
						currentgm.getControlpoints().set(i, (ControlPoint) currentobj);
						return;
					}
				currentgm.getControlpoints().add((ControlPoint) currentobj);
			} else if (currentobj.getClass() == ObjectPlacement.class) {
				for (int i = 0; i < currentgm.getPlacements().size(); i++)
					if (currentgm.getPlacements().get(i).getCode().equalsIgnoreCase(currentobj.getCode())) {
						currentgm.getPlacements().set(i, (ObjectPlacement) currentobj);
						return;
					}
				currentgm.getPlacements().add((ObjectPlacement) currentobj);
			} else if (currentobj.getClass() == SpawnPoint.class) {
				for (int i = 0; i < currentgm.getSpawnpoints().size(); i++)
					if (currentgm.getSpawnpoints().get(i).getCode().equalsIgnoreCase(currentobj.getCode())) {
						currentgm.getSpawnpoints().set(i, (SpawnPoint) currentobj);
						return;
					}
				currentgm.getSpawnpoints().add((SpawnPoint) currentobj);
			}
		} finally {
			currentobj = null;
		}
		return;
	}

	protected static float pfloat(String a) {
		try {
			return Float.parseFloat(a);
		} catch (NumberFormatException e) {
			System.err.println(currentobj.getCode() + " " + currentobj.getClass().getSimpleName());
			System.err.println(a);
			e.printStackTrace();
			System.exit(1);
		}
		return Float.NaN;
	}

	protected static int pint(String a) {
		try {
			return Integer.parseInt(a);
		} catch (NumberFormatException e) {
			System.err.println(a);
			e.printStackTrace();
			System.exit(1);
		}
		return -987654321;
	}

	protected static void processremblock(BufferedReader scanner, String[] tokens, Path path) throws IOException {
		while (tokens != null) {
			tokens = prepLine(scanner, null);
			if (tokens == null) {
				log("WARNING: A comment block in " + path.toString()
						+ " is being started with 'beginrem' but not being closed with 'endrem' until the end of file has been reached!");
				break;
			}
			if (tokens[0].equalsIgnoreCase("endrem")) {
				break;
			}
		}

	}

	/**
	 * Figures out if any of the tokens include a v_arg and replaces it with the content of currently active v_args. Necessary when v_args are handed over in
	 * two sequential include calls include x.con 1 2 3 -> x.con -> include y.con v_arg2 v_arg3 -> varg_check replaces v_arg2 and v_arg3
	 * 
	 * @param tokens
	 * @param varg
	 * @return
	 */
	private static String[] varg_check(String[] tokens, String[] varg) {
		if (varg == null || tokens[0].equalsIgnoreCase("if") || tokens[0].equalsIgnoreCase("elseif"))
			return tokens;
		for (int x = 0; x < tokens.length; x++) {
			for (int y = 0; y < varg.length; y++) {
				if (tokens[x].equalsIgnoreCase("v_arg" + String.valueOf(y + 1)))
					tokens[x] = varg[y];
			}
		}
		return tokens;
	}

	protected static GameMode processIf(BufferedReader scanner, String[] tokens, String[] vargs, Path path, GameMode currentgm) throws IOException {
		ArrayList<Boolean> subifs = new ArrayList<Boolean>();

		while (tokens != null) {
			if (tokens[0].equalsIgnoreCase("if") || tokens[0].equalsIgnoreCase("elseif")) {

				//System.out.println(Arrays.toString(tokens) + "----" + Arrays.toString(vargs));
				if (tokens[0].equalsIgnoreCase("elseif")) {
					if (subifs.size() == 0) {
						System.out.println("ERROR: An elseif condition begins in " + path + ", but there was no prior 'if'.");
						return currentgm;
					} else
						subifs.remove(subifs.size() - 1); // One level closes...
				}
				if (!tokens[1].startsWith("v_arg")) {
					// System.err.println("ERROR: Conditions are only supported with variable names 'v_arg'! " + tokens[1] + " == " + tokens[2] +
					// " is not valid.");
					return currentgm;
				}
				try {
					int argno = pint(tokens[1].substring(5));
					boolean valid = true; // Are outer if-conditions true? If not, we skip the inner ones.
					for (int x = 0; x < subifs.size(); x++)
						if (subifs.get(x) == false)
							valid = false;
					// System.out.println(path + " " + Arrays.toString(tokens) + " --- " + Arrays.toString(vargs));
					if (tokens.length < 4) {
						System.err.println("ERROR: There are only " + tokens.length + " tokens, while at least 4 are needed for this to work:"
								+ Arrays.toString(tokens));
						return currentgm;
					}
					if (vargs == null || vargs.length < argno) {
						if (tokens[3].equalsIgnoreCase("BF2Editor") || tokens[3].equalsIgnoreCase("host"))
							return currentgm;
						System.out.println("Path: " + path);
						System.out.println("Tokens: " + Arrays.toString(tokens));
						if (vargs == null)
							System.out.println("WARNING: There are null vargs, while at least " + argno + " is needed for this to work:"
									+ Arrays.toString(tokens) + " --- " + Arrays.toString(vargs));
						else {
							System.out.println("Vargs: " + Arrays.toString(vargs));
							System.out.println("WARNING: There are only " + vargs.length + " vargs, while at least " + argno + " are needed for this to work:"
									+ Arrays.toString(tokens) + " --- " + Arrays.toString(vargs));
						}
						valid = false;
						//return currentobj;
					}
					if (valid && tokens[3].equalsIgnoreCase(vargs[argno - 1])) { // Varg equality check
						//System.out.print("true");
						subifs.add(true); // ... and another level opens true
						while (tokens != null) {
							tokens = prepLine(scanner, vargs);
							if (tokens == null) {
								log("WARNING: An if block in " + path
										+ " is being started with 'if' but not being closed with 'endif' until the end of file has been reached!");
								return currentgm;
							}
							if (tokens[0].equalsIgnoreCase("if") || tokens[0].equalsIgnoreCase("elseif") || tokens[0].equalsIgnoreCase("endif"))
								break;
							currentgm = processLine(scanner, tokens, vargs, path, currentgm);
						}
					} else {
						subifs.add(false); // ... and another level opens false
						while (tokens != null) {
							tokens = prepLine(scanner, vargs);
							if (tokens[0].equalsIgnoreCase("if") || tokens[0].equalsIgnoreCase("elseif") || tokens[0].equalsIgnoreCase("endif"))
								break;
						}
					}
				} catch (IndexOutOfBoundsException e) {

					System.err.println("Tokens: " + Arrays.toString(tokens));
					System.err.println("Vargs: " + Arrays.toString(vargs));
					e.printStackTrace();
					System.exit(-1);
				}
			} else if (tokens[0].equalsIgnoreCase("endif")) {
				if (subifs.size() == 1)
					return currentgm;
				else {
					subifs.remove(subifs.size() - 1);
					tokens = prepLine(scanner, vargs);
				}

			} else
				tokens = prepLine(scanner, vargs);
		}
		return currentgm;
	}

	protected static void processInclude(String inclpath, String[] vargs, Path path, GameMode currentgm) throws IOException {
		File newpath = new File(path.toString().substring(0, path.toString().lastIndexOf(File.separator)) + File.separator + inclpath);
		String[] blacklist = { "hudsetup", "tr_scripts", "staticobjects.con", "spawners/spawners_" };
		for (String black : blacklist)
			if (inclpath.toLowerCase().contains(black))
				//We exclude these because these have zero impact on bf2 objects, but are very bulky and increase parsing time significantly.
				return;
		try {
			processFile(newpath.toPath(), vargs, currentgm);

			// System.out.println(newpath + "----" + Arrays.toString(vargs));
		} catch (FileNotFoundException e) {
			if (inclpath.split("/").length > 1)
				if (inclpath.split("/")[1].equalsIgnoreCase("objects"))
					return;
			// System.out.println(inclpath + "---" + inclpath.split("/").length);
			//System.out.println("INFO: The following file was referenced by an 'include' call in " + path + ", but does not exist: " + inclpath);

			// This is not fatal, but usually indicates a typo or sloppiness on behalf of the mod
		}

	}
}