package db.map;

public class ControlPoint  implements GMObject {

	private String code;
	private String physicsType;
	private String name; // Localize value if possible. If not, value is used.
	private int team;
	private int controlpointid;
	private boolean unabletochangeteam = false;
	private float[] hoistminmax;
	private float radius;
	private float areavalueteam1;
	private float areavalueteam2;
	private float timetogetcontrol;
	private float timetolosecontrol;
	private int supplygroupid = -999;

	public ControlPoint() {
	}
	public ControlPoint(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPhysicsType() {
		return physicsType;
	}

	public void setPhysicsType(String physicsType) {
		this.physicsType = physicsType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTeam() {
		return team;
	}

	public void setTeam(int team) {
		this.team = team;
	}

	public int getControlpointid() {
		return controlpointid;
	}

	public void setControlpointid(int controlpointid) {
		this.controlpointid = controlpointid;
	}

	public boolean isUnabletochangeteam() {
		return unabletochangeteam;
	}

	public void setUnabletochangeteam(boolean unabletochangeteam) {
		this.unabletochangeteam = unabletochangeteam;
	}

	public float[] getHoistminmax() {
		return hoistminmax;
	}

	public void setHoistminmax(float[] hoistminmax) {
		this.hoistminmax = hoistminmax;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public float getAreavalueteam1() {
		return areavalueteam1;
	}

	public void setAreavalueteam1(float areavalueteam1) {
		this.areavalueteam1 = areavalueteam1;
	}

	public float getAreavalueteam2() {
		return areavalueteam2;
	}

	public void setAreavalueteam2(float areavalueteam2) {
		this.areavalueteam2 = areavalueteam2;
	}

	public float getTimetogetcontrol() {
		return timetogetcontrol;
	}

	public void setTimetogetcontrol(float timetogetcontrol) {
		this.timetogetcontrol = timetogetcontrol;
	}

	public float getTimetolosecontrol() {
		return timetolosecontrol;
	}

	public void setTimetolosecontrol(float timetolosecontrol) {
		this.timetolosecontrol = timetolosecontrol;
	}

	public int getSupplygroupid() {
		return supplygroupid;
	}

	public void setSupplygroupid(int supplygroupid) {
		this.supplygroupid = supplygroupid;
	}
}
