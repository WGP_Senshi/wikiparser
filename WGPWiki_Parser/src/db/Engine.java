package db;

import java.util.ArrayList;
import java.util.List;

public class Engine implements BF2Object {
	String code;
	private List<String> subtemplates = new ArrayList<String>();
	
	private float[] minRotation;
	private float[] maxRotation;
	private float[] maxSpeed;
	private float[] acceleration;
	private float torque;
	private float differential;
	private int numberofgears;
	private float gearup;
	private float geardown;
	private float[] gearratios;
	private boolean automaticreset;
	private boolean snaptozeroonnoinput;
	private boolean restorerotationonexit;
	private String enginetype;

	private float trackturnacceleration;
	private float trackturnspeed;

	public Engine(String code) {
		this.code = code;
	}

	public List<String> getSubTemplates() {
		return subtemplates;
	}

	public void setSubTemplates(List<String> subTemplates) {
		this.subtemplates = subTemplates;
	}

	public void addSubTemplate(String subTemplate) {
		this.subtemplates.add(subTemplate);
	}

	public void getAddTemplate(int i) {
		this.subtemplates.get(i);
	}

	public String getCode() {
		return this.code;
	}

	public float[] getMinRotation() {
		return minRotation;
	}

	public void setMinRotation(float[] minRotation) {
		this.minRotation = minRotation;
	}

	public float[] getMaxRotation() {
		return maxRotation;
	}

	public void setMaxRotation(float[] maxRotation) {
		this.maxRotation = maxRotation;
	}

	public float[] getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(float[] maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public float[] getAcceleration() {
		return acceleration;
	}

	public void setAcceleration(float[] acceleration) {
		this.acceleration = acceleration;
	}

	public float getTorque() {
		return torque;
	}

	public void setTorque(float torque) {
		this.torque = torque;
	}

	public boolean isAutomaticreset() {
		return automaticreset;
	}

	public void setAutomaticreset(boolean automaticreset) {
		this.automaticreset = automaticreset;
	}

	public boolean isSnaptozeroonnoinput() {
		return snaptozeroonnoinput;
	}

	public void setSnaptozeroonnoinput(boolean snaptozeroonnoinput) {
		this.snaptozeroonnoinput = snaptozeroonnoinput;
	}

	public boolean isRestorerotationonexit() {
		return restorerotationonexit;
	}

	public void setRestorerotationonexit(boolean restorerotationonexit) {
		this.restorerotationonexit = restorerotationonexit;
	}

	public String getEnginetype() {
		return enginetype;
	}

	public void setEnginetype(String enginetype) {
		this.enginetype = enginetype;
	}

	public float getDifferential() {
		return differential;
	}

	public void setDifferential(float differential) {
		this.differential = differential;
	}

	public int getNumberofgears() {
		return numberofgears;
	}

	public void setNumberofgears(int numberofgears) {
		this.numberofgears = numberofgears;
	}

	public float getGearup() {
		return gearup;
	}

	public void setGearup(float gearup) {
		this.gearup = gearup;
	}

	public float getGeardown() {
		return geardown;
	}

	public void setGeardown(float geardown) {
		this.geardown = geardown;
	}

	public float[] getGearratios() {
		return gearratios;
	}

	public void setGearratios(float[] gearratios) {
		this.gearratios = gearratios;
	}

	public float getTrackturnacceleration() {
		return trackturnacceleration;
	}

	public void setTrackturnacceleration(float trackturnacceleration) {
		this.trackturnacceleration = trackturnacceleration;
	}

	public float getTrackturnspeed() {
		return trackturnspeed;
	}

	public void setTrackturnspeed(float trackturnspeed) {
		this.trackturnspeed = trackturnspeed;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
