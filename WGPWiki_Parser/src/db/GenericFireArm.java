package db;

import java.util.ArrayList;
import java.util.List;

public class GenericFireArm implements BF2Object {

	private String code; // GenericFireArm Codename
	private String name; // A fancy name. Either localized or hudname
	private List<String> subtemplates;
	private List<Integer> firerates; // Possible Firerates: 0 = Semiauto, 1 = 3-Burst, 2 = Full Auto
	private int magno; // Number of mags
	private int magsize; // Size of each mag
	private float reload; // Time to reload in sec
	private float setup; // Time to setup in sec
	private float rpm; // Rounds per minute
	private List<Float> zooms; // List of zoom values (multiple if toggle-through)
	private String projectilename; // name of the projectile. References Projectile object in ProjectileDB
	private String tracername; // Name of tracer projectile. Currently not used for anything.
	private float muzzle; // Muzzle velocity in m/sec
	private int index; // Item index
	private String icon;
	private String geometry;
	private boolean rangefinder;

	private float mindev;
	private float[] firedev; // TODO: Implement parser!
	private float[] turndev;
	private float[] speeddev;
	private float[] miscdev;
	private float devmodstand;
	private float devmodcrouch;
	private float devmodlie;
	private float devmodzoom;

	private boolean hasRecoilForce;
	private String[] recoilForceUp; // usually is String/float/float/float, hence we keep it as String
	private String[] recoilForceLeftRight;
	private float recoilzoommodifier;
	private boolean goBackOnRecoil;
	private float cameraRecoilSpeed;
	private float cameraRecoilSize;

	public float getDevmodstand() {
		return devmodstand;
	}

	public void setDevmodstand(float devmodstand) {
		this.devmodstand = devmodstand;
	}

	public float getDevmodcrouch() {
		return devmodcrouch;
	}

	public void setDevmodcrouch(float devmodcrouch) {
		this.devmodcrouch = devmodcrouch;
	}

	public float getDevmodlie() {
		return devmodlie;
	}

	public void setDevmodlie(float devmodlie) {
		this.devmodlie = devmodlie;
	}

	public float getDevmodzoom() {
		return devmodzoom;
	}

	public void setDevmodzoom(float devmodzoom) {
		this.devmodzoom = devmodzoom;
	}

	public boolean isHasRecoilForce() {
		return hasRecoilForce;
	}

	public void setHasRecoilForce(boolean hasRecoilForce) {
		this.hasRecoilForce = hasRecoilForce;
	}

	public String[] getRecoilForceUp() {
		return recoilForceUp;
	}

	public void setRecoilForceUp(String[] recoilForceUp) {
		this.recoilForceUp = recoilForceUp;
	}

	public String[] getRecoilForceLeftRight() {
		return recoilForceLeftRight;
	}

	public void setRecoilForceLeftRight(String[] recoilForceLeftRight) {
		this.recoilForceLeftRight = recoilForceLeftRight;
	}

	public float getRecoilZoommodifier() {
		return recoilzoommodifier;
	}

	public void setRecoilZoommodifier(float zoommodifier) {
		this.recoilzoommodifier = zoommodifier;
	}

	public boolean isGoBackOnRecoil() {
		return goBackOnRecoil;
	}

	public void setGoBackOnRecoil(boolean goBackOnRecoil) {
		this.goBackOnRecoil = goBackOnRecoil;
	}

	public float getCameraRecoilSpeed() {
		return cameraRecoilSpeed;
	}

	public void setCameraRecoilSpeed(float cameraRecoilSpeed) {
		this.cameraRecoilSpeed = cameraRecoilSpeed;
	}

	public float getCameraRecoilSize() {
		return cameraRecoilSize;
	}

	public void setCameraRecoilSize(float cameraRecoilSize) {
		this.cameraRecoilSize = cameraRecoilSize;
	}

	public boolean isRangefinder() {
		return rangefinder;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public GenericFireArm(String code) {
		this.code = code;
		this.firerates = new ArrayList<Integer>();
		this.zooms = new ArrayList<Float>();
		this.subtemplates = new ArrayList<String>();
	}

	public List<Integer> getFirerates() {
		return firerates;
	}

	public void setFirerates(List<Integer> firerates) {
		this.firerates = firerates;
	}

	public void addFirerate(int x) {
		this.firerates.add(x);
	}

	public int getMagno() {
		return magno;
	}

	public void setMagno(int magno) {
		this.magno = magno;
	}

	public int getMagsize() {
		return magsize;
	}

	public void setMagsize(int magsize) {
		this.magsize = magsize;
	}

	public float getReload() {
		return reload;
	}

	public void setReload(float reload) {
		this.reload = reload;
	}

	public float getSetup() {
		return setup;
	}

	public void setSetup(float setup) {
		this.setup = setup;
	}

	public List<Float> getZooms() {
		return zooms;
	}

	public void setZooms(List<Float> zooms) {
		this.zooms = zooms;
	}

	public void addZoom(float x) {
		this.zooms.add(x);
	}

	public String getProjectilename() {
		return projectilename;
	}

	public void setProjectilename(String projectilename) {
		this.projectilename = projectilename;
	}

	public float getMuzzle() {
		return muzzle;
	}

	public void setMuzzle(float muzzle) {
		this.muzzle = muzzle;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTracername() {
		return tracername;
	}

	public void setTracername(String tracername) {
		this.tracername = tracername;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getCode() {
		return this.code;
	}

	public List<String> getSubTemplates() {
		return this.subtemplates;
	}

	public void setSubTemplates(List<String> newsubs) {
		this.subtemplates = newsubs;

	}

	public void addSubTemplate(String subtemplate) {
		this.subtemplates.add(subtemplate);
	}

	public void setRangefinder(boolean b) {
		this.rangefinder = b;

	}

	public boolean hasRangefinder() {
		return this.rangefinder;

	}

	public float getRPM() {
		return rpm;
	}

	public void setRPM(float rpm) {
		this.rpm = rpm;
	}

	public String getGeometry() {
		return geometry;
	}

	public void setGeometry(String geometry) {
		this.geometry = geometry;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof GenericFireArm))
			return false;
		if (obj == this)
			return true;
		GenericFireArm gfa = (GenericFireArm) obj;
		if (this.getClass().equals(gfa.getCode()))
			return true;
		else
			return false;

	}

	public float getMindev() {
		return mindev;
	}

	public void setMindev(float mindev) {
		this.mindev = mindev;
	}

	public float[] getTurndev() {
		return turndev;
	}

	public void setTurndev(float[] turndev) {
		this.turndev = turndev;
	}

	public float[] getSpeeddev() {
		return speeddev;
	}

	public void setSpeeddev(float[] speeddev) {
		this.speeddev = speeddev;
	}

	public float[] getMiscdev() {
		return miscdev;
	}

	public void setMiscdev(float[] miscdev) {
		this.miscdev = miscdev;
	}

	public float[] getFiredev() {
		return firedev;
	}

	public void setFiredev(float[] firedev) {
		this.firedev = firedev;
	}
	
	public String toString() {
		return this.getCode();
	}
}
