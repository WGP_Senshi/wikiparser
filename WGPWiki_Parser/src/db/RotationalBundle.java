package db;

import java.util.ArrayList;
import java.util.List;

public class RotationalBundle implements BF2Object {

	String code;
	private List<String> subtemplates = new ArrayList<String>();
	private float[] maxSpeed;
	private float[] acceleration;
	private float[] minRotation;
	private float[] maxRotation;
	private boolean automaticpitchstabilization;
	private float[] regulatePitch;
	private float[] regulateYaw;
	
	private float floaterMod;
	private boolean rememberExcessInput;
	private boolean hasMobilePhysics;
	private boolean hasCollisionPhysics;
	private String physicsType;

	public RotationalBundle(String code) {
		this.code = code;
	}

	@Override
	public List<String> getSubTemplates() {
		return subtemplates;
	}

	@Override
	public void setSubTemplates(List<String> subTemplates) {
		this.subtemplates = subTemplates;
	}

	@Override
	public void addSubTemplate(String subTemplate) {
		this.subtemplates.add(subTemplate);
	}

	public void getAddTemplate(int i) {
		this.subtemplates.get(i);
	}

	@Override
	public String getCode() {
		return this.code;
	}

	public float[] getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(float[] maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public float[] getAcceleration() {
		return acceleration;
	}

	public void setAcceleration(float[] acceleration) {
		this.acceleration = acceleration;
	}

	public float[] getMinRotation() {
		return minRotation;
	}

	public void setMinRotation(float[] minRotation) {
		this.minRotation = minRotation;
	}

	public float[] getMaxRotation() {
		return maxRotation;
	}

	public void setMaxRotation(float[] maxRotation) {
		this.maxRotation = maxRotation;
	}

	public boolean isAutomaticpitchstabilization() {
		return automaticpitchstabilization;
	}

	public void setAutomaticpitchstabilization(boolean automaticpitchstabilization) {
		this.automaticpitchstabilization = automaticpitchstabilization;
	}

	public float[] getRegulatePitch() {
		return regulatePitch;
	}

	public void setRegulatePitch(float[] regulatePitch) {
		this.regulatePitch = regulatePitch;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public float[] getRegulateYaw() {
		return regulateYaw;
	}

	public void setRegulateYaw(float[] regulateYaw) {
		this.regulateYaw = regulateYaw;
	}

	public float getFloaterMod() {
		return floaterMod;
	}

	public void setFloaterMod(float floaterMod) {
		this.floaterMod = floaterMod;
	}

	public boolean isRememberExcessInput() {
		return rememberExcessInput;
	}

	public void setRememberExcessInput(boolean rememberExcessInput) {
		this.rememberExcessInput = rememberExcessInput;
	}

	public boolean isHasMobilePhysics() {
		return hasMobilePhysics;
	}

	public void setHasMobilePhysics(boolean hasMobilePhysics) {
		this.hasMobilePhysics = hasMobilePhysics;
	}

	public boolean isHasCollisionPhysics() {
		return hasCollisionPhysics;
	}

	public void setHasCollisionPhysics(boolean hasCollisionPhysics) {
		this.hasCollisionPhysics = hasCollisionPhysics;
	}

	public String getPhysicsType() {
		return physicsType;
	}

	public void setPhysicsType(String physicsType) {
		this.physicsType = physicsType;
	}

}
