package parsers;
/**
 * Defines a FlagSpawner on maps.
 * @author Senshi
 * 
 * @param name 
 * @param posX width
 * @param posY altitude
 * @param posZ height
 * @param radius
 * @param team
 * @param cpID
 * @param ttgc Time to get control
 * @param ttlc Time to lose control
 * @param uncap unable to change team
 * @param sgID Supply Group ID
 * @param value1
 * @param value2
 * 
 */

public class FlagSpawner {
    private String name;
    private int radius;
    private int cpID;
    private int ttgc; // Time To Get Control
    private int ttlc; // Time To Lose Control
    private int sgID; // Supply Group ID
    private int uncap; // unabletochangeteam
    private int team;
    private float posX;
    private float posY;
    private float posZ;
    private int value1;
    private int value2;

    FlagSpawner(String name) {
        this.setName(name);
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public int getRadius() {
        return radius;
    }
    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getCPID() {
        return cpID;
    }
    public void setCPID(int cpID) {
        this.cpID = cpID;
    }

    public int getTTGC() {
        return ttgc;
    }
    public void setTTGC(int ttgc) {
        this.ttgc = ttgc;
    }

    public int getTTLC() {
        return ttlc;
    }
    public void setTTLC(int ttlc) {
        this.ttlc = ttlc;
    }

    public int getSGID() {
        return sgID;
    }
    public void setSGID(int sgID) {
        this.sgID = sgID;
    }

    public int getUncap() {
        return uncap;
    }
    public void setUncap(int uncap) {
        this.uncap = uncap;
    }

    public float getPosX() {
        return posX;
    }
    public void setPosX(float f) {
        this.posX = f;
    }

    public float getPosY() {
        return posY;
    }
    public void setPosY(float posy) {
        this.posY = posy;
    }

    public int getTeam() {
        return team;
    }

    public void setTeam(int team) {
        this.team = team;
    }

    public int getValue1() {
        return value1;
    }

    public void setValue1(int value1) {
        this.value1 = value1;
    }

    public int getValue2() {
        return value2;
    }

    public void setValue2(int value2) {
        this.value2 = value2;
    }

    public float getPosZ() {
        return posZ;
    }

    public void setPosZ(float posZ) {
        this.posZ = posZ;
    }
    
    @Override
	public String toString() {
        //@override
        return name + " |PosX: " + posX + "|PosY: " + posY +  " |posZ: " + posZ + " |Team: " + team + " |Radius " + radius;
    }
}
