package parsers.map;

import gui.Main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.widgets.Text;

import parsers.FlagSpawner;
import db.VehicleSpawner;

public class parseMapGamemodes {

	// PRIVATE
	static String spawnsave; // saves minspawntime
	public static List<VehicleSpawner> vehicles;
	public static List<FlagSpawner> flags;
	private static String initmode;

	private static void log(Text target, Object aObject) {
		/**
		 * Prints object to first log window
		 */
		target.insert(aObject.toString());
	}

	private static void log1(Object aObject) {
		/**
		 * Prints object to first log window
		 */
		log(Main.log_team1, aObject.toString());
	}

	private static void log2(Object aObject) {
		/**
		 * Prints object to first log window
		 */
		log(Main.log_team2, aObject.toString());
	}

	public static String[] prepLine(BufferedReader scanner) throws IOException {
		/**
		 * Prepares a line for parsing: Removes leading whitespaces, splits line
		 * into tokens separated by whitespaces and returns tokens.
		 */
		String line = scanner.readLine();
		if (line == null) // Eject if end of file
			return null;
		line = line.replaceAll("^\\s+", ""); // remove leading spaces which are
												// useless and only confuse the
												// parser
		String[] tokens = line.split("\\s"); // split on spaces
		return tokens;
	}

	public static void processFile(File filename, String initmodus) throws FileNotFoundException {
		/**
		 * Controls the line by line processing feed
		 */
		initmode = initmodus;
		String mapname = filename.getParentFile().getParentFile().getParentFile().getParentFile().getName(); // asad_khal
		String gamemodename = filename.getParentFile().getParentFile().getName();
		String gamesizename = filename.getParentFile().getName();
		String gamemodeproper = "";
		String gamesizeproper = "";
		if (gamemodename.equalsIgnoreCase("gpm_skirmish"))
			gamemodeproper = "Skirmish";
		else if (gamemodename.equalsIgnoreCase("gpm_cq")) {
			gamemodename = "gpm_aas";
			gamemodeproper = "Assault and Secure";
		} else if (gamemodename.equalsIgnoreCase("gpm_coop"))
			gamemodeproper = "Cooperative";
		else if (gamemodename.equalsIgnoreCase("gpm_insurgency"))
			gamemodeproper = "Insurgency";
		else if (gamemodename.equalsIgnoreCase("gpm_skirmish"))
			gamemodeproper = "Skirmish";
		else if (gamemodename.equalsIgnoreCase("gpm_vehicles"))
			gamemodeproper = "Vehicle Warfare";
		else if (gamemodename.equalsIgnoreCase("gpm_cnc"))
			gamemodeproper = "Command And Conquer";
		if (gamesizename.equalsIgnoreCase("16")) {
			gamesizename = "inf";
			gamesizeproper = "Infantry";
		} else if (gamesizename.equalsIgnoreCase("32")) {
			gamesizename = "alt";
			gamesizeproper = "Alternative";
		} else if (gamesizename.equalsIgnoreCase("64")) {
			gamesizename = "std";
			gamesizeproper = "Standard";
		} else if (gamesizename.equalsIgnoreCase("128")) {
			gamesizename = "lrg";
			gamesizeproper = "Large";
		}
		log1("{{clear}}\n");
		log1("== " + gamemodeproper + " " + gamesizeproper + " ==\n");
		log1("[[File:" + mapname + "_" + gamemodename + "_" + gamesizename + ".jpg||thumb|right|" + gamemodeproper + " " + gamesizeproper + "|300px]]\n");

		// Note that FileReader is used, not File, since File is not Closeable
		FileReader fr = new FileReader(filename);
		BufferedReader scanner = new BufferedReader(fr);
		vehicles = new ArrayList<VehicleSpawner>();
		flags = new ArrayList<FlagSpawner>();

		try {
			String line = "x";

			// Loops over ALL LINES
			while (line != null) {
				boolean breakflag = false;
				scanner.mark(2000);
				String[] tokens = prepLine(scanner);
				if (tokens == null)
					break;

				if (breakflag)
					break;

				// If create is found, a new line/token loop is started that
				// iterates over the full object, keeping data coherent
				if (tokens[0].equalsIgnoreCase("ObjectTemplate.create")) {
					if (tokens[1].equalsIgnoreCase("ObjectSpawner")) {
						processObjectTemplate(scanner, tokens);
						// parse rudimentary FLAG info as requested by Excalibur

					} else if (tokens[1].equalsIgnoreCase("ControlPoint")) {
						processFlagTemplate(scanner, tokens);
					}
				} else if (tokens[0].equalsIgnoreCase("Object.create")) {
					for (int i = 0; i < vehicles.size(); i++)
						if (vehicles.get(i).getName().equalsIgnoreCase(tokens[1])) {
							processObjectSpawner(scanner, tokens, i);
							break;
						}
					for (int i = 0; i < flags.size(); i++)
						if (flags.get(i).getName().equalsIgnoreCase(tokens[1])) {
							processFlagSpawner(scanner, tokens, i);
							break;
						}

				}

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// ensure the underlying stream is always closed
			try {
				scanner.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// Print vehicle lists for both teams
		printvehicles();
		// TODO: Print flag layout - TEMPORARY: Should be moved to minimap
		// window!
		// printflags();
		// chooser.log_team1.setSelection(0);
		// chooser.log_team1.setSelection(1);
	}

	private static void processFlagSpawner(BufferedReader scanner, String[] firstline, int index) throws IOException {
		FlagSpawner flag = flags.get(index);

		String line = "x";
		while (line != null) {

			scanner.mark(200);
			String[] tokens = prepLine(scanner);
			if (tokens == null)
				break;
			if (tokens[0].equalsIgnoreCase("ObjectTemplate.create") || tokens[0].equalsIgnoreCase("Object.create")) {
				scanner.reset();
				break;
			}
			if (tokens[0].equalsIgnoreCase("Object.absolutePosition")) {
				String[] position = tokens[1].split("/");
				flag.setPosX(Float.parseFloat(position[0]));
				flag.setPosY(Float.parseFloat(position[1]));
				flag.setPosZ(Float.parseFloat(position[2]));
			}

		}
		flags.remove(index);
		flags.add(index, flag);

	}

	private static void processObjectSpawner(BufferedReader scanner, String[] firstline, int index) {
		// TODO Auto-generated method stub Process Object positions and add them
		// to vehicles

	}

	public static void processObjectTemplate(BufferedReader scanner, String[] firstline) throws IOException {
		// Parser for when it's an objectspawner template...

		int listindex = -999;
		String spawnername = firstline[2];
		VehicleSpawner vehicle = new VehicleSpawner(spawnername);
		// System.out.println("BLa " + vehicles.isEmpty() + " bla");
		if (!vehicles.isEmpty()) {
			for (int i = 0; i < vehicles.size(); i++) {
				// Verifies if vehicle already has been created. If this is
				// true, the vehicle will be loaded. If not, a new vehicle will
				// be created.
				if (vehicles.get(i).equals(vehicle)) {
					vehicle = vehicles.get(i);
					listindex = i;
				}
			}
		}

		String line = "x";

		while (line != null) {
			scanner.mark(2000); // Puts a marker here, this is only to catch the
								// beginning of a new object
			String[] tokens = prepLine(scanner);
			if (tokens == null)
				break;

			// Rem means comment, no need to look at this.
			if (tokens[0].equalsIgnoreCase("rem")) {
				continue;
			}
			// On reaching the next object, reset to beginning of line, reset
			// scanner so next object can be parsed and break loop
			if (tokens[0].equalsIgnoreCase("ObjectTemplate.create") || tokens[0].equalsIgnoreCase("Object.create")) {
				scanner.reset();
				break;
			}
			try {
				if (tokens[0].equalsIgnoreCase("ObjectTemplate.setObjectTemplate")) {
					if (tokens[1].equalsIgnoreCase("1")) {
						vehicle.setObject1(tokens[2]);
					} else if (tokens[1].equalsIgnoreCase("2")) {
						vehicle.setObject2(tokens[2]);
					}
				} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.minSpawnDelay")) {
					vehicle.setSpawnMin(Integer.parseInt(tokens[1]));
				} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.maxSpawnDelay")) {
					vehicle.setSpawnMax(Integer.parseInt(tokens[1]));
				} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.spawnDelayAtStart")) {
					if (Integer.parseInt(tokens[1]) == 1) {
						vehicle.setSpawnDelayAtStart(true);
					} else {
						vehicle.setSpawnDelayAtStart(false);
					}
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("ArrayIndexError in " + line);
			}
		}
		if (listindex == -999)
			vehicles.add(vehicle);
		else
			vehicles.add(listindex, vehicle);
	}

	public static void processFlagTemplate(BufferedReader scanner, String[] firstline) throws IOException {
		int listindex = -999;
		String spawnername = firstline[2];
		System.out.println(spawnername);
		FlagSpawner flag = new FlagSpawner(spawnername);
		if (!flags.isEmpty()) {
			for (int i = 0; i < flags.size(); i++) {
				// Verifies if flag already has been created. If this is true,
				// the flag will be loaded. If not, a new flag will be created.
				if (flags.get(i).equals(flag)) {
					flag = flags.get(i);
					listindex = i;
				}
			}
		}

		String line = "x";
		while (line != null) {

			scanner.mark(2000);
			String[] tokens = prepLine(scanner);
			if (tokens == null)
				break;
			if (tokens[0].equalsIgnoreCase("ObjectTemplate.create") || tokens[0].equalsIgnoreCase("Object.create")) {
				scanner.reset();
				break;
			}
			if (tokens[0].equalsIgnoreCase("ObjectTemplate.team"))
				flag.setTeam(Integer.parseInt(tokens[1]));
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.controlpointid"))
				flag.setCPID(Integer.parseInt(tokens[1]));
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.timeToGetControl"))
				flag.setTTGC(Integer.parseInt(tokens[1]));
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.timetoloseControl"))
				flag.setTTLC(Integer.parseInt(tokens[1]));
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.supplyGroupID"))
				flag.setSGID(Integer.parseInt(tokens[1]));
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.areaValueTeam1"))
				flag.setValue1(Integer.parseInt(tokens[1]));
			else if (tokens[0].equalsIgnoreCase("objectTemplate.areaValueTeam2"))
				flag.setValue2(Integer.parseInt(tokens[1]));
			else if (tokens[0].equalsIgnoreCase("Objecttemplate.unabletochangeteam"))
				flag.setUncap(Integer.parseInt(tokens[1]));
			if (tokens[0].equalsIgnoreCase("Objecttemplate.radius"))
				flag.setRadius(Integer.parseInt(tokens[1]));

		}

		System.out.println("Flag SGID: " + flag.getSGID());
		if (listindex == -999)
			flags.add(flag);
		else
			flags.add(listindex, flag);
	}

	public static void printTeamVehicles(int teamID, Map<String, Integer> frequencies) {
		// Objects that should be excluded on the wiki
		ArrayList<String> exclude = new ArrayList<String>();
		exclude.add("fixed_supply_crate"); // Supply crates
		exclude.add("_acv_"); // UAV command vehicle
		exclude.add("rallypoint_"); // Rally points
		exclude.add("bipod"); // stationary MGs
		exclude.add("vehicle_depot"); // Base vehicle depot
		exclude.add("dummy"); // well, obvious
		exclude.add("commandpost"); // FOB
		exclude.add("command_post"); // FOB
		exclude.add("ammocache"); // small ammo cache
		exclude.add("_pickup"); // pickup kits

		// Vehicles we have already processed
		ArrayList<String> done = new ArrayList<String>();

		String[] currentteam;
		if (teamID == 1) {
			currentteam = Main.team1;
		} else {
			currentteam = Main.team2;
		}
		String currentmode = "";
		if (initmode == "insurgency") {
			currentmode = currentteam[2];
			if (currentteam[2] == "")
				currentmode = currentteam[0];
		} else if (initmode == "vehicles") {
			currentmode = currentteam[1];
			if (currentteam[1] == "")
				currentmode = currentteam[0];
		} else {
			currentmode = currentteam[0];
		}
		log1("{| class='wikitable' style='width:300px; float:left;'\n"
				+ "!  colspan='4'| {{Flag|" + currentmode + "}} Vehicles\n"
				+ "|-\n" + "! Vehicle "
				+ "!! style='width:55px; max-width:55px' | Amount "
				+ "!! style='width:45px; max-width:45px' | Spawn time "
				+ "!! style='width:45px; max-width:45px' | Spawn delay\n"
				+ "<span style='vertical-align:middle;'>\n");

		for (int p = 0; p < vehicles.size(); p++) {
			VehicleSpawner vehicle = vehicles.get(p);
			String editorcode = null;
			if (teamID == 1)
				editorcode = vehicle.getObject1();
			else
				editorcode = vehicle.getObject2();

			if (editorcode != null && editorcode != "") {
				String name = editorcode + "_" + String.valueOf((vehicle.getSpawnMax() + vehicle.getSpawnMin()) / 2) + String.valueOf(vehicle.getSpawnDelayAtStart());
				boolean flag = false;
				if (done.contains(name)) { // Dont log if vehicle already has
											// been done
					continue;
				}
				;
				for (int e = 0; e < exclude.size(); e++)
					// Dont log if vehicle is to be explicitly excluded
					// if
					// (vehicle.getObject1().compareToIgnoreCase(exclude.get(e))
					// == 0)
					if (editorcode.contains(exclude.get(e)))
						flag = true;
				if (flag)
					continue;

				int count = frequencies.get(name);

				// return MEAN of both min and max (is the same in most cases)
				float spawntime = ((float) vehicle.getSpawnMax() + (float) vehicle.getSpawnMin()) / 2;
				spawntime = spawntime / 60; // get spawn in minutes
				// If delay is set insanely high (==infinity)
				if (spawntime >= 600.0)
					spawntime = -1;
				String spawndelay = "";
				String realname = Main.lang.getValue(editorcode.toLowerCase());
				if (realname.equalsIgnoreCase(editorcode))
					System.out.println("WARNING: Vehicle " + realname + " has no localization!");
				if (vehicle.getSpawnDelayAtStart())
					spawndelay = "&#x2713;";
				log1("|-\n"
					+ "|[[" + editorcode + "|" + realname + "]]\n"
					+ "|style='text-align: right;'|" + count + "\n"
					+ "|style='text-align: right;'|" + String.format("%.2f", spawntime) + "min\n"
					+ "|style='text-align: center;'|" + spawndelay + "\n");
				// log1("\n|editorcode" + no1 + " = " + vehicle.getObject1() +
				// " |amount" + no1 + " = " + count + " |spawn" + no1 + " = " +
				// spawntime + " |delay"
				// + no1 + " = " + spawndelay);
				done.add(name);
			}
		}
		log1("</span>\n|}\n");

	}

	public static void printvehicles() {
		// Counts number of occurrences of all individual vehicles (incl.
		// spawntime, cpid, spawndelay-ID)
		Map<String, Integer> frequencymap = new HashMap<String, Integer>();
		for (VehicleSpawner v : vehicles) {
			String name1 = v.getObject1() + "_" + String.valueOf((v.getSpawnMax() + v.getSpawnMin()) / 2) + String.valueOf(v.getSpawnDelayAtStart());
			String name2 = v.getObject2() + "_" + String.valueOf((v.getSpawnMax() + v.getSpawnMin()) / 2) + String.valueOf(v.getSpawnDelayAtStart());

			if (frequencymap.containsKey(name1))
				frequencymap.put(name1, frequencymap.get(name1) + 1);
			else
				frequencymap.put(name1, 1);

			if (frequencymap.containsKey(name2))
				frequencymap.put(name2, frequencymap.get(name2) + 1);
			else
				frequencymap.put(name2, 1);

		}
		//Sort alphabetically
		Collections.sort(vehicles, new Comparator<VehicleSpawner>() {
					@Override
					public int compare(VehicleSpawner v1, VehicleSpawner v2) {
						String name1 = "";
						String name2 = "";
						if (v1.getObject1() != null)
							name1 = v1.getObject1();
						if (v2.getObject1() != null)
							name2 = v2.getObject1();

						return name1.compareTo(name2);
					}

				});
		printTeamVehicles(1, frequencymap);
		printTeamVehicles(2, frequencymap);
	}

	public static void printflags() {
		// parse rudimentary FLAG info as requested by Excalibur
		/*
		 * log1("\n\nXXXXXXXXXXXXX FLAGS"); for (FlagSpawner f : flags) {
		 * log1("\n\nFlagname: " + f.getName()); log1("\nTeam: " + f.getTeam());
		 * log1("\nRadius: " + f.getRadius()); log1("\nControl Point ID: " +
		 * f.getCPID()); log1("\nTime to get control: " + f.getTTGC());
		 * log1("\nTime to lose control: " + f.getTTLC());
		 * log1("\nSupply Group ID: " + f.getSGID());
		 * log1("\nFlag Value Team 1: " + f.getValue1());
		 * log1("\nFlag Value Team 2: " + f.getValue2()); if (f.getUncap() == 1)
		 * log1("\nNOT CAPTUREABLE!");
		 * 
		 * }
		 */
	}
}