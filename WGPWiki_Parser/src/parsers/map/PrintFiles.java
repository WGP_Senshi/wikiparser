package parsers.map;

import static java.nio.file.FileVisitResult.CONTINUE;
import gui.Main;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import org.eclipse.swt.SWT;

import common.Utils;

public class PrintFiles extends SimpleFileVisitor<Path> {

	private int counter;
	public common.PBar bar = new common.PBar(SWT.SMOOTH);

	public PrintFiles() {
		counter = 0;
		String dir_chosen = "";
		dir_chosen = Main.props.getProperty("MapPath");
		int no_files = Utils.countFilesInDirectory(new File(dir_chosen));
		bar.setMaximum(no_files);
		bar.setMinimum(0);
		bar.setText("Parsing " + no_files + " files, please be patient.");

	}

	// Print information about each type of file.
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
		counter++;
		bar.setSelection(counter++);
		Path name = file.getFileName();

		if (name != null && Main.matcher.matches(name)) {
			// for (int i = 0; i < 128; i++)
			if (Main.lang.getValue("HUD_LEVELNAME_" + name.toString().substring(0, name.toString().lastIndexOf("."))) != null)
				Main.list_m.add(Main.lang.getValue("HUD_LEVELNAME_" + name.toString().substring(0, name.toString().lastIndexOf("."))));
			else
				Main.list_m.add(name.toString().substring(0, name.toString().lastIndexOf(".")));
			Main.maplist.add(file);
			Main.log_team1.insert("Found " + name + "\n");
		}
		return CONTINUE;
	}

	// Print each directory visited.
	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
		return CONTINUE;
	}

	// If there is some error accessing the file, let the user know.
	// If you don't override this method and an error occurs, an IOException is thrown.
	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) {
		System.err.println(exc);
		return CONTINUE;
	}

}