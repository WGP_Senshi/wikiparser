package parsers.map;

import gui.Main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class parseMaps {

    // PRIVATE
    private final File fFile;
    public static int no = 1; // Dateizaehler
    private static int gamemodeno = 1; // gamemode-Zaehler
    private static int gamemodexno = 1; // gamemodex-Zaehler

    private static void log(Object aObject) {
        Main.log_team1.insert(aObject.toString() + "\n");
        // System.out.println(String.valueOf(aObject));
    }

    private static void log2(Object aObject) {
        Main.log_team2.insert(aObject.toString() + "\n");
        // System.out.println(String.valueOf(aObject));
    }

    private static void log_gm(Object aObject) {
        Main.gamemode_list.add(Main.lang.getValue(aObject.toString()));
        Main.filelist_gm.add(aObject.toString());
        // System.out.println(String.valueOf(aObject));
    }

    public static String processFile(String filename) throws FileNotFoundException {
        Main.filelist_gm.clear();
        log("{{Version|" + Main.PR_version + "}}\n");
        log("{{Infobox Map");
        parseMaps parser = new parseMaps(filename);
        String[] dummy = filename.split("\\\\|\\.");
        String mapname = "";
        if (dummy.length != 0) {
            mapname = dummy[dummy.length - 2];
            log("|mapname = " + mapname);
        }
        parseMapsinit.processFile(filename);
        parser.processLineByLinedesc();
        log("}}__TOC__");
        no++;
        gamemodeno = 1;
        gamemodexno = 1;

        String[] dummy2 = filename.split("\\\\");

        System.out.println(Main.filelist_gm.size());
        for (int i = 0; i < Main.filelist_gm.size(); i++) {
            String gamemodepath = dummy2[0];
            for (int j = 1; !dummy2[j].toLowerCase().equals("info"); j++) {
                gamemodepath = gamemodepath + "\\" + dummy2[j];
            }
            gamemodepath = gamemodepath + "\\GameModes";
            String initmodus = "";
            String gm = Main.filelist_gm.get(i);
            System.out.println(gm);
            switch (gm) {
            case "gpm_cnc_inf":
                gamemodepath = gamemodepath + "\\gpm_cnc\\16\\GamePlayObjects.con";
                break;
            case "gpm_cnc_alt":
                gamemodepath = gamemodepath + "\\gpm_cnc\\32\\GamePlayObjects.con";
                break;
            case "gpm_cnc_std":
                gamemodepath = gamemodepath + "\\gpm_cnc\\64\\GamePlayObjects.con";
                break;
            case "gpm_aas_inf":
                gamemodepath = gamemodepath + "\\gpm_cq\\16\\GamePlayObjects.con";
                break;
            case "gpm_aas_alt":
                gamemodepath = gamemodepath + "\\gpm_cq\\32\\GamePlayObjects.con";
                break;
            case "gpm_aas_std":
                gamemodepath = gamemodepath + "\\gpm_cq\\64\\GamePlayObjects.con";
                break;
            case "gpm_aas_128":
                gamemodepath = gamemodepath + "\\gpm_cq\\128\\GamePlayObjects.con";
                break;
            case "gpm_insurgency_inf":
                initmodus = "insurgency";
                gamemodepath = gamemodepath + "\\gpm_insurgency\\16\\GamePlayObjects.con";
                break;
            case "gpm_insurgency_alt":
                initmodus = "insurgency";
                gamemodepath = gamemodepath + "\\gpm_insurgency\\32\\GamePlayObjects.con";
                break;
            case "gpm_insurgency_std":
                initmodus = "insurgency";
                gamemodepath = gamemodepath + "\\gpm_insurgency\\64\\GamePlayObjects.con";
                break;
            case "gpm_skirmish_inf":
                gamemodepath = gamemodepath + "\\gpm_skirmish\\16\\GamePlayObjects.con";
                break;
            case "gpm_skirmish_alt":
                gamemodepath = gamemodepath + "\\gpm_skirmish\\32\\GamePlayObjects.con";
                break;
            case "gpm_skirmish_std":
                gamemodepath = gamemodepath + "\\gpm_skirmish\\64\\GamePlayObjects.con";
                break;
            case "gpm_vehicles_inf":
                initmodus = "vehicles";
                gamemodepath = gamemodepath + "\\gpm_vehicles\\16\\GamePlayObjects.con";
                break;
            case "gpm_vehicles_alt":
                initmodus = "vehicles";
                gamemodepath = gamemodepath + "\\gpm_vehicles\\32\\GamePlayObjects.con";
                break;
            case "gpm_vehicles_std":
                initmodus = "vehicles";
                gamemodepath = gamemodepath + "\\gpm_vehicles\\64\\GamePlayObjects.con";
                break;
            case "gpm_coop_inf":
                gamemodepath = gamemodepath + "\\gpm_coop\\16\\GamePlayObjects.con";
                break;
            case "gpm_coop_alt":
                gamemodepath = gamemodepath + "\\gpm_coop\\32\\GamePlayObjects.con";
                break;
            case "gpm_coop_std":
                gamemodepath = gamemodepath + "\\gpm_coop\\64\\GamePlayObjects.con";
                break;
            }
            File gamemodefile = new File(gamemodepath);
            if (gamemodefile.exists()) {
                try {
                    parseMapGamemodes.processFile(gamemodefile, initmodus);
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        }

        log("{{Navbox}}");
        return filename;
    }

    public parseMaps(String aFileName) {
        fFile = new File(aFileName);
    }

    // Controls the line by line processing feed
    public final void processLineByLinedesc() throws FileNotFoundException {
        // Note that FileReader is used, not File, since File is not Closeable
        Scanner scanner = new Scanner(new FileReader(fFile));
        try {
            // first use a Scanner to get each line
            while (scanner.hasNextLine()) {
                processLinedesc(scanner.nextLine());
            }
        } finally {
            // ensure the underlying stream is always closed
            // this only has any effect if the item passed to the Scanner
            // constructor implements Closeable (which it does in this case).
            // Chooser.log.insert("\n");
            scanner.close();
        }
    }

    /**
     * Processes a single .desc line
     */
    protected void processLinedesc(String aLine) {
        // use a second Scanner to parse the content of each line
        Scanner scanner = new Scanner(aLine);
        // scanner.useDelimiter("");
        if (scanner.hasNext()) {

            // String test = scanner.next();
            String test = aLine;
            if (test.contains("<maptype players=\"16\" type=\"headon\"")) {
                log("|gamemode" + gamemodeno + " = " + "gpm_aas_inf");
                log_gm("gpm_aas_inf");
                gamemodeno++;
            } else if (test.contains("<maptype players=\"32\" type=\"headon\"")) {
                log("|gamemode" + gamemodeno + " = " + "gpm_aas_alt");
                log_gm("gpm_aas_alt");
                gamemodeno++;
            } else if (test.contains("<maptype players=\"64\" type=\"headon\" locid=\"GAMEMODE_DESCRIPTION_aasfullassets\">")) {
                log("|gamemode" + gamemodeno + " = " + "gpm_aas_std");
                log_gm("gpm_aas_std");
                gamemodeno++;
            } else if (test.contains("<maptype players=\"128\" type=\"headon\"")) {
                log("|gamemode" + gamemodeno + " = " + "gpm_aas_128");
                log_gm("gpm_aas_128");
                gamemodeno++;
            } else if (test.contains("<maptype players=\"16\" type=\"skirmish\"")) {
                log("|gamemode" + gamemodeno + " = " + "gpm_skirmish_inf");
                log_gm("gpm_skirmish_inf");
                gamemodeno++;
            } else if (test.contains("<maptype ai=\"1\" players=\"16\" type=\"headon\"")) {
                log("|gamemode" + gamemodeno + " = " + "gpm_coop_inf");
                log_gm("gpm_coop_inf");
                gamemodeno++;
            } else if (test.contains("<maptype ai=\"1\" players=\"32\" type=\"headon\"")) {
                log("|gamemode" + gamemodeno + " = " + "gpm_coop_alt");
                log_gm("gpm_coop_alt");
                gamemodeno++;
            } else if (test.contains("<maptype ai=\"1\" players=\"64\" type=\"headon\"")) {
                log("|gamemode" + gamemodeno + " = " + "gpm_coop_std");
                log_gm("gpm_coop_std");
                gamemodeno++;
            } else if (test.contains("<maptype players=\"16\" type=\"cnc\"")) {
                log("|gamemode" + gamemodeno + " = " + "gpm_cnc_inf");
                log_gm("gpm_cnc_inf");
                gamemodeno++;
            } else if (test.contains("<maptype players=\"32\" type=\"cnc\"")) {
                log("|gamemode" + gamemodeno + " = " + "gpm_cnc_alt");
                log_gm("gpm_cnc_alt");
                gamemodeno++;
            } else if (test.contains("<maptype players=\"64\" type=\"cnc\"")) {
                log("|gamemode" + gamemodeno + " = " + "gpm_cnc_std");
                log_gm("gpm_cnc_std");
                gamemodeno++;
            } else if (test.contains("<maptype players=\"64\" type=\"headon\" locid=\"GAMEMODE_DESCRIPTION_vehiclewarfare\"")) {
                log("|gamemode" + gamemodeno + " = " + "gpm_vehicles_std");
                log_gm("gpm_vehicles_std");
                gamemodeno++;
            } else if (test.contains("<maptype players=\"16\" type=\"insurgency\"")) {
                log("|gamemode_x" + gamemodexno + " = " + "gpm_insurgency_inf");
                log_gm("gpm_insurgency_inf");
                gamemodexno++;
            } else if (test.contains("<maptype players=\"32\" type=\"insurgency\"")) {
                log("|gamemode_x" + gamemodexno + " = " + "gpm_insurgency_alt");
                log_gm("gpm_insurgency_alt");
                gamemodexno++;
            } else if (test.contains("<maptype players=\"64\" type=\"insurgency\"")) {
                log("|gamemode_x" + gamemodexno + " = " + "gpm_insurgency_std");
                log_gm("gpm_insurgency_std");
                gamemodexno++;
            }
            // log("Empty or invalid line. Unable to process.");
        }
        scanner.close();
    }

}