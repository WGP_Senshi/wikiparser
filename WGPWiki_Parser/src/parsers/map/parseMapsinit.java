package parsers.map;

import gui.Main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class parseMapsinit {

    // PRIVATE
    public static int no = 1; // Dateizaehler

    private static void log(Object aObject) {
        Main.log_team1.insert(aObject.toString() + "\n");
    }

    public static String processFile(String filename) throws FileNotFoundException {
        String modus;
        String[] dummy = filename.split("\\\\");
        String initname = dummy[0];
        for (int i = 1; !dummy[i].toLowerCase().equals("info"); i++) {
            initname = initname + "\\" + dummy[i];
        }
        File initpath = new File(initname + "\\init.con");
        File initvehiclepath = new File(initname + "\\init_vehicles.con");
        File initinsurgencypath = new File(initname + "\\init_insurgency.con");
        File heightdatapath = new File(initname + "\\Heightdata.con");
        if (initpath.exists()) {
            processLineByLine_init(initpath, "");
        }
        
        if (initvehiclepath.exists()) {
            modus = "vehicles";

            System.out.println(initvehiclepath);
            processLineByLine_init(initvehiclepath, modus);
        }
        if (initinsurgencypath.exists()) {
            modus = "insurgency";
            processLineByLine_init(initinsurgencypath, modus);
        }
        if (heightdatapath.exists()) {
            modus = "heightdatapath";
            processLineByLine_init(heightdatapath, modus);
        }
        no++;
        return filename;
    }

    // Controls the line by line processing feed
    public final static void processLineByLine_init(File filename, String modus) throws FileNotFoundException {
        // Note that FileReader is used, not File, since File is not Closeable
        Scanner scanner = new Scanner(new FileReader(filename));
        try {
            // first use a Scanner to get each line
            while (scanner.hasNextLine()) {
                processLine_init(scanner.nextLine(), modus);
            }
        } finally {
            // ensure the underlying stream is always closed
            // this only has any effect if the item passed to the Scanner
            // constructor implements Closeable (which it does in this case).
            // Chooser.log.insert("\n");
            scanner.close();
        }
    }

    /**
     * Processes a single .desc line
     */
    static void processLine_init(String aLine, String modus) {
        // use a second Scanner to parse the content of each line
        Scanner scanner = new Scanner(aLine);
        scanner.useDelimiter(" ");
        if (scanner.hasNext()) {

            String test = scanner.next();

            if (test.contains("rem")) {
                scanner.close();
                return;
            } else if (test.contains("run")) {
                test = scanner.next();
                if (test.contains("../../Factions/faction_init.con")) {
                    test = scanner.next();
                    if (test.contains("1")) {
                        String dummy = scanner.next();
                        String[] dummy2 = dummy.split("\"");
                        if (modus == "") {
                            Main.team1[0] = dummy2[1];
                            log("|faction1_1 = " + dummy2[1]);
                        } else if (modus == "vehicles") {
                            Main.team1[1] = dummy2[1];
                            log("|faction1_2 = " + dummy2[1]);
                        } else if (modus == "insurgency") {
                            Main.team1[2] = dummy2[1];
                            log("|faction1_2 = " + dummy2[1]);
                        }
                    } else if (test.contains("2")) {
                        String dummy = scanner.next();
                        String[] dummy2 = dummy.split("\"");
                        if (modus == "") {
                            Main.team2[0] = dummy2[1];
                            log("|faction2_1 = " + dummy2[1]);
                        } else if (modus == "vehicles") {
                            Main.team2[1] = dummy2[1];
                            log("|faction2_2 = " + dummy2[1]);
                        } else if (modus == "insurgency") {
                            Main.team2[2] = dummy2[1];
                            log("|faction2_2 = " + dummy2[1]);
                        }
                    }
                }
            } else if (test.contains("GameLogic.MaximumLevelViewDistance")) {
                String dummy = scanner.next();
                dummy.replace(".", ",");
                log("|viewrange = " + dummy);
            } else if (test.contains("heightmapcluster.setHeightmapSize")) {
                String dummy = scanner.next();
                Main.mapsize = Integer.parseInt(dummy);
                if (dummy.equals("512")) 
                    log("|mapsize = 0.5");
                else if (dummy.equals("1024"))
                    log("|mapsize = 1");
                else if (dummy.equals("2048"))
                    log("|mapsize = 2");
                else if (dummy.equals("4096"))
                    log("|mapsize = 4");
                else
                    log("|mapsize = ");
            }
        } else {
            // log("Empty or invalid line. Unable to process.");
        }
        scanner.close();
    }

}