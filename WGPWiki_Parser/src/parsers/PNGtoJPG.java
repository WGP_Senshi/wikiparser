package parsers;

import static java.nio.file.FileVisitResult.CONTINUE;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import javax.imageio.ImageIO;

public class PNGtoJPG extends SimpleFileVisitor<Path> {

	private static boolean overwrite = false;

	PNGtoJPG(Path path) {
	}

	public static void parse(Path path, boolean overwrite) {
		PNGtoJPG.overwrite = overwrite;
		PNGtoJPG pW = new PNGtoJPG(null);
		// System.out.println(dir);
		try {
			Files.walkFileTree(path, pW);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void convert(Path file) {
		if (file.toString().endsWith(".jpg"))
			return;
		BufferedImage bufferedImage;

		try {
			bufferedImage = ImageIO.read(file.toFile());

			// create a blank, RGB, same width and height, and a white background
			BufferedImage newBufferedImage = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
			newBufferedImage.createGraphics().drawImage(bufferedImage, 0, 0, Color.WHITE, null);

			// write to jpeg file
			String outstring = file.toString();
			outstring = outstring.substring(0, outstring.lastIndexOf(".")) + ".jpg";
			ImageIO.write(newBufferedImage, "jpg", new File(outstring));
			if (overwrite)
				file.toFile().delete();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			System.err.println(file);
			e.printStackTrace();
		}

	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		convert(file);
		return CONTINUE;
	}

}