package parsers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.Arrays;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.List;

import common.PBar;

public class extractContent implements common.Killable {

	static PBar bar;
	private volatile static boolean isRunning = true;

	public static void parse(final File path, final List l_log) {
		extractContent ec = new extractContent();
		Thread thread = new Thread() {

			@Override
			public void run() {
				try {
					File clientarchives = new File(path + File.separator + "clientarchives.con");
					File serverarchives = new File(path + File.separator + "serverarchives.con");
					BufferedReader scanner = new BufferedReader(new FileReader(clientarchives));
					String line = scanner.readLine();
					while (line != null) {
						if (!isRunning)
							break;
						String[] tokens = line.trim().split(" ");
						if (tokens.length != 3) {
							System.err.println("This line should have 3 tokens: " + Arrays.toString(tokens));
							continue;
						}
						unzipFile(new File(path + File.separator + tokens[1]).toPath(), new File(path + File.separator + tokens[2]).toPath());
						line = scanner.readLine();
					}
					scanner.close();
					scanner = new BufferedReader(new FileReader(serverarchives));
					line = scanner.readLine();
					while (line != null) {
						if (!isRunning)
							break;
						String[] tokens = line.trim().split(" ");
						if (tokens.length != 3) {
							System.err.println("This line should have 3 tokens: " + Arrays.toString(tokens));
							continue;
						}
						File zipfile = new File(path + File.separator + tokens[1]);
						unzipFile(zipfile.toPath(), new File(path + File.separator + tokens[2]).toPath());
						zipfile.delete();

						line = scanner.readLine();
					}
					scanner.close();
					gui.Main.display.asyncExec(new Runnable() {
						public void run() {
							if (bar.bar.isDisposed())
								return;
							bar.dispose();
							l_log.add("Content successfully extracted!");
						}
					});

				} catch (IOException e) {

				}
			};

		};
		bar = new common.PBar(SWT.SMOOTH, ec);
		thread.start();

	}

	private static void unzipFile(final Path zipfile, Path output) throws IOException {
		byte[] buffer = new byte[8192];
		PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:*.{zip}");
		final Path name = zipfile.getFileName();
		if (matcher.matches(name)) {
			try {
				gui.Main.display.asyncExec(new Runnable() {
					public void run() {
						if (bar.bar.isDisposed())
							return;
						bar.setText("Extracting " + zipfile.getParent().getFileName() + File.separator + name);
						try {
							ZipFile zf = new ZipFile(zipfile.toString());
							bar.setMaximum(zf.size());
							zf.close();
						} catch (IOException e) {
							e.printStackTrace();
							return;
						}
					}
				});
				ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipfile.toString())));
				// get the zipped file list entry
				ZipEntry ze = zis.getNextEntry();
				int no = 0;
				while (ze != null && isRunning) {
					no++;
					final int xno = no;

					String fileName = ze.getName();
					gui.Main.display.asyncExec(new Runnable() {
						public void run() {
							if (bar.bar.isDisposed())
								return;
							bar.setSelection(xno);
						}
					});
					File newFile = new File(output + File.separator + fileName);
					new File(newFile.getParent()).mkdirs();

					BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(newFile), buffer.length);
					int len;
					while ((len = zis.read(buffer)) > 0) {
						fos.write(buffer, 0, len);
					}

					fos.close();
					ze = zis.getNextEntry();
				}
				zis.closeEntry();
				zis.close();
			} catch (IOException e) {
				e.printStackTrace();
				return;

			}
		}

	}

	@Override
	public void kill() {
		System.out.println("Aborted!");
		isRunning = false;
	}

}
