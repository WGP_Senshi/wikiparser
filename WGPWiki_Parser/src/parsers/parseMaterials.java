package parsers;

import gui.Main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import org.eclipse.swt.widgets.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import db.Material;
import db.MaterialCell;
import db.MaterialDB;

public class parseMaterials {

    public static void parseSource(File dir, List log) {
        java.util.List<Material> materials;
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        log.add(dateFormat.format(new Date()) + " : Processing materials. This shouldn't take long.");
        log.getParent().update();
        try {
            File output = new File("db/materials.xml");
            materials = new ArrayList<Material>();
            materials = processDefines(materials, new File(dir.getAbsolutePath() + "\\materialmanagerdefine.con"));
            materials = processSettings(materials, new File(dir.getAbsolutePath() + "\\materialmanagersettings.con"));
            FileWriter fw_out = new FileWriter(output);
            // XStream xstream = new XStream(new StaxDriver()); // Would allow better stream-based processing (memory), BUT creates horrible to read single-line
            // output.
            // Opting for the fancier Dom for now.
            XStream xstream = new XStream(new DomDriver());

            xstream.alias("material", Material.class);
            xstream.alias("materialcell", db.MaterialCell.class);
            ObjectOutputStream out = xstream.createObjectOutputStream(fw_out, "materials");
            for (int i = 0; i < materials.size(); i++)
                out.writeObject(materials.get(i));
            out.close();

            final ObjectMapper mapper = new ObjectMapper();
            File f_out_json = new File("db/materials.json");
            BufferedWriter bw_json = new BufferedWriter(new FileWriter(f_out_json));
            mapper.writerWithDefaultPrettyPrinter().writeValue(bw_json, materials);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Main.materialDB = MaterialDB.readDB();
        log.add(dateFormat.format(new Date()) + " : Materials database has been rebuilt!");
    }

    static private java.util.List<Material> processDefines(java.util.List<Material> list, File file) throws FileNotFoundException {
        if (!file.exists()) {
            System.out.println("ERROR: " + file + " could not be found. Please ensure this file exists!");
        }

        Scanner s = new Scanner(new FileReader(file));
        Material mat = null;
        // first use a Scanner to get each line
        while (s.hasNextLine()) {
            String line = s.nextLine().toLowerCase();
            if (line.startsWith("material.active ")) {
                if (mat != null)
                    list.add(mat);
                mat = null;
                String[] tokens = line.split(" ");
                for (int i = 0; i < list.size(); i++) {
                    if (Integer.parseInt(tokens[1]) == list.get(i).getId()) {
                        mat = list.get(i);
                        list.remove(i);
                        break;
                    }
                }
                if (mat == null)
                    mat = new Material(Integer.parseInt(tokens[1]));
            } else if (line.startsWith("material.name "))
                mat.setName(line.split(" ")[1].replace("\"", ""));
            else if (line.startsWith("material.type "))
                mat.setType(Integer.parseInt(line.split(" ")[1]));
            else if (line.startsWith("material.friction "))
                mat.setFriction(Float.parseFloat(line.split(" ")[1]));
            else if (line.startsWith("material.elasticity "))
                mat.setElasticity(Float.parseFloat(line.split(" ")[1]));
            else if (line.startsWith("material.resistance "))
                mat.setResistance(Float.parseFloat(line.split(" ")[1]));
            else if (line.startsWith("material.damageloss "))
                mat.setDamageLoss(Float.parseFloat(line.split(" ")[1]));
            else if (line.startsWith("material.maxdamageloss "))
                mat.setMaxDamageLoss(Float.parseFloat(line.split(" ")[1]));
            else if (line.startsWith("material.mindamageloss "))
                mat.setMinDamageLoss(Float.parseFloat(line.split(" ")[1]));
            else if (line.startsWith("material.penetrationdeviation "))
                mat.setPenDeviation(Float.parseFloat(line.split(" ")[1]));
            else if (line.startsWith("material.overrideneverpenetrate ")) {
                if (line.split(" ")[1].equals(1))
                    mat.setOverrideNeverPenetrate(true);
                else
                    mat.setOverrideNeverPenetrate(false);
            } else if (line.startsWith("material.isonesided ")) {
                if (line.split(" ")[1].equals(1))
                    mat.setOneSided(true);
                else
                    mat.setOneSided(false);
            } else if (line.startsWith("material.projectilecollisionhardness "))
                mat.setProjectileCollisionHardness(Float.parseFloat(line.split(" ")[1]));
            else if (line.startsWith("material.haswaterphysics ")) {
                if (line.split(" ")[1].equals(1))
                    mat.setHasWaterPhysics(true);
                else
                    mat.setHasWaterPhysics(false);
            }
        }
        s.close();
        if (mat != null)
            list.add(mat);
        return list;
    }

    private static java.util.List<Material> processSettings(java.util.List<Material> list, File file) throws FileNotFoundException {
        if (!file.exists()) {
            System.out.println("ERROR: " + file + " could not be found. Please ensure this file exists!");
            return list;
        }

        Scanner s = new Scanner(new FileReader(file));
        Material mat = null;
        MaterialCell matcell = null;
        // first use a Scanner to get each line
        while (s.hasNextLine()) {
            String line = s.nextLine().toLowerCase();
            if (line.startsWith("materialmanager.createcell ")) {
                if (mat != null && matcell != null) {
                    mat.getCells().add(matcell);
                    list.add(mat);
                }
                mat = null;
                String[] tokens = line.split(" ");
                matcell = new MaterialCell(Integer.parseInt(tokens[2]));
                for (int i = 0; i < list.size(); i++) {
                    if (Integer.parseInt(tokens[1]) == list.get(i).getId()) {
                        mat = list.get(i);
                        list.remove(i);
                        break;
                    }
                }
                if (mat == null) {
                    System.out.println("Source material " + tokens[1]
                            + " has not been defined in the materialmanagerdefine.con . This is very likely a mod bug.");
                    mat = new Material(Integer.parseInt(tokens[1]));
                }
            } else if (line.startsWith("materialmanager.damagemod "))
                matcell.setDamagemod(Float.parseFloat(line.split(" ")[1]));
            else if (line.startsWith("materialmanager.setsoundtemplate ")) {
                String[] tokens = line.split(" ");
                if (tokens.length < 3)
                    System.out.println("INFO: Line " + line + " has less than two parameters. This is probably a mod bug.");
                matcell.setSoundtemplate(tokens[tokens.length - 1]);
            } else if (line.startsWith("materialmanager.seteffecttemplate ")) {
                String[] tokens = line.split(" ");
                if (tokens.length < 3)
                    System.out.println("INFO: Line " + line + " has less than two parameters. This is probably a mod bug.");
                matcell.setEffecttemplate(line.split(" ")[tokens.length - 1]);
            }
        }
        s.close();
        if (mat != null)
            list.add(mat);
        return list;
    }

}