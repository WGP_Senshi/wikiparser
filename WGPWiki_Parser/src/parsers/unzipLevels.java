package parsers;

import static java.nio.file.FileVisitResult.*;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.List;

import common.PBar;

public class unzipLevels extends SimpleFileVisitor<Path> implements common.Killable {
	static PBar bar;
	private static volatile boolean isRunning = true;

	public static void unzip(final File dir, List l_log) throws IOException {
		final unzipLevels pW = new unzipLevels();
		Thread thread = new Thread() {

			@Override
			public void run() {
				try {
					Files.walkFileTree(dir.toPath(), pW);
					System.out.println("Hello!");
					gui.Main.display.asyncExec(new Runnable() {
						public void run() {
							if (bar.bar.isDisposed())
								return;
							bar.dispose();
						}
					});
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			};

		};
		bar = new common.PBar(SWT.INDETERMINATE, pW);
		thread.start();

	}

	public void kill() {
		isRunning = false;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		if (!isRunning) {
			System.out.println("Process aborted at " + file + "!");
			return TERMINATE;
		}
		unzipFile(file);
		return CONTINUE;
	}

	private void unzipFile(final Path zipfile) throws IOException {
		byte[] buffer = new byte[8192];
		PathMatcher matchercon = FileSystems.getDefault().getPathMatcher("glob:*.{zip}");
		final Path name = zipfile.getFileName();
		if (matchercon.matches(name)) {
			try {
				gui.Main.display.asyncExec(new Runnable() {
					public void run() {
						if (bar.bar.isDisposed())
							return;
						bar.setText("Extracting " + zipfile.getParent().getFileName() + File.separator + name);
					}
				});
				ZipInputStream zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipfile.toString())));
				// get the zipped file list entry
				ZipEntry ze = zis.getNextEntry();
				while (ze != null && isRunning) {
					gui.Main.display.asyncExec(new Runnable() {
						public void run() {
							if (bar.bar.isDisposed())
								return;
							bar.setSelection(1);
						}
					});

					String fileName = ze.getName();
					File newFile = new File(zipfile.getParent() + File.separator + fileName);
					new File(newFile.getParent()).mkdirs();

					BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(newFile), buffer.length);
					int len;
					while ((len = zis.read(buffer)) > 0) {
						fos.write(buffer, 0, len);
					}

					fos.close();
					ze = zis.getNextEntry();
				}
				zis.closeEntry();
				zis.close();

				zipfile.toFile().delete();
			} catch (IOException e) {

			}
		}

	}

}
