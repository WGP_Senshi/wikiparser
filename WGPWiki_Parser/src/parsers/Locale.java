package parsers;

import static java.nio.file.FileVisitResult.CONTINUE;
import gui.Main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang3.ArrayUtils;

import net.sourceforge.jwbf.core.contentRep.Article;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

public class Locale {

	public static String modus;
	// BidiMap<String, String> map;
	Map<String, String> map;
	private String lang;

	public Locale(String lang) {
		this.lang = lang;
		this.map = new HashMap<String, String>();
		parseLocale(this.lang);
		//System.out.println(ArrayUtils.toString(this.map));
	}

	public void parseLocale(String lang) {
		/**
		 * Parses all locale files (utxt) and stores keysets in hashmap Used to replace codenames with "real" names for easier reading
		 */
		this.map.put("gpm_cnc_inf", "Command and Control Inf");
		this.map.put("gpm_cnc_alt", "Command and Control Alt");
		this.map.put("gpm_cnc_std", "Command and Control Std");
		this.map.put("gpm_aas_inf", "Assault and Secure Inf");
		this.map.put("gpm_aas_alt", "Assault and Secure Alt");
		this.map.put("gpm_aas_128", "Assault and Secure 128");
		this.map.put("gpm_aas_std", "Assault and Secure Std");
		this.map.put("gpm_insurgency_std", "Insurgency Std");
		this.map.put("gpm_insurgency_alt", "Insurgency Alt");
		this.map.put("gpm_insurgency_inf", "Insurgency Inf");
		this.map.put("gpm_skirmish_std", "Skirmish Std");
		this.map.put("gpm_skirmish_alt", "Skirmish Alt");
		this.map.put("gpm_skirmish_inf", "Skirmish Inf");
		this.map.put("gpm_vehicles_std", "Vehicle Warfare Std");
		this.map.put("gpm_vehicles_alt", "Vehicle Warfare Alt");
		this.map.put("gpm_vehicles_inf", "Vehicle Warfare Inf");
		this.map.put("gpm_coop_inf", "Cooperative Inf");
		this.map.put("gpm_coop_std", "Cooperative Std");
		this.map.put("gpm_coop_alt", "Cooperative Alt");
		String dir_chosen = Main.props.getProperty("LocalePath") + "\\" + lang;
		Path check = FileSystems.getDefault().getPath(dir_chosen);
		LocaleWalker pf = new LocaleWalker(this.map, "");

		try {

			parseRemoteDict();
		} catch (java.lang.IllegalStateException e) {
			System.err.println("ERROR: Failed to load wiki localization file!");

		}
		try {
			Files.walkFileTree(check, pf);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void parseRemoteDict() {
		MediaWikiBot wikiBot = new MediaWikiBot(Main.props.getProperty("wiki_url"));
		Article article = wikiBot.getArticle("Template:CodeToName");
		String text = article.getText();
		Scanner s = new Scanner(text);
		s.useDelimiter("\n");
		ArrayList<String> keys = new ArrayList<String>();
		while (s.hasNext()) {
			String line = s.next();
			if (line.startsWith("|")) {
				String[] tokens = line.substring(1).trim().split("=");
				if (tokens.length > 0)
					tokens[0] = tokens[0].trim().toLowerCase();
				if (tokens.length == 1) {
					keys.add(tokens[0]);
				} else if (tokens.length == 2) {
					keys.add(tokens[0]);
					for (String key : keys) {
						if (this.map.get(key) == null) // If enabled, this means that PR localization files have precedence over game files
							this.map.put(key, tokens[1].trim());
					}
					keys = new ArrayList<String>();
				}
				//System.out.println(ArrayUtils.toString(tokens) + " " + tokens.length);
			}
		}
		s.close();

	}

	/**
	 * Walks the locale files
	 * 
	 * @author Senshi
	 * 
	 */
	class LocaleWalker implements FileVisitor<Path> {

		private String separator;
		private Map<String, String> map;

		LocaleWalker(Map<String, String> map2, String separator) {
			this.map = map2;
			this.separator = separator;
		}

		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
			// before visiting entries in a directory we copy the directory
			// (okay if directory already exists).
			return CONTINUE;
		}

		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws FileNotFoundException {
			PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:*.{utxt}");
			Path name = file.getFileName();
			// First of all, verify that the file is a correct file format
			if (name != null && matcher.matches(name)) {
				try {
					this.processLocale(file);
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			return CONTINUE;
		}

		public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
			return CONTINUE;
		}

		public FileVisitResult visitFileFailed(Path file, IOException exc) {
			System.err.println(exc);
			return CONTINUE;
		}

		public void processLocale(Path file) throws FileNotFoundException, UnsupportedEncodingException {
			// Manual addition of various non-localized labels

			Scanner scanner = new Scanner(new InputStreamReader(new FileInputStream(file.toString()), "UTF-16"));
			scanner.useDelimiter(this.separator);
			try {
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					processLine(line);

				}
			} finally {
				// ensure the underlying stream is always closed
				// this only has any effect if the item passed to the Scanner
				// constructor implements Closeable (which it does in this case).
				scanner.close();
			}

		}

		private void processLine(String aLine) {
			Scanner scanner = new Scanner(aLine);
			scanner.useDelimiter(this.separator);

			if (scanner.hasNext()) {
				String code = scanner.next();
				code = code.replaceAll("\\s", "").trim(); // Codes are defined as "no-whitespace", so let's do it.
				if (code.equalsIgnoreCase("rem") || code.startsWith("//")) {
					scanner.close();
					return;
				}
				if (scanner.hasNext()) {
					String name = scanner.next();
					name.replaceAll("", "");
					// if (code.contains("_LEVELNAME_"))
					//System.out.println(code.toLowerCase() + "----------" + name);
					//if (this.map.get(code.toLowerCase()) == null) 
					this.map.put(code.toLowerCase(), name);
				}
			}
			scanner.close();
		}

	}

	/**
	 * Returns String value if found. If not, returns null.
	 */
	public String getValue(String key) {
		String retval = this.map.get(key.toLowerCase());
		if (retval != null)
			retval = replacenbsp(retval);
		//System.out.println(ArrayUtils.toString(this.map.keySet()));
		//System.out.println(key.toLowerCase() + "------" + retval + "------" + this.map.);
		return retval;

	}

	/**
	 * Returns String value if found. If not, returns default value.
	 */
	public String getValue(String key, String defaultvalue) {
		String retval = getValue(key.toLowerCase());
		if (retval == null)
			retval = defaultvalue;
		return retval;

	}

	private String replacenbsp(String val) {
		return val.replace("&nbsp;", " ");
	}
}
