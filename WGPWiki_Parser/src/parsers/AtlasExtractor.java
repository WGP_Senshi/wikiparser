package parsers;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageInputStream;

public class AtlasExtractor {

	public static void processCon(String input, String outdir, org.eclipse.swt.widgets.List l_log) throws FileNotFoundException {
		/**
		 * Reads a given atlaslist.con file and extracts all atlas.dds files specified there.
		 * Every atlas image is extracted and created in PNG format with its given name.
		 * 
		 */
		if (new File(outdir).exists())
			new File(outdir).delete();
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		File file_in = new File(input);
		List<String> atlasfiles = new ArrayList<String>();
		try { // Scans atlascon for list of atlas files
			Scanner s = new Scanner(file_in);
			while (s.hasNextLine()) {
				String line = s.nextLine();
				String[] tokens = line.split(" ");
				String filename = tokens[1].replace("\"", "");
				atlasfiles.add(file_in.getParent() + "\\" + filename);
			}
			s.close();
		} catch (FileNotFoundException e) {
			System.out.println("File " + file_in + " not found!");
			return;
		}

		List<String[]> tex_list = new ArrayList<String[]>();
		int prevatlas = -1;
		BufferedImage ddsatlas = null;
		for (String afile : atlasfiles) {
			File f_afile = new File(afile);
			Scanner s = new Scanner(f_afile);
			while (s.hasNextLine()) {
				String line = s.nextLine().trim().replace(",", "");
				if (line.equals(""))
					continue;
				if (line.startsWith("#"))
					continue;
				String[] tokens = line.split("\\s+");
				for (int i = 0; i < tokens.length; i++)
					tokens[i] = tokens[i].trim();

				tex_list.add(tokens);
				// Tokens should be: filename, atlas filename, atlas index, woffset, hoffset, width, height
				// Coordinates start topleft

				tokens[0] = tokens[0].substring(0, tokens[0].lastIndexOf("."));
				tokens[0] = tokens[0].concat(".png");
				File outfile = new File((outdir + "\\" + tokens[0]).toLowerCase());
				outfile.getParentFile().mkdirs();
				if (prevatlas != Integer.parseInt(tokens[2])) { // Only load new dds file if necessary.
					l_log.add(dateFormat.format(new Date()) + " : Loading " + tokens[1].replace("Menu\\Atlas\\", ""));
					l_log.getParent().update();
					ddsatlas = loadDDS(file_in.getParent() + "\\" + tokens[1].replace("Menu\\Atlas\\", ""));
				}
				int totalwidth = ddsatlas.getWidth();
				int totalheight = ddsatlas.getHeight();
				int x = Math.round(Float.parseFloat(tokens[3]) * totalwidth);
				int y = Math.round(Float.parseFloat(tokens[4]) * totalheight);
				int width = Math.round(Float.parseFloat(tokens[5]) * totalwidth);
				int height = Math.round(Float.parseFloat(tokens[6]) * totalheight);

				try {
					ImageIO.write(ddsatlas.getSubimage(x, y, width, height), "png", outfile);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				prevatlas = Integer.parseInt(tokens[2]);

			}
			s.close();
		}
		l_log.add(dateFormat.format(new Date()) + " : Done!");
	}

	private static BufferedImage loadDDS(String ddsname) {
		File file = new File(ddsname);
		if (!file.exists()) {
			System.out.println("Failed to load: " + file.getName());
			return null;
		}

		BufferedImage image_dds = null;
		ddsreader.DDSImageReaderSpi xspi = new ddsreader.DDSImageReaderSpi();
		ddsreader.DDSImageReader imageReader = new ddsreader.DDSImageReader(xspi);
		try {
			imageReader.setInput(new FileImageInputStream(file));
			image_dds = imageReader.read(0);
		} catch (IOException ex) {
			System.out.println("Failed to load: " + file.getName());
		}

		return image_dds;
		// return new Image(chooser.display, SWTUtils.convertToSWT(image_dds));
	}
}
