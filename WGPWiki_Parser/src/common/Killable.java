package common;

/**
 * Killable processes have some kind of kill method. This method can be used to abort the process before it has finished. Abortable, long worker threads come to
 * mind :) .
 * 
 * @author Senshi
 *
 */
public interface Killable {
	public void kill();
}
