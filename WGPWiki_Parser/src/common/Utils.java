package common;

import gui.Main;

import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;

public class Utils {

	/**
	 * Count files in a directory (including files in all subdirectories)
	 * 
	 * @param directory
	 *            the directory to start in
	 * @return the total number of files
	 */
	public static int countFilesInDirectory(File directory) {
		int count = 0;
		for (File file : directory.listFiles()) {
			if (file.isFile()) {
				count++;
			}
			if (file.isDirectory()) {
				count += countFilesInDirectory(file);
			}
		}
		return count;
	}

	/**
	 * Returns number of immediate sub directoriers. NOT recursively (no subsubfolders).
	 * 
	 * @param directory
	 * @return
	 */
	public static int countDirsinDir(File directory) {
		File[] files = directory.listFiles(new FileFilter() {
			@Override
			public boolean accept(File f) {
				return f.isDirectory();
			}
		});
		return files.length;
	}

	public static String openDirectorydialog(String filterpath, String title, String message) throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException {
		final DirectoryDialog dialog_dir = new DirectoryDialog(Main.mainshell);
		dialog_dir.setText(title);
		dialog_dir.setMessage(message);
		// read Battlefield 2\mods\pr\objects\weapons\Handheld directory
		// Two checks to cover 64bit/32bit registries
		String bf2path = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\Wow6432Node\\Project Reality\\Project Reality: BF2", "InstallDir");
		if (bf2path == null)
			bf2path = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\Project Reality\\Project Reality: BF2", "InstallDir");
		if (bf2path == null)
			bf2path = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\Wow6432Node\\Electronic Arts\\EA GAMES\\Battlefield 2", "InstallDir");
		if (bf2path == null)
			bf2path = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\Electronic Arts\\EA GAMES\\Battlefield 2", "InstallDir");
		// Checks if pr_wgp_edit or pr_edit folder exist, sets them to default
		// if available
		String prwgpeditpath = bf2path + "\\mods\\pr_wgp_edit\\" + filterpath;
		File prwgpeditdirectory = new File(prwgpeditpath);
		String preditpath = bf2path + "\\mods\\pr_edit\\" + filterpath;
		File preditdirectory = new File(preditpath);

		if (prwgpeditdirectory.exists()) {
			dialog_dir.setFilterPath(prwgpeditpath);
		} else if (preditdirectory.exists()) {
			dialog_dir.setFilterPath(preditpath);
		} else if (new File(filterpath).exists()) {
			dialog_dir.setFilterPath(filterpath);
		} else if (bf2path != null) {
			dialog_dir.setFilterPath(bf2path);
		}

		return dialog_dir.open();
	}

	public static String openFiledialog(String filterpath, String fname, String title, String[] extensions) throws IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		final FileDialog dialog_file = new FileDialog(Main.mainshell);
		dialog_file.setText(title);
		dialog_file.setFilterExtensions(extensions);
		dialog_file.setFileName(fname);
		// read Battlefield 2\mods\pr\objects\weapons\Handheld directory
		// Two checks to cover 64bit/32bit registries
		String bf2path = WinRegistry
				.readString(WinRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\Wow6432Node\\Electronic Arts\\EA GAMES\\Battlefield 2", "InstallDir");
		if (bf2path == null)
			bf2path = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\Electronic Arts\\EA GAMES\\Battlefield 2", "InstallDir");
		// Checks if pr_wgp_edit or pr_edit folder exist, sets them to default
		// if available
		String prwgpeditpath = bf2path + "\\mods\\pr_wgp_edit\\" + filterpath;
		File prwgpeditdirectory = new File(prwgpeditpath);
		String preditpath = bf2path + "\\mods\\pr_edit\\" + filterpath;
		File preditdirectory = new File(preditpath);

		if (prwgpeditdirectory.exists()) {
			dialog_file.setFilterPath(prwgpeditpath);
		} else if (preditdirectory.exists()) {
			dialog_file.setFilterPath(preditpath);
		} else if (bf2path != null) {
			dialog_file.setFilterPath(bf2path);
		}

		return dialog_file.open();
	}

	public static String getName_nosuffix(String filename) {
		return filename.substring(0, filename.lastIndexOf("."));
	}
}