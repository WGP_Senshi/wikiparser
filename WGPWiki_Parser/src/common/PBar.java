package common;

import gui.Main;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

public class PBar {

	Shell shell;
	public ProgressBar bar;
	public Label text;

	public PBar(int barstyle) {
		// this.shell = new Shell(Main.display, SWT.APPLICATION_MODAL);
		this.shell = new Shell(Main.display, SWT.NO_TRIM | SWT.ON_TOP);
		this.bar = new ProgressBar(shell, barstyle);
		this.text = new Label(shell, SWT.NONE);
		shell.setLayout(new FormLayout());
		text.setBounds(5, 5, 200, 20);
		bar.setBounds(5, 25, 200, 20);

		FormData text_ld = new FormData();
		text_ld.left = new FormAttachment(0, 5);
		text_ld.top = new FormAttachment(0, 5);
		text_ld.right = new FormAttachment(100, -5);
		text.setLayoutData(text_ld);

		FormData bar_ld = new FormData();
		bar_ld.left = new FormAttachment(0, 5);
		bar_ld.top = new FormAttachment(text, 5);
		bar_ld.right = new FormAttachment(100, -5);
		bar.setLayoutData(bar_ld);

		shell.layout();
		shell.setSize(220, 55);
		shell.open();
	}

	public PBar(int barstyle, final Killable process) {
		// this.shell = new Shell(Main.display, SWT.APPLICATION_MODAL);
		this.shell = new Shell(Main.display, SWT.NO_TRIM | SWT.ON_TOP);
		final Button b_cancel = new Button(shell, SWT.PUSH);
		this.bar = new ProgressBar(shell, barstyle);
		this.text = new Label(shell, SWT.NONE);
		shell.setLayout(new FormLayout());
		text.setBounds(5, 5, 200, 20);
		bar.setBounds(5, 25, 200, 20);
		b_cancel.setText("Abort");

		FormData text_ld = new FormData();
		text_ld.left = new FormAttachment(0, 5);
		text_ld.top = new FormAttachment(0, 5);
		text_ld.right = new FormAttachment(100, -5);
		text.setLayoutData(text_ld);

		FormData b_cancel_fd = new FormData();
		b_cancel_fd.top = new FormAttachment(text, 5);
		b_cancel_fd.right = new FormAttachment(100, -5);
		b_cancel.setLayoutData(b_cancel_fd);

		FormData bar_ld = new FormData();
		bar_ld.left = new FormAttachment(0, 5);
		bar_ld.top = new FormAttachment(text, 5);
		bar_ld.right = new FormAttachment(b_cancel, -5);
		bar.setLayoutData(bar_ld);

		b_cancel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				b_cancel.setEnabled(false);
				process.kill();
			}
		});

		shell.layout();
		shell.setSize(220, 55);
		shell.open();
	}

	public void setText(String str) {
		this.text.setText(str);
	}

	public void setSelection(int i) {
		this.bar.setSelection(i);
	}

	public void setMaximum(int i) {
		this.bar.setMaximum(i);
	}

	public void setMinimum(int i) {
		this.bar.setMinimum(i);
	}

	public void dispose() {
		this.shell.dispose();
		this.bar.dispose();
	}
}
