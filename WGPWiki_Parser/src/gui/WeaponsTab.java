package gui;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sourceforge.jwbf.core.contentRep.Article;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

import db.BF2Object;
import db.GenericFireArm;
import db.Projectile;

public class WeaponsTab {

	public static void initUI(final Composite comp) {
		comp.setLayout(new FormLayout());

		final Tree tree = new Tree(comp, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION | SWT.H_SCROLL);
		tree.setHeaderVisible(true);
		tree.setLinesVisible(true);
		TreeColumn column1 = new TreeColumn(tree, SWT.LEFT);
		column1.setText("Name");
		column1.setWidth(200);
		TreeColumn column2 = new TreeColumn(tree, SWT.LEFT);
		column2.setText("Index");
		column2.setWidth(50);
		TreeColumn column3 = new TreeColumn(tree, SWT.LEFT);
		column3.setText("MagSize");
		column3.setWidth(60);
		TreeColumn column4 = new TreeColumn(tree, SWT.LEFT);
		column4.setText("MagNo");
		column4.setWidth(60);
		TreeColumn column5 = new TreeColumn(tree, SWT.LEFT);
		column5.setText("Reload");
		column5.setWidth(50);
		TreeColumn column6 = new TreeColumn(tree, SWT.LEFT);
		column6.setText("Velocity");
		column6.setWidth(60);
		TreeColumn column7 = new TreeColumn(tree, SWT.LEFT);
		column7.setText("Projectile");
		column7.setWidth(200);
		final Text t_filter1 = new Text(comp, SWT.BORDER);
		final Text t_filter2 = new Text(comp, SWT.BORDER);
		final Text t_filter3 = new Text(comp, SWT.BORDER);
		final Text t_filter4 = new Text(comp, SWT.BORDER);
		final Text t_filter5 = new Text(comp, SWT.BORDER);
		final Text t_filter6 = new Text(comp, SWT.BORDER);
		final Text t_filter7 = new Text(comp, SWT.BORDER);
		Label log_label = new Label(comp, 0);
		Label list_label = new Label(comp, 0);
		Text log = new Text(comp, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		final Canvas icon = new Canvas(comp, 0);
		Button b_refresh = new Button(comp, 0);
		final Combo c_copymode = new Combo(comp, 0);
		Button b_copy = new Button(comp, 0);
		final Label l_filterno = new Label(comp, 0);
		final Sash separator_h = new Sash(comp, SWT.BORDER | SWT.HORIZONTAL);

		comp.setLayout(new FormLayout());
		log_label.setText("Log:");
		list_label.setText("Weapons to parse (use Ctrl/Shift to select multiple files):");
		b_refresh.setText("Refresh List");
		b_refresh.setToolTipText("Begin processing!");
		b_copy.setText("Wiki it!");
		fillCombo(c_copymode);

		/*
		 * Format Layout dir_label - dir savetemplate - opentemplate list
		 * log_label log
		 */

		FormData list_labeldata = new FormData(300, 25);
		list_labeldata.top = new FormAttachment(0, 5);
		list_labeldata.left = new FormAttachment(0, 5);
		list_label.setLayoutData(list_labeldata);

		FormData b_copy_layoutdata = new FormData();
		b_copy_layoutdata.top = new FormAttachment(0, 5);
		b_copy_layoutdata.right = new FormAttachment(100, -5);
		b_copy.setLayoutData(b_copy_layoutdata);

		FormData c_copymode_layoutdata = new FormData(120, 25);
		c_copymode_layoutdata.top = new FormAttachment(b_copy, 0, SWT.CENTER);
		c_copymode_layoutdata.right = new FormAttachment(b_copy, -5, SWT.LEFT);
		c_copymode.setLayoutData(c_copymode_layoutdata);

		FormData dir_layoutdata = new FormData(192, 20);
		dir_layoutdata.left = new FormAttachment(0, 5);
		dir_layoutdata.top = new FormAttachment(list_label, 5);
		t_filter1.setLayoutData(dir_layoutdata);
		t_filter1.setSize(200, t_filter1.getSize().y);

		FormData t_filter2_fd = new FormData(column2.getWidth() - 16, 20);
		t_filter2_fd.left = new FormAttachment(t_filter1, 0, SWT.RIGHT);
		t_filter2_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter2.setLayoutData(t_filter2_fd);

		FormData t_filter3_fd = new FormData(column3.getWidth() - 16, 20);
		t_filter3_fd.left = new FormAttachment(t_filter2, 0, SWT.RIGHT);
		t_filter3_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter3.setLayoutData(t_filter3_fd);

		FormData t_filter4_fd = new FormData(column4.getWidth() - 16, 20);
		t_filter4_fd.left = new FormAttachment(t_filter3, 0, SWT.RIGHT);
		t_filter4_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter4.setLayoutData(t_filter4_fd);

		FormData t_filter5_fd = new FormData(column5.getWidth() - 16, 20);
		t_filter5_fd.left = new FormAttachment(t_filter4, 0, SWT.RIGHT);
		t_filter5_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter5.setLayoutData(t_filter5_fd);

		FormData t_filter6_fd = new FormData(column6.getWidth() - 16, 20);
		t_filter6_fd.left = new FormAttachment(t_filter5, 0, SWT.RIGHT);
		t_filter6_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter6.setLayoutData(t_filter6_fd);

		FormData t_filter7_fd = new FormData(column7.getWidth() - 8, 20);
		t_filter7_fd.left = new FormAttachment(t_filter6, 0, SWT.RIGHT);
		t_filter7_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter7.setLayoutData(t_filter7_fd);

		FormData btn_go_layoutdata = new FormData(70, 25);
		btn_go_layoutdata.top = new FormAttachment(t_filter1, 0, SWT.CENTER);
		btn_go_layoutdata.right = new FormAttachment(100, -5);
		b_refresh.setLayoutData(btn_go_layoutdata);

		FormData btn_opendir_layoutdata = new FormData();
		btn_opendir_layoutdata.top = new FormAttachment(t_filter1, 0, SWT.CENTER);
		btn_opendir_layoutdata.right = new FormAttachment(b_refresh, -5, SWT.LEFT);
		btn_opendir_layoutdata.left = new FormAttachment(t_filter7, 5, SWT.RIGHT);
		l_filterno.setLayoutData(btn_opendir_layoutdata);

		FormData listlayoutdata = new FormData(600, -1);
		listlayoutdata.bottom = new FormAttachment(separator_h, -5, SWT.TOP);
		listlayoutdata.top = new FormAttachment(t_filter1, 5, SWT.BOTTOM);
		listlayoutdata.left = new FormAttachment(comp, 5, SWT.LEFT);
		listlayoutdata.right = new FormAttachment(100, -5);
		tree.setLayoutData(listlayoutdata);

		FormData log_labeldata = new FormData(300, 20);
		log_labeldata.top = new FormAttachment(icon, 0, SWT.BOTTOM);
		log_labeldata.left = new FormAttachment(0, 5);
		log_label.setLayoutData(log_labeldata);

		FormData icon_fd = new FormData(150, 45);
		icon_fd.left = new FormAttachment(0, 5);
		icon_fd.top = new FormAttachment(separator_h, 5, SWT.BOTTOM);
		icon.setLayoutData(icon_fd);

		FormData loglayoutdata = new FormData(SWT.DEFAULT, 150);
		loglayoutdata.left = new FormAttachment(0, 5);
		loglayoutdata.right = new FormAttachment(100, -5);
		loglayoutdata.bottom = new FormAttachment(100, -5);
		loglayoutdata.top = new FormAttachment(log_label, 5, SWT.BOTTOM);
		log.setLayoutData(loglayoutdata);

		final FormData separatorlayoutdata = new FormData(-1, -1);
		separatorlayoutdata.bottom = new FormAttachment(50, 5);
		separatorlayoutdata.left = new FormAttachment(0, 0);
		separatorlayoutdata.right = new FormAttachment(100, 0);
		separatorlayoutdata.height = 3;
		separator_h.setLayoutData(separatorlayoutdata);
		separator_h.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				Rectangle sashRect = separator_h.getBounds();
				Rectangle shellRect = comp.getClientArea();
				int top = shellRect.height - sashRect.height - 100;
				e.y = Math.max(Math.min(e.y, top), 200);
				if (e.y != sashRect.y) {
					separatorlayoutdata.bottom = new FormAttachment(0, e.y);
				}
				comp.layout(true);
			}
		});

		b_refresh.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				t_filter1.setText("");
				refreshTree(tree, t_filter1.getText(), t_filter2.getText(), t_filter3.getText(), t_filter4.getText(), t_filter5.getText(), t_filter6.getText(),
						t_filter7.getText());
				l_filterno.setText(String.valueOf(tree.getItemCount()));
			}
		});

		t_filter1.addListener(SWT.Modify, new Listener() {
			@Override
			public void handleEvent(Event e) {
				refreshTree(tree, t_filter1.getText(), t_filter2.getText(), t_filter3.getText(), t_filter4.getText(), t_filter5.getText(), t_filter6.getText(),
						t_filter7.getText());
				l_filterno.setText(String.valueOf(tree.getItemCount()));
			}
		});
		t_filter2.addListener(SWT.Modify, new Listener() {
			@Override
			public void handleEvent(Event e) {
				refreshTree(tree, t_filter1.getText(), t_filter2.getText(), t_filter3.getText(), t_filter4.getText(), t_filter5.getText(), t_filter6.getText(),
						t_filter7.getText());
				l_filterno.setText(String.valueOf(tree.getItemCount()));
			}
		});
		t_filter3.addListener(SWT.Modify, new Listener() {
			@Override
			public void handleEvent(Event e) {
				refreshTree(tree, t_filter1.getText(), t_filter2.getText(), t_filter3.getText(), t_filter4.getText(), t_filter5.getText(), t_filter6.getText(),
						t_filter7.getText());
				l_filterno.setText(String.valueOf(tree.getItemCount()));
			}
		});
		t_filter4.addListener(SWT.Modify, new Listener() {
			@Override
			public void handleEvent(Event e) {
				refreshTree(tree, t_filter1.getText(), t_filter2.getText(), t_filter3.getText(), t_filter4.getText(), t_filter5.getText(), t_filter6.getText(),
						t_filter7.getText());
				l_filterno.setText(String.valueOf(tree.getItemCount()));
			}
		});
		t_filter5.addListener(SWT.Modify, new Listener() {
			@Override
			public void handleEvent(Event e) {
				refreshTree(tree, t_filter1.getText(), t_filter2.getText(), t_filter3.getText(), t_filter4.getText(), t_filter5.getText(), t_filter6.getText(),
						t_filter7.getText());
				l_filterno.setText(String.valueOf(tree.getItemCount()));
			}
		});
		t_filter6.addListener(SWT.Modify, new Listener() {
			@Override
			public void handleEvent(Event e) {
				refreshTree(tree, t_filter1.getText(), t_filter2.getText(), t_filter3.getText(), t_filter4.getText(), t_filter5.getText(), t_filter6.getText(),
						t_filter7.getText());
				l_filterno.setText(String.valueOf(tree.getItemCount()));
			}
		});
		t_filter7.addListener(SWT.Modify, new Listener() {
			@Override
			public void handleEvent(Event e) {
				refreshTree(tree, t_filter1.getText(), t_filter2.getText(), t_filter3.getText(), t_filter4.getText(), t_filter5.getText(), t_filter6.getText(),
						t_filter7.getText());
				l_filterno.setText(String.valueOf(tree.getItemCount()));
			}
		});
		tree.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				TreeItem[] selection = tree.getSelection();
				String name = selection[selection.length - 1].getText();
				// System.out.println(name);
				try {
					String filename = "db/atlas/Menu/HUD/Texture/"
							+ ((GenericFireArm) Main.bf2objectDB.getByCode(name)).getIcon().substring(0,
									((GenericFireArm) Main.bf2objectDB.getByCode(name)).getIcon().lastIndexOf(".")) + ".png";
					Image img = new Image(Main.display, filename);
					// Image img = new Image(chooser.display,
					// SWTUtils.convertToSWT(image_dds));
					GC gc = new GC(icon, 0);
					gc.setBackground(icon.getBackground());
					gc.fillRectangle(0, 0, icon.getClientArea().width, icon.getClientArea().height);
					gc.drawImage(img, 0, 0);
					gc.dispose();
				} catch (SWTException | NullPointerException ex) {
					GC gc = new GC(icon, 0);
					gc.setBackground(icon.getBackground());
					gc.fillRectangle(0, 0, icon.getClientArea().width, icon.getClientArea().height);
					gc.setForeground(Main.display.getSystemColor(SWT.COLOR_RED));
					gc.setAntialias(SWT.ON);
					gc.drawLine(0, 0, icon.getClientArea().width, icon.getClientArea().height);
					gc.drawLine(icon.getClientArea().width, 0, 0, icon.getClientArea().height);
					gc.setForeground(Main.display.getSystemColor(SWT.COLOR_BLACK));
					gc.drawText("No image found", 0, 0, true);
					gc.dispose();
				}
			}
		});

		b_copy.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				if (c_copymode.getText().equals("Table")) {
					TreeItem[] selection = tree.getSelection();
					String namelist = "";
					String copy = "{{Version|" + Main.PR_version + "}}\n";
					copy += "{| class='wikitable sortable' style='float:left; width:100px'\n";
					copy += "! Name" + "!! class='unsortable' | Icon " + "!! class='unsortable' | Setup time " + "!! class='unsortable' | Muzzle velocity "
							+ "!! class='unsortable' | Fire Modes* " + "!! class='unsortable' | Mag size " + "!! class='unsortable' | No of mags "
							+ "!! class='unsortable' | Reload time" + "!! class='unsortable' | Raw Damage" + "!! class='unsortable' | Range"
							+ "!! class='unsortable' | Zoom levels";
					for (int i = 0; i < selection.length; i++) {
						GenericFireArm genericFireArm = (GenericFireArm) Main.bf2objectDB.getByCode(selection[i].getText());
						if (genericFireArm.getIcon() != null) {
							namelist += "| " + genericFireArm.getCode() + " = " + Main.lang.getValue(genericFireArm.getCode(), genericFireArm.getCode()) + "\n";
							copy += "\n|-";
							copy += "\n|[[" + Main.lang.getValue(genericFireArm.getCode()) + "]]";
							copy += "\n|[[File:"
									+ genericFireArm.getIcon().substring(genericFireArm.getIcon().lastIndexOf("/") + 1, genericFireArm.getIcon().length() - 4)
									+ "_icon.png]]";
							copy += "\n|" + genericFireArm.getSetup();
							copy += "\n|" + genericFireArm.getMuzzle();
							copy += "\n|" + genericFireArm.getFirerates();
							copy += "\n|" + genericFireArm.getMagsize();
							copy += "\n|" + genericFireArm.getMagno();
							copy += "\n|" + genericFireArm.getReload();
							if (Main.bf2objectDB.getByCode(genericFireArm.getProjectilename()) != null) {
								copy += "\n|" + ((Projectile) Main.bf2objectDB.getByCode(genericFireArm.getProjectilename())).getDmg();
								copy += "\n|" + ((Projectile) Main.bf2objectDB.getByCode(genericFireArm.getProjectilename())).getTTL()
										* genericFireArm.getMuzzle();
							}
							copy += "\n|" + genericFireArm.getZooms();
							copy += "\n"; // nicer output
						}
					}
					copy += "\n|-\n|\n| colspan='12' class='unsortable' |* 0: Semi-Auto; 1 = Burst Fire; 2 = Full Auto|}}";
					copy += "\n|}";
					copy += "\n{{Navbox}}";
					System.out.println(namelist);
					Clipboard cb = new Clipboard(Display.getDefault());
					TextTransfer tt = TextTransfer.getInstance();
					cb.setContents(new Object[] { copy }, new TextTransfer[] { tt });
					cb.dispose();
				} else if (c_copymode.getText().equals("Infobox")) {
					System.out.println("Infoboxes, yay");
					// 1. Find all weapons with the same name (will be placed on the same page)

					String copy = "{{Version|" + Main.PR_version + "}}\n";
					copy += "{| class='wikitable sortable' style='float:left; width:100px'\n";
					copy += "! Name" + "!! class='unsortable' | Icon " + "!! class='unsortable' | Setup time " + "!! class='unsortable' | Muzzle velocity "
							+ "!! class='unsortable' | Fire Modes* " + "!! class='unsortable' | Mag size " + "!! class='unsortable' | No of mags "
							+ "!! class='unsortable' | Reload time" + "!! class='unsortable' | Raw Damage" + "!! class='unsortable' | Range"
							+ "!! class='unsortable' | Zoom levels";
					List<ArrayList<String>> names = new ArrayList<ArrayList<String>>();

					for (TreeItem item : tree.getItems()) {
						GenericFireArm genericFireArm = (GenericFireArm) Main.bf2objectDB.getByCode(item.getText());
						String name = genericFireArm.getName();
						if (name == null)
							name = Main.lang.getValue(genericFireArm.getCode(), null);
						if (name == null)
							continue;

						boolean exist = false;
						for (ArrayList<String> listname : names) {
							if (listname.get(0).equalsIgnoreCase(Main.lang.getValue(genericFireArm.getCode(), ""))) {
								listname.add(genericFireArm.getCode());
								exist = true;
								break;
							}
						}
						if (!exist) {
							names.add(new ArrayList<String>());
							names.get(names.size() - 1).add(Main.lang.getValue(genericFireArm.getCode(), null));
						}

					}

					// 2. Iterate over all unique weaponnames
					// 2.1 Iterate over all weapons per unique weaponname
					// 2.2 Create/Edit wiki pages (getArticle, edit, setArticle)
					// 2.3 Submit article

					TreeItem[] selection = tree.getSelection();
					List<GenericFireArm> sameweapons = new ArrayList<GenericFireArm>();
					for (TreeItem selitem : selection) {
						GenericFireArm selectFA = (GenericFireArm) Main.bf2objectDB.getByCode(selitem.getText());
						sameweapons = new ArrayList<GenericFireArm>();

						//Article article = wikibot.getArticle(Main.lang.getValue(selectFA.getCode(), null));

						for (TreeItem item : tree.getItems()) {
							GenericFireArm iterFA = (GenericFireArm) Main.bf2objectDB.getByCode(item.getText());
							if (iterFA != null) {
								if (Main.lang.getValue(selectFA.getCode(), null).equalsIgnoreCase(Main.lang.getValue(iterFA.getCode(), ""))) {
									sameweapons.add(iterFA);
								}

							}
						}
						String wcopy = "";
						for (GenericFireArm curweapon : sameweapons) {
							String loc = Main.lang.getValue(curweapon.getCode(), "");
							wcopy += "\n{{Infobox Weapon";
							wcopy += "\n|name = " + curweapon.getCode();
							if (curweapon.getIcon() != null) {
								wcopy += "\n|image = "
										+ curweapon.getIcon().substring(curweapon.getIcon().lastIndexOf("/") + 1, curweapon.getIcon().length() - 4)
												.toLowerCase() + "_icon.png";
								wcopy += "\n|icon = "
										+ curweapon.getIcon().substring(curweapon.getIcon().lastIndexOf("/") + 1, curweapon.getIcon().length() - 4)
										+ "_icon.png";
							}
							wcopy += "\n|magno = " + curweapon.getMagno();
							wcopy += "\n|magsize = " + curweapon.getMagsize();
							wcopy += "\n|reload = " + curweapon.getReload();
							wcopy += "\n|firerates = ";
							if (curweapon.getFirerates().contains(0))
								wcopy += " SA "; // semi automatic
							if (curweapon.getFirerates().contains(1))
								wcopy += " BF "; // Burst fire
							if (curweapon.getFirerates().contains(2))
								wcopy += " FA "; // fully automatic
							wcopy += "\n|setup = " + curweapon.getSetup();
							wcopy += "\n|rpm = " + curweapon.getRPM();
							wcopy += "\n|muzzle = " + curweapon.getMuzzle();
							wcopy += "\n|mindev = " + curweapon.getMindev();
							wcopy += "\n|turndev = " + Arrays.toString(curweapon.getTurndev());
							wcopy += "\n|speeddev = " + Arrays.toString(curweapon.getSpeeddev());
							wcopy += "\n|miscdev = " + Arrays.toString(curweapon.getMiscdev());
							wcopy += "\n|zooms = ";
							for (Float x : curweapon.getZooms()) {
								if (x != 0 && x != 1.099)
									wcopy += x;
							}
							wcopy += "\n|devmodstand = " + curweapon.getDevmodstand();
							wcopy += "\n|devmodcrouch = " + curweapon.getDevmodcrouch();
							wcopy += "\n|devmodlie = " + curweapon.getDevmodlie();
							wcopy += "\n|devmodzoom = " + curweapon.getDevmodzoom();
							wcopy += "\n|recoilforceup = " + Arrays.toString(curweapon.getRecoilForceUp());
							wcopy += "\n|recoilforceleftright = " + Arrays.toString(curweapon.getRecoilForceLeftRight());
							wcopy += "\n|recoilzoommodifier = " + curweapon.getRecoilZoommodifier();
							Projectile proj = (Projectile) Main.bf2objectDB.getByCode(curweapon.getProjectilename());
							wcopy += "\n|dmg = " + proj.getDmg();
							wcopy += "\n|range = " + String.valueOf(proj.getTTL() * curweapon.getMuzzle());
							wcopy += "\n|grav_mod = " + String.format("%1d", (int) (proj.getGrav_mod() * 100));
							;
							wcopy += "\n|dmg_min = " + String.format("%1d", (int) (proj.getDmg_min() * 100));
							wcopy += "\n|disttomindmg = " + proj.getDisttomindmg();
							wcopy += "\n|disttostartlosedmg = " + proj.getDisttostartlosedmg();
							if (proj.getTracer_interval() == 0)
								wcopy += "\n|tracer = None";
							else
								wcopy += "\n|tracer = Every " + proj.getTracer_interval() + " rounds";
							wcopy += "\n|expl_dmg = " + proj.getExpl_dmg();
							wcopy += "\n|expl_radius = " + proj.getExpl_radius();
							wcopy += "\n|expl_force = " + proj.getExpl_force();
							wcopy += "\n|expl_maxdepth = " + proj.getExpl_maxdepth();
							wcopy += "\n}}";

							/*
							 * {{Infobox Weapon
									|name = C9A2
									|image = cflmg_c9_icon.png
									|icon = cflmg_c9_icon.png
									|magno = 3
									|magsize = 200
									|reload = 9.8
									|firerates = <s>Single</s> {{!}} <s>Burst</s> {{!}} Automatic
									|setup = 3.4
									|rpm = 900
									|muzzle = 866
									|mindev = 0.56
									|turndev = 0.3 0.4 0.1819
									|speeddev = 0.01705 0.014666 0.02095
									|miscdev = 6.0 0.03333
									|zooms = None
									|devmodstand = 1.15
									|devmodcrouch = 0.9
									|devmodlie = 0.7
									|devmodzoom = 0.4
									|recoilforceup = CRD_UNIFORM/2.5/2.5/0.2
									|recoilforceleftright= CRD_UNIFORM/-1.7/1.7/0.85
									|recoilzoommodifier = 0.4
									
									|dmg = 39
									|range = 1732
									|grav_mod = 30%
									|dmg_min = 40%
									|disttostartlosedmg = 400
									|disttomindmg = 1000
									|tracer = Red, every third round
									|expl_dmg = 0
									|expl_radius = 3
									|expl_force = 0
									|expl_dmg = 0
									|expl_maxdepth = 2
									}}
							 */
						}
						try {
							MediaWikiBot wikibot = new MediaWikiBot(Main.props.getProperty("wiki_url"));
							wikibot.login(Main.props.getProperty("wiki_user"), Main.props.getProperty("wiki_pass"));
							//Article article = wikibot.getArticle("Sandbox");
							Article article = wikibot.getArticle(Main.lang.getValue(sameweapons.get(0).getCode(), sameweapons.get(0).getCode()).replace("[", "").replace("]", ""));
							article.setEditSummary("BOT: Test Update " + selectFA.getCode());
							article.setText(wcopy);
							article.save();
						} catch (Exception ex) {
							ex.printStackTrace();
						}

					}
					String namelist = "";
					for (int i = 0; i < selection.length; i++) {
						GenericFireArm genericFireArm = (GenericFireArm) Main.bf2objectDB.getByCode(selection[i].getText());
						if (genericFireArm.getIcon() != null) {
							namelist += "| " + genericFireArm.getCode() + " = " + Main.lang.getValue(genericFireArm.getCode(), genericFireArm.getCode()) + "\n";
							copy += "\n|-";
							copy += "\n|[[" + Main.lang.getValue(genericFireArm.getCode()) + "]]";
							copy += "\n|[[File:"
									+ genericFireArm.getIcon().substring(genericFireArm.getIcon().lastIndexOf("/") + 1, genericFireArm.getIcon().length() - 4)
									+ "_icon.png]]";
							copy += "\n|" + genericFireArm.getSetup();
							copy += "\n|" + genericFireArm.getMuzzle();
							copy += "\n|" + genericFireArm.getFirerates();
							copy += "\n|" + genericFireArm.getMagsize();
							copy += "\n|" + genericFireArm.getMagno();
							copy += "\n|" + genericFireArm.getReload();
							if (Main.bf2objectDB.getByCode(genericFireArm.getProjectilename()) != null) {
								copy += "\n|" + ((Projectile) Main.bf2objectDB.getByCode(genericFireArm.getProjectilename())).getDmg();
								copy += "\n|" + ((Projectile) Main.bf2objectDB.getByCode(genericFireArm.getProjectilename())).getTTL()
										* genericFireArm.getMuzzle();
							}
							copy += "\n|" + genericFireArm.getZooms();
							copy += "\n"; // nicer output
						}
					}
					copy += "\n|}";
					copy += "\n{{Navbox}}";
					System.out.println(namelist);
					Clipboard cb = new Clipboard(Display.getDefault());
					TextTransfer tt = TextTransfer.getInstance();
					cb.setContents(new Object[] { copy }, new TextTransfer[] { tt });
					cb.dispose();
				}
			}
		});

		comp.layout();
		refreshTree(tree, t_filter1.getText(), t_filter2.getText(), t_filter3.getText(), t_filter4.getText(), t_filter5.getText(), t_filter6.getText(),
				t_filter7.getText());
		l_filterno.setText(String.valueOf(tree.getItemCount()));
	}

	private static void refreshTree(Tree tree, String filter1, String filter2, String filter3, String filter4, String filter5, String filter6, String filter7) {
		List<BF2Object> db = Main.bf2objectDB.getDB();
		tree.removeAll();

		boolean DEBUG = true;
		FileWriter f_debug = null;
		if (DEBUG == true)
			try {
				f_debug = new FileWriter(new File("debug_WeaponsTab.log"));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		Pattern pattern1 = Pattern.compile(filter1);
		Pattern pattern2 = Pattern.compile(filter2);
		Pattern pattern3 = Pattern.compile(filter3);
		Pattern pattern4 = Pattern.compile(filter4);
		Pattern pattern5 = Pattern.compile(filter5);
		Pattern pattern6 = Pattern.compile(filter6);
		Pattern pattern7 = Pattern.compile(filter7);

		for (int i = 0; i < db.size(); i++) {
			if (db.get(i).getClass() != GenericFireArm.class)
				continue;
			GenericFireArm w = (GenericFireArm) db.get(i);
			Matcher matcher1 = pattern1.matcher(w.getCode());
			Matcher matcher2 = pattern2.matcher(String.valueOf(w.getIndex()));
			Matcher matcher3 = pattern3.matcher(String.valueOf(w.getMagsize()));
			Matcher matcher4 = pattern4.matcher(String.valueOf(w.getMagno()));
			Matcher matcher5 = pattern5.matcher(String.valueOf(w.getReload()));
			Matcher matcher6 = pattern6.matcher(String.valueOf(w.getMuzzle()));
			Matcher matcher7 = pattern7.matcher("");
			if (w.getProjectilename() != null)
				matcher7 = pattern7.matcher(w.getProjectilename());
			if (matcher1.find() && matcher2.find() && matcher3.find() && matcher4.find() && matcher5.find() && matcher6.find() && matcher7.find()) {
				TreeItem item = new TreeItem(tree, SWT.NONE);
				if (DEBUG == true)
					try {
						f_debug.write("| " + w.getCode() + " = \n");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				//item.setText(new String[] { w.getCode() + " " + gui.Main.lang.getValue(w.getCode(), ""), String.valueOf(w.getIndex()),
				item.setText(new String[] { w.getCode(), String.valueOf(w.getIndex()), String.valueOf(w.getMagsize()), String.valueOf(w.getMagno()),
						String.valueOf(w.getReload()), String.valueOf(w.getMuzzle()), w.getProjectilename() });
			}
		}
		if (f_debug != null)
			try {
				f_debug.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

	private static void fillCombo(Combo c) {
		c.removeAll();
		c.add("Table");
		c.add("Infobox");
		c.select(0);
	}
}
