package gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.ArrayList;
import java.util.Properties;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import parsers.Locale;
import parsers.map.PrintFiles;
import parsers.map.parseMapGamemodes;
import parsers.map.parseMaps;
import parsers.map.parseMapsinit;
import common.WinRegistry;
import db.BF2ObjectDB;
import db.MaterialDB;
import db.map.BF2MapDB;

public class Main {
	static String appdir;
	public static Properties props;

	public static BF2ObjectDB bf2objectDB;
	public static BF2MapDB bf2mapdb;
	public static MaterialDB materialDB;

	public static String version = "1.0";
	public static String PR_version = "1.4.0.0";
	public static String url = "mailto:tassilo.m@gmail.com";
	public static PathMatcher matcher;
	public static Locale lang;

	public static Display display = new Display();
	public static Shell mainshell = new Shell(display);

	public static CTabFolder tabFolder = new CTabFolder(mainshell, SWT.BORDER);
	static CTabItem tab_kits = new CTabItem(tabFolder, SWT.NULL);
	static CTabItem tab_weapon = new CTabItem(tabFolder, SWT.NULL);
	static CTabItem tab_map = new CTabItem(tabFolder, SWT.NONE);
	static CTabItem tab_vehicle = new CTabItem(tabFolder, SWT.NULL);
	static CTabItem tab_settings = new CTabItem(tabFolder, SWT.NULL);

	static Composite comp_kits = new Composite(tabFolder, SWT.NONE);
	static Composite comp_weapon = new Composite(tabFolder, SWT.NONE);
	static Composite comp_map = new Composite(tabFolder, SWT.NONE);
	static Composite comp_vehicle = new Composite(tabFolder, SWT.NONE);
	static Composite comp_settings = new Composite(tabFolder, SWT.NONE);

	public static Link copyright_link = new Link(mainshell, SWT.NONE);

	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, IOException {

		gui.SettingsTab.initLanguage();
		tabFolder.setLayoutData(new FormData());
		appdir = new File(".").getAbsolutePath();

		materialDB = MaterialDB.readDB();
		bf2objectDB = BF2ObjectDB.readDB();
		bf2mapdb = BF2MapDB.readDB();

		mainshell.setText("WGPWiki-Parser v" + version);

		initUI();
		mainshell.pack();
		mainshell.setSize(mainshell.computeSize(800, 640, true)); // Launches min size

		tabFolder.setSelection(4);
		mainshell.open();

		tabFolder.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				if (tabFolder.getSelection() == tab_weapon)
					initUI_weapon();
				else if (tabFolder.getSelection() == tab_map)
					initUI_map();
				else if (tabFolder.getSelection() == tab_vehicle)
					initUI_vehicle();
				else if (tabFolder.getSelection() == tab_settings)
					initUI_settings();
				else if (tabFolder.getSelection() == tab_kits)
					initUI_kits();
			}
		});

		// Opens wargamer project support page
		copyright_link.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				org.eclipse.swt.program.Program.launch(url);
			}
		});

		while (!mainshell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}

		display.dispose();
	}

	public static void initUI_map() {
		tab_map.setControl(comp_map);
		gui.MapsTab.initUI(comp_map);
	}

	public static void initUI_weapon() {
		tab_weapon.setControl(comp_weapon);
		gui.WeaponsTab.initUI(comp_weapon);
	}

	public static void initUI_vehicle() {
		tab_vehicle.setControl(comp_vehicle);
		gui.VehiclesTab.initUI(comp_vehicle);
	}

	public static void initUI_settings() {
		tab_settings.setControl(comp_settings);
		gui.SettingsTab.initUI(comp_settings);
	}

	public static void initUI_kits() {
		tab_kits.setControl(comp_kits);
		gui.KitsTab.initUI(comp_kits);
	}

	public static void initUI() {

		mainshell.setMinimumSize(600, 400);
		mainshell.setLayout(new FormLayout());
		tab_weapon.setText("Weapons");
		tab_map.setText("Maps");
		tab_vehicle.setText("Vehicles");
		tab_settings.setText("Settings");
		tab_kits.setText("Kits");
		FormData fd_tabfolder = new FormData();
		fd_tabfolder.left = new FormAttachment(mainshell, 5, SWT.LEFT);
		fd_tabfolder.right = new FormAttachment(100, -5);
		fd_tabfolder.top = new FormAttachment(mainshell, 5, SWT.TOP);
		fd_tabfolder.bottom = new FormAttachment(100, -35);
		tabFolder.setLayoutData(fd_tabfolder);

		FormData fd_copyright_link = new FormData();
		fd_copyright_link.bottom = new FormAttachment(100, -5);
		fd_copyright_link.right = new FormAttachment(100, -5);
		copyright_link.setToolTipText("senshi@wargamer-project.de");
		copyright_link.setLayoutData(fd_copyright_link);
		copyright_link.setText("<a href=\"http://www.wargamer-project.de\">\u00a9 [WGP]Senshi</a>");

		//initUI_kits();
		initUI_settings();
		//initUI_vehicle();
		//initUI_weapon();
	}

	public static void getSettings() {
		props = new Properties();
		InputStream is;

		// Try loading from current dir
		try {
			File f = new File("settings.properties");
			is = new FileInputStream(f);
		} catch (Exception e) {
			is = null;
		}

		try {
			props.load(is);
			is.close();
		} catch (Exception e) {
			System.out.println("Failed to load settings.");
		}
	}

	public static void saveSettings() {
		try {
			if (props == null) {
				props = new Properties();
			}
			File f = new File("settings.properties");

			OutputStream out = new FileOutputStream(f);
			props.store(out, "");
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
