package gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Text;

import common.WinRegistry;
import parsers.map.PrintFiles;
import parsers.map.parseMapGamemodes;
import parsers.map.parseMaps;
import parsers.map.parseMapsinit;

public class MapsTab {
	// comp_map elements

	public static void initUI(final Composite comp) {
		final Label dir_m_label = new Label(comp, 0);
		final Text dir_m = new Text(comp, SWT.BORDER);
		final List list_m = new List(comp, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		final Label log_label1_m = new Label(comp, 0);
		final Label log_label2_m = new Label(comp, 0);
		final Label list_label_m = new Label(comp, 0);
		final Text log_team1 = new Text(comp, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		final Text log_team2 = new Text(comp, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		final Button b_minimap = new Button(comp, 0);
		final List gamemode_list = new List(comp, SWT.BORDER | SWT.V_SCROLL);
		final ArrayList<String> filelist_gm = new ArrayList<String>();
		final Label gamemode_list_label = new Label(comp, 0);
		final ArrayList<Path> maplist = new ArrayList<Path>();
		final ArrayList<Path> maplist_selection = new ArrayList<Path>();
		final Sash separator_h_m = new Sash(comp, SWT.BORDER | SWT.HORIZONTAL);

		String[] team1 = { "", "", "" };
		String[] team2 = { "", "", "" };
		final int mapsize = 0;

		comp.setLayout(new FormLayout());

		FormData dir_m_label_ld = new FormData(SWT.DEFAULT, 20);
		dir_m_label_ld.left = new FormAttachment(0, 5);
		dir_m_label_ld.top = new FormAttachment(0, 5);
		dir_m_label.setLayoutData(dir_m_label_ld);
		dir_m_label.setText("Directory:");

		FormData dir_m_ld = new FormData(SWT.DEFAULT, 20);
		dir_m_ld.left = new FormAttachment(dir_m_label, 5, SWT.RIGHT);
		dir_m_ld.right = new FormAttachment(100, -5);
		dir_m_ld.top = new FormAttachment(dir_m_label, 0, SWT.CENTER);
		dir_m.setLayoutData(dir_m_ld);

		FormData list_label_m_data = new FormData(SWT.DEFAULT, 20);
		list_label_m_data.top = new FormAttachment(dir_m, 5, SWT.BOTTOM);
		list_label_m_data.left = new FormAttachment(0, 5);
		list_label_m_data.right = new FormAttachment(50, 0);
		list_label_m.setLayoutData(list_label_m_data);
		list_label_m.setText("Maps to parse (use Ctrl/Shift to select multiple maps)");

		FormData gamemode_list_label_layoutdata = new FormData(SWT.DEFAULT, 20);
		gamemode_list_label_layoutdata.top = new FormAttachment(dir_m, 5, SWT.BOTTOM);
		gamemode_list_label_layoutdata.left = new FormAttachment(50, 5);
		gamemode_list_label_layoutdata.right = new FormAttachment(100, -5);
		gamemode_list_label.setLayoutData(gamemode_list_label_layoutdata);
		gamemode_list_label.setText("Select gamemode to parse vehicle lists:");

		FormData listlayoutdata = new FormData();
		listlayoutdata.bottom = new FormAttachment(separator_h_m, -5, SWT.TOP);
		listlayoutdata.top = new FormAttachment(list_label_m, 5, SWT.BOTTOM);
		listlayoutdata.left = new FormAttachment(0, 5);
		listlayoutdata.right = new FormAttachment(50, -5);
		list_m.setLayoutData(listlayoutdata);

		FormData gamemode_listlayoutdata = new FormData();
		gamemode_listlayoutdata.bottom = new FormAttachment(separator_h_m, -5, SWT.TOP);
		gamemode_listlayoutdata.top = new FormAttachment(gamemode_list_label, 5, SWT.BOTTOM);
		gamemode_listlayoutdata.left = new FormAttachment(50, 5);
		gamemode_listlayoutdata.right = new FormAttachment(100, -5);
		gamemode_list.setLayoutData(gamemode_listlayoutdata);

		FormData b_minimap_layoutdata = new FormData(60, 25);
		b_minimap_layoutdata.top = new FormAttachment(log_label1_m, 0, SWT.CENTER);
		b_minimap_layoutdata.right = new FormAttachment(100, -5);
		b_minimap.setLayoutData(b_minimap_layoutdata);
		b_minimap.setText("Minimap");
		b_minimap.setToolTipText("Display the minimap.");
		if (gamemode_list.getSelectionCount() == 0)
			b_minimap.setVisible(false);

		FormData log_labeldata = new FormData(SWT.DEFAULT, 20);
		log_labeldata.top = new FormAttachment(separator_h_m, 0, SWT.BOTTOM);
		log_labeldata.left = new FormAttachment(0, 5);
		log_labeldata.right = new FormAttachment(50, -5);
		log_label1_m.setLayoutData(log_labeldata);
		log_label1_m.setText("Log Team 1:");

		FormData log_label2data = new FormData(SWT.DEFAULT, 20);
		log_label2data.top = new FormAttachment(separator_h_m, 0, SWT.BOTTOM);
		log_label2data.left = new FormAttachment(50, 5);
		log_label2data.right = new FormAttachment(b_minimap, -5);
		log_label2_m.setLayoutData(log_label2data);
		log_label2_m.setText("Log Team 2:");

		FormData log_team1_ld = new FormData();
		log_team1_ld.left = new FormAttachment(0, 5);
		log_team1_ld.right = new FormAttachment(50, -5);
		log_team1_ld.bottom = new FormAttachment(100, -5);
		log_team1_ld.top = new FormAttachment(log_label1_m, 5, SWT.BOTTOM);
		log_team1.setLayoutData(log_team1_ld);

		FormData log_team2_ld = new FormData();
		log_team2_ld.left = new FormAttachment(50, 5);
		log_team2_ld.right = new FormAttachment(100, -5);
		log_team2_ld.bottom = new FormAttachment(100, -5);
		log_team2_ld.top = new FormAttachment(log_label1_m, 5, SWT.BOTTOM);
		log_team2.setLayoutData(log_team2_ld);

		final FormData separatorlayoutdata = new FormData(-1, -1);
		separatorlayoutdata.bottom = new FormAttachment(50, 5);
		separatorlayoutdata.left = new FormAttachment(0, 0);
		separatorlayoutdata.right = new FormAttachment(100, 0);
		separatorlayoutdata.height = 3;
		separator_h_m.setLayoutData(separatorlayoutdata);
		

		dir_m.setText(Main.props.getProperty("MapPath", ""));
		if (dir_m.getText().length() > 0 && list_m.getItemCount() == 0)
			try {
				walkDirectory();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		comp.layout();		
		
		
		separator_h_m.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				Rectangle sashRect = separator_h_m.getBounds();
				Rectangle shellRect = comp.getClientArea();
				int top = shellRect.height - sashRect.height - 100;
				e.y = Math.max(Math.min(e.y, top), 200);
				if (e.y != sashRect.y) {
					separatorlayoutdata.bottom = new FormAttachment(0, e.y);
				}
				comp.layout(true);
			}
		});
		


		list_m.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				log_team1.setText("");
				log_team2.setText("");
				gamemode_list.removeAll();
				int[] selectionIndices = list_m.getSelectionIndices();

				maplist_selection.clear();
				for (int i = 0; i < list_m.getSelectionCount(); i++) {
					maplist_selection.add(maplist.get(selectionIndices[i]));
				}

				b_minimap.setVisible(false);

				for (int i = 0; i < list_m.getSelectionCount(); i++) {
					try {
						parseMaps.processFile(maplist_selection.get(i).toString());
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
						log_team1.insert("Level data could not be processed! Make sure you have extraced the level data!\n");
					}
				}
				// Gotta clear, as multiparsing gamemodes makes no sense
				if (selectionIndices.length > 1)
					gamemode_list.removeAll();
				parseMaps.no = 0;
				parseMapsinit.no = 0;

			}
		});

		gamemode_list.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				if (gamemode_list.getSelectionIndex() != -1) {
					b_minimap.setVisible(true);
					log_team1.setText("");
					log_team2.setText("");

					int selectionIndex = list_m.getSelectionIndex();
					maplist_selection.clear();
					String filename = maplist.get(selectionIndex).toString();
					String[] dummy = filename.split("\\\\");
					String gamemodepath = dummy[0];
					for (int i = 1; !dummy[i].equals("info"); i++) {
						gamemodepath = gamemodepath + "\\" + dummy[i];
					}
					gamemodepath = gamemodepath + "\\GameModes";
					String initmodus = "";
					String gm_selection = filelist_gm.get(gamemode_list.getSelectionIndex());
					switch (gm_selection) {
					case "gpm_cnc_inf":
						gamemodepath = gamemodepath + "\\gpm_cnc\\16\\GamePlayObjects.con";
						break;
					case "gpm_cnc_alt":
						gamemodepath = gamemodepath + "\\gpm_cnc\\32\\GamePlayObjects.con";
						break;
					case "gpm_cnc_std":
						gamemodepath = gamemodepath + "\\gpm_cnc\\64\\GamePlayObjects.con";
						break;
					case "gpm_aas_inf":
						gamemodepath = gamemodepath + "\\gpm_cq\\16\\GamePlayObjects.con";
						break;
					case "gpm_aas_alt":
						gamemodepath = gamemodepath + "\\gpm_cq\\32\\GamePlayObjects.con";
						break;
					case "gpm_aas_std":
						gamemodepath = gamemodepath + "\\gpm_cq\\64\\GamePlayObjects.con";
						break;
					case "gpm_aas_128":
						gamemodepath = gamemodepath + "\\gpm_cq\\128\\GamePlayObjects.con";
						break;
					case "gpm_insurgency_inf":
						initmodus = "insurgency";
						gamemodepath = gamemodepath + "\\gpm_insurgency\\16\\GamePlayObjects.con";
						break;
					case "gpm_insurgency_alt":
						initmodus = "insurgency";
						gamemodepath = gamemodepath + "\\gpm_insurgency\\32\\GamePlayObjects.con";
						break;
					case "gpm_insurgency_std":
						initmodus = "insurgency";
						gamemodepath = gamemodepath + "\\gpm_insurgency\\64\\GamePlayObjects.con";
						break;
					case "gpm_skirmish_inf":
						gamemodepath = gamemodepath + "\\gpm_skirmish\\16\\GamePlayObjects.con";
						break;
					case "gpm_skirmish_alt":
						gamemodepath = gamemodepath + "\\gpm_skirmish\\32\\GamePlayObjects.con";
						break;
					case "gpm_skirmish_std":
						gamemodepath = gamemodepath + "\\gpm_skirmish\\64\\GamePlayObjects.con";
						break;
					case "gpm_vehicles_inf":
						initmodus = "vehicles";
						gamemodepath = gamemodepath + "\\gpm_vehicles\\16\\GamePlayObjects.con";
						break;
					case "gpm_vehicles_alt":
						initmodus = "vehicles";
						gamemodepath = gamemodepath + "\\gpm_vehicles\\32\\GamePlayObjects.con";
						break;
					case "gpm_vehicles_std":
						initmodus = "vehicles";
						gamemodepath = gamemodepath + "\\gpm_vehicles\\64\\GamePlayObjects.con";
						break;
					case "gpm_coop_inf":
						gamemodepath = gamemodepath + "\\gpm_coop\\16\\GamePlayObjects.con";
						break;
					case "gpm_coop_alt":
						gamemodepath = gamemodepath + "\\gpm_coop\\32\\GamePlayObjects.con";
						break;
					case "gpm_coop_std":
						gamemodepath = gamemodepath + "\\gpm_coop\\64\\GamePlayObjects.con";
						break;
					}
					File gamemodefile = new File(gamemodepath);
					if (gamemodefile.exists()) {
						try {
							parseMapGamemodes.processFile(gamemodefile, initmodus);
						} catch (FileNotFoundException e1) {
							e1.printStackTrace();
							log_team1.insert("Gamemode data could not be processed!\n Make sure you have extraced the level data properly!\n");
							log_team2.insert("Gamemode data could not be processed!\n Make sure you have extraced the level data properly!\n");
						}
					}
					parseMaps.no = 0;
					parseMapsinit.no = 0;
				}
			}
		});

		b_minimap.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (list_m.getSelectionCount() > 0) {
					if (gamemode_list.getSelectionCount() > 0)
						try {
							Path name = maplist.get(list_m.getSelectionIndex()).getFileName();
							String mapname = name.toString().substring(0, name.toString().lastIndexOf("."));
							minimap.GUI.init(mapname, gamemode_list.getSelection()[0], mapsize, parseMapGamemodes.vehicles, parseMapGamemodes.flags);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
				} else
					try {
						minimap.GUI.init("kashan_desert", "gpm_cq_64", 4096, null, null);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				b_minimap.setEnabled(false);
			}
		});

	}

	public static void walkDirectory() throws IOException {
		list_m.removeAll();
		maplist.clear();
		log_team1.setText("");
		log_team2.setText("");
		dir_m.update();
		matcher = FileSystems.getDefault().getPathMatcher("glob:*.{desc}");
		log_team1.insert("Searching for *.desc in: " + dir_m.getText() + "\n");
		Path check = FileSystems.getDefault().getPath(dir_m.getText());
		PrintFiles pf = new PrintFiles(modus);
		// Files.walkFileTree(check, EnumSet.noneOf(FileVisitOption.class),
		// scandepth, pf);
		Files.walkFileTree(check, pf);
		pf.bar.dispose();

	}
}
