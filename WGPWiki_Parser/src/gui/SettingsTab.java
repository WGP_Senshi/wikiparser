package gui;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import minimap.Minimap;
import net.sourceforge.jwbf.core.contentRep.Article;
import net.sourceforge.jwbf.mediawiki.bots.MediaWikiBot;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import parsers.Locale;
import parsers.PNGtoJPG;
import common.PBar;
import common.Utils;
import common.WinRegistry;
import db.BF2ObjectDB;
import db.MaterialDB;
import db.map.parser.printGeoJSON;

public class SettingsTab {

	public static void initUI(Composite comp) {
		comp.setLayout(new FormLayout());

		Button b_lang = new Button(comp, 0);
		Button b_extractcontent = new Button(comp, 0);
		Button b_rebuildobjects = new Button(comp, 0);
		Button b_extractLevels = new Button(comp, 0);
		Button b_rebuildatlas = new Button(comp, 0);
		Button b_rebuildmat = new Button(comp, 0);
		Button b_rebuildmaps = new Button(comp, 0);
		Button b_rebuildminimaps = new Button(comp, 0);
		Button b_batchvonertpngtojpg = new Button(comp, 0);

		Label l_wiki = new Label(comp, 0);
		Label l_wiki_user = new Label(comp, 0);
		final Text t_wiki_user = new Text(comp, SWT.BORDER);
		Label l_wiki_pass = new Label(comp, 0);
		final Text t_wiki_pass = new Text(comp, SWT.PASSWORD | SWT.BORDER);
		Label l_wiki_url = new Label(comp, 0);
		final Text t_wiki_url = new Text(comp, SWT.BORDER);
		Button b_wiki_save = new Button(comp, 0);

		final List l_log = new List(comp, SWT.BORDER);
		l_log.add("LOG");

		FormData b_lang_fd = new FormData();
		b_lang_fd.top = new FormAttachment(0, 5);
		b_lang_fd.left = new FormAttachment(0, 5);
		b_lang.setLayoutData(b_lang_fd);
		b_lang.setText("Language");
		b_lang.setToolTipText("Choose a different language");

		FormData b_extractcontent_fd = new FormData();
		b_extractcontent_fd.top = new FormAttachment(0, 5);
		b_extractcontent_fd.left = new FormAttachment(b_lang, 5);
		b_extractcontent.setLayoutData(b_extractcontent_fd);
		b_extractcontent.setText("Extract content");
		b_extractcontent.setToolTipText("Extracts all content");

		FormData b_extractlevels_fd = new FormData();
		b_extractlevels_fd.top = new FormAttachment(0, 5);
		b_extractlevels_fd.left = new FormAttachment(b_extractcontent, 5);
		b_extractLevels.setLayoutData(b_extractlevels_fd);
		b_extractLevels.setText("Extract Levels");
		b_extractLevels.setToolTipText("Extracts all level zip files");

		FormData b_atlasgo_fd = new FormData();
		b_atlasgo_fd.top = new FormAttachment(0, 5);
		b_atlasgo_fd.left = new FormAttachment(b_extractLevels, 5);
		b_rebuildatlas.setLayoutData(b_atlasgo_fd);
		b_rebuildatlas.setText("Extract Textures");
		b_rebuildatlas.setToolTipText("Extracts the GUI Texture atlas files");

		FormData b_map_fd = new FormData();
		b_map_fd.top = new FormAttachment(0, 5);
		b_map_fd.left = new FormAttachment(b_rebuildatlas, 5);
		b_rebuildmaps.setLayoutData(b_map_fd);
		b_rebuildmaps.setText("Build Minimaps");
		b_rebuildmaps.setToolTipText("Builds Minimaps");

		FormData b_rebuildobjects_fd = new FormData();
		b_rebuildobjects_fd.top = new FormAttachment(0, 5);
		b_rebuildobjects_fd.left = new FormAttachment(b_rebuildmaps, 5);
		b_rebuildobjects.setLayoutData(b_rebuildobjects_fd);
		b_rebuildobjects.setText("Build ObjectsDB");
		b_rebuildobjects.setToolTipText("Rebuilds the BF2Object database");

		FormData b_mat_fd = new FormData();
		b_mat_fd.top = new FormAttachment(0, 5);
		b_mat_fd.left = new FormAttachment(b_rebuildobjects, 5);
		b_rebuildmat.setLayoutData(b_mat_fd);
		b_rebuildmat.setText("Build Materials");
		b_rebuildmat.setToolTipText("Builds Material Database");

		FormData b_minimap_fd = new FormData();
		b_minimap_fd.top = new FormAttachment(0, 5);
		b_minimap_fd.left = new FormAttachment(b_rebuildmat, 5);
		b_rebuildminimaps.setLayoutData(b_minimap_fd);
		b_rebuildminimaps.setText("Build MapsDB");
		b_rebuildminimaps.setToolTipText("Builds Map database");

		FormData b_batchconvert_fd = new FormData();
		b_batchconvert_fd.top = new FormAttachment(0, 5);
		b_batchconvert_fd.left = new FormAttachment(b_rebuildminimaps, 5);
		b_batchvonertpngtojpg.setLayoutData(b_batchconvert_fd);
		b_batchvonertpngtojpg.setText("PNG->JPG");
		b_batchvonertpngtojpg.setToolTipText("Converts all png to jpg");

		// Row 2
		FormData l_wiki_fd = new FormData();
		l_wiki_fd.top = new FormAttachment(b_lang, 5, SWT.BOTTOM);
		l_wiki_fd.left = new FormAttachment(0, 5);
		l_wiki.setLayoutData(l_wiki_fd);
		l_wiki.setText("Wiki settings |");

		FormData l_wiki_user_fd = new FormData();
		l_wiki_user_fd.top = new FormAttachment(b_lang, 5, SWT.BOTTOM);
		l_wiki_user_fd.left = new FormAttachment(l_wiki, 5);
		l_wiki_user.setLayoutData(l_wiki_user_fd);
		l_wiki_user.setText("User:");

		FormData t_user_fd = new FormData(100, SWT.DEFAULT);
		t_user_fd.top = new FormAttachment(l_wiki_user, 0, SWT.CENTER);
		t_user_fd.left = new FormAttachment(l_wiki_user, 5);
		t_wiki_user.setLayoutData(t_user_fd);
		t_wiki_user.setText("");
		t_wiki_user.setToolTipText("Enter your username on the wiki");

		FormData l_wiki_pass_fd = new FormData();
		l_wiki_pass_fd.top = new FormAttachment(b_lang, 5, SWT.BOTTOM);
		l_wiki_pass_fd.left = new FormAttachment(t_wiki_user, 5);
		l_wiki_pass.setLayoutData(l_wiki_pass_fd);
		l_wiki_pass.setText("Pass:");

		FormData t_wikipass_fd = new FormData(100, SWT.DEFAULT);
		t_wikipass_fd.top = new FormAttachment(l_wiki_pass, 0, SWT.CENTER);
		t_wikipass_fd.left = new FormAttachment(l_wiki_pass, 5);
		t_wiki_pass.setLayoutData(t_wikipass_fd);
		t_wiki_pass.setText("");
		t_wiki_pass.setToolTipText("Enter your password on the wiki");

		FormData l_wiki_url_fd = new FormData();
		l_wiki_url_fd.top = new FormAttachment(b_lang, 5, SWT.BOTTOM);
		l_wiki_url_fd.left = new FormAttachment(t_wiki_pass, 5);
		l_wiki_url.setLayoutData(l_wiki_url_fd);
		l_wiki_url.setText("URL:");

		FormData t_wiki_url_fd = new FormData(200, SWT.DEFAULT);
		t_wiki_url_fd.top = new FormAttachment(l_wiki_url, 0, SWT.CENTER);
		t_wiki_url_fd.left = new FormAttachment(l_wiki_url, 5);
		t_wiki_url.setLayoutData(t_wiki_url_fd);
		t_wiki_url.setText("http://tournament.realitymod.com/wiki/");
		t_wiki_url.setToolTipText("The wiki URL");

		FormData b_wiki_save_fd = new FormData();
		b_wiki_save_fd.top = new FormAttachment(l_wiki_url, 0, SWT.CENTER);
		b_wiki_save_fd.left = new FormAttachment(t_wiki_url, 5);
		b_wiki_save.setLayoutData(b_wiki_save_fd);
		b_wiki_save.setText("Remember*");
		b_wiki_save.setToolTipText("*WARNING: This data is stored in CLEAR TEXT!");
		// Row 3

		FormData l_log_fd = new FormData();
		l_log_fd.bottom = new FormAttachment(100, -5);
		l_log_fd.top = new FormAttachment(t_wiki_user, 5, SWT.BOTTOM);
		l_log_fd.left = new FormAttachment(0, 5);
		l_log_fd.right = new FormAttachment(100, -5);
		l_log.setLayoutData(l_log_fd);

		init_UI(t_wiki_url, t_wiki_user, t_wiki_pass);
		comp.layout();

		b_lang.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					chooseLanguage(Main.lang);
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				}
			}
		});

		b_extractcontent.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					File path = new File(Utils.openDirectorydialog("", "Extract game content",
							"Select the mod's main folder which contains clientarchives.con and serverarchives.con"));
					parsers.extractContent.parse(path, l_log);
					//if (path.exists())
					//parsers.parseProjectiles.parseSource(path, l_log);
					//Main.projectileDB = ProjectileDB.readDB();
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NullPointerException e1) {
					e1.printStackTrace();
				}
			}
		});

		b_rebuildobjects.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					File path = new File(Utils.openDirectorydialog("objects\\", "Rebuild BF2 objects database", "Select ...\\objects"));
					if (path.exists())
						bf2objectparser.parseINIT.parseSource(path, l_log);
					Main.bf2objectDB = BF2ObjectDB.readDB();
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NullPointerException e1) {
					e1.printStackTrace();
				}
			}
		});

		b_extractLevels.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					File path = new File(Utils.openDirectorydialog("levels", "Extract level zip files", "Select ...\\levels where all levels reside"));
					if (path.exists())
						parsers.unzipLevels.unzip(path, l_log);
					//parsers.parseVehicleComponents.parseSource(path, l_log);
					Main.bf2objectDB = BF2ObjectDB.readDB();
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NullPointerException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		b_rebuildatlas.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					String path = Utils.openFiledialog("menu/atlas/atlas", "atlaslist.con", "Select the atlaslist.con of your mod", new String[] { "*.con" });
					if (path != null)
						if (new File(path).exists())
							parsers.AtlasExtractor.processCon(path, "db\\atlas", l_log);
				} catch (FileNotFoundException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		b_rebuildmat.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					File path = new File(Utils.openDirectorydialog("common\\material", "Rebuild materials database", "Select ...\\common\\material"));
					if (path.exists())
						parsers.parseMaterials.parseSource(path, l_log);
					Main.materialDB = MaterialDB.readDB();
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NullPointerException e1) {
					e1.printStackTrace();
				}
			}
		});

		b_rebuildmaps.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
					l_log.add(dateFormat.format(new Date()) + " : Extracting minimaps. This might take a couple of minutes.");
					File path = new File(Utils.openDirectorydialog("levels", "Rebuild minimaps", "Select ...\\levels"));
					if (path.exists()) {
						minimap.Minimap.parseSource(path, l_log);
						l_log.add(dateFormat.format(new Date()) + " : Extraction of minimaps complete.");
					}
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NullPointerException e1) {
					return;
				}
			}
		});

		b_rebuildminimaps.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
					l_log.add(dateFormat.format(new Date()) + " : Building MapsDB. This might take a bit.");
					File path = new File(Utils.openDirectorydialog("levels", "Rebuild MapDB", "Select ...\\levels"));
					if (path.exists()) {
						//db.map.parser.parseINIT.parseSource(path, l_log);
						printGeoJSON.print(Main.bf2mapdb);
						l_log.add(dateFormat.format(new Date()) + " : MapDB build complete.");
					}

				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (NullPointerException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		b_batchvonertpngtojpg.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
					l_log.add(dateFormat.format(new Date()) + " : Convert PNG to JPG.");
					File path = new File(Utils.openDirectorydialog("C:\\xampp\\htdocs\\prmap_leaf\\tiles", "Rebuild MapDB", "Select ...\\levels"));
					if (path.exists()) {
						PNGtoJPG.parse(path.toPath(), true);
						db.map.parser.parseINIT.parseSource(path, l_log);
						printGeoJSON.print(Main.bf2mapdb);
						l_log.add(dateFormat.format(new Date()) + " : MapDB build complete.");
					}

				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (NullPointerException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		b_wiki_save.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					Main.props.setProperty("wiki_user", t_wiki_user.getText());
					Main.props.setProperty("wiki_pass", t_wiki_pass.getText());
					Main.props.setProperty("wiki_url", t_wiki_url.getText());
					Main.saveSettings();
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (NullPointerException e1) {
					return;
				}
			}
		});
	}

	private static void init_UI(Text t_wiki_url, Text t_wiki_user, Text t_wiki_pass) {
		t_wiki_url.setText(Main.props.getProperty("wiki_url", "http://tournament.realitymod.com/wiki/"));
		t_wiki_user.setText(Main.props.getProperty("wiki_user", ""));
		t_wiki_pass.setText(Main.props.getProperty("wiki_pass", ""));

	}

	public static void initLanguage() {
		gui.Main.getSettings();
		String language = gui.Main.props.getProperty("Language");
		if (language != null)
			setLanguage(language);
		else {
			chooseLanguage(Main.lang);
		}
	}

	public static void chooseLanguage(Locale lang) {
		final Shell shell = new Shell(gui.Main.mainshell, SWT.TITLE | SWT.APPLICATION_MODAL | SWT.RESIZE | SWT.CLOSE | SWT.MAX);
		shell.setText("Language Selector");
		shell.setLayout(new FormLayout());
		shell.setMinimumSize(160, 240);
		final List langlist = new List(shell, SWT.BORDER | SWT.SINGLE | SWT.READ_ONLY);
		Label instruction = new Label(shell, 0);
		instruction.setText("Please choose your preferred language.\nThis will be used to display the localized names of all items in the tool");

		FormData langlist_ld = new FormData();
		langlist_ld.top = new FormAttachment(instruction, 5, SWT.BOTTOM);
		langlist_ld.bottom = new FormAttachment(100, -5);
		langlist_ld.left = new FormAttachment(0, 5);
		langlist_ld.right = new FormAttachment(100, -5);
		langlist.setLayoutData(langlist_ld);

		FormData instruction_ld = new FormData();
		instruction_ld.top = new FormAttachment(0, 5);
		instruction_ld.left = new FormAttachment(0, 5);
		instruction_ld.right = new FormAttachment(100, -5);
		instruction.setLayoutData(instruction_ld);

		langlist.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				String selection = langlist.getItem(langlist.getSelectionIndex());
				setLanguage(selection);
				gui.Main.props.setProperty("Language", selection);
				gui.Main.saveSettings();
				shell.dispose();
			}
		});

		DirectoryDialog dialog_parsedir = new DirectoryDialog(Main.mainshell);
		dialog_parsedir.setMessage("Pick the folder where the extracted localization folders are stored.\nDefault is mods\\MODNAME\\localization\\");
		// read Battlefield 2\mods\pr\objects\weapons\Handheld directory
		// Two checks to cover 64bit/32bit registries
		String bf2path = null;

		try {
			bf2path = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\Wow6432Node\\Project Reality\\Project Reality: BF2", "InstallDir");
			if (bf2path == null)
				bf2path = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\Project Reality\\Project Reality: BF2", "InstallDir");
			if (bf2path == null)
				bf2path = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\Wow6432Node\\Electronic Arts\\EA GAMES\\Battlefield 2",
						"InstallDir");
			if (bf2path == null)
				bf2path = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\Electronic Arts\\EA GAMES\\Battlefield 2", "InstallDir");
		} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Checks if pr_wgp_edit or pr_edit folder exist, sets them to default
		// if available
		String proppath;
		String dir_chosen = null;
		if (Main.props != null) {
			proppath = Main.props.getProperty("LocalePath", "");
			if (proppath != "") {
				dir_chosen = proppath;
			} else {
				String prwgpeditpath = bf2path + "mods\\pr_wgp_edit\\localization\\";
				String preditpath = bf2path + "mods\\pr_edit\\localization\\";

				File prwgpeditdirectory = new File(prwgpeditpath);
				File preditdirectory = new File(preditpath);

				if (prwgpeditdirectory.exists()) {
					dialog_parsedir.setFilterPath(prwgpeditpath);
				} else if (preditdirectory.exists()) {
					dialog_parsedir.setFilterPath(preditpath);
				} else if (bf2path != null) {
					dialog_parsedir.setFilterPath(bf2path);
				}

				dir_chosen = dialog_parsedir.open();
				if (dir_chosen != null) {
					Main.props.setProperty("LocalePath", dir_chosen);
					Main.saveSettings();
				}

			}

			FileFilter fileFilter = new FileFilter() {
				@Override
				public boolean accept(File file) {
					return file.isDirectory();
				}
			};
			File[] languages = new File(dir_chosen).listFiles(fileFilter);
			for (File l : languages) {
				String lname = l.getName();
				langlist.add(lname);
			}

			shell.pack();
			shell.open();
			while (!shell.isDisposed()) {
				if (!gui.Main.display.readAndDispatch())
					gui.Main.display.sleep();
			}

		}
	}

	public static void setLanguage(String lang) {
		Main.lang = new Locale(lang);
	}

}
