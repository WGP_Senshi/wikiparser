package gui;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

import db.BF2Object;
import db.GenericFireArm;
import db.Kit;

public class KitsTab {

	private static Timer timer;
	private static List<Kit> kitdb = new ArrayList<Kit>();

	public static void initUI(final Composite comp) {
		for (BF2Object obj : Main.bf2objectDB.getDB()) {
			try {
				kitdb.add((Kit) obj);
			} catch (ClassCastException e) {
			}
		}

		FileWriter f_debug;
		try {
			f_debug = new FileWriter(new File("debug_KitTab.log"));
			for (Kit kit : kitdb)
				f_debug.write(kit.getCode() + "\n");
			f_debug.close();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		comp.setLayout(new FormLayout());

		final Tree tree = new Tree(comp, SWT.BORDER | SWT.MULTI | SWT.FULL_SELECTION | SWT.H_SCROLL | SWT.VIRTUAL);
		tree.setHeaderVisible(true);
		tree.setLinesVisible(true);
		TreeColumn column1 = new TreeColumn(tree, SWT.LEFT);
		column1.setText("Name");
		column1.setWidth(200);
		TreeColumn column2 = new TreeColumn(tree, SWT.LEFT);
		column2.setText("Slot 1");
		column2.setWidth(150);
		TreeColumn column3 = new TreeColumn(tree, SWT.LEFT);
		column3.setText("Slot 2");
		column3.setWidth(150);
		TreeColumn column4 = new TreeColumn(tree, SWT.LEFT);
		column4.setText("Slot 3");
		column4.setWidth(150);
		TreeColumn column5 = new TreeColumn(tree, SWT.LEFT);
		column5.setText("Slot 4");
		column5.setWidth(150);
		TreeColumn column6 = new TreeColumn(tree, SWT.LEFT);
		column6.setText("Slot 5");
		column6.setWidth(150);
		TreeColumn column7 = new TreeColumn(tree, SWT.LEFT);
		column7.setText("Slot 6");
		column7.setWidth(150);
		TreeColumn column8 = new TreeColumn(tree, SWT.LEFT);
		column8.setText("Slot 7");
		column8.setWidth(150);
		TreeColumn column9 = new TreeColumn(tree, SWT.LEFT);
		column9.setText("Slot 8");
		column9.setWidth(150);
		TreeColumn column10 = new TreeColumn(tree, SWT.LEFT);
		column10.setText("Slot 9");
		column10.setWidth(150);
		final Text t_filter1 = new Text(comp, SWT.BORDER);
		final Text t_filter2 = new Text(comp, SWT.BORDER);
		final Text t_filter3 = new Text(comp, SWT.BORDER);
		final Text t_filter4 = new Text(comp, SWT.BORDER);
		final Text t_filter5 = new Text(comp, SWT.BORDER);
		final Text t_filter6 = new Text(comp, SWT.BORDER);
		final Text t_filter7 = new Text(comp, SWT.BORDER);
		final Text t_filter8 = new Text(comp, SWT.BORDER);
		final Text t_filter9 = new Text(comp, SWT.BORDER);
		final Text t_filter10 = new Text(comp, SWT.BORDER);
		Label log_label = new Label(comp, 0);
		Label list_label = new Label(comp, 0);
		Text log = new Text(comp, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		final Canvas icon = new Canvas(comp, 0);
		Button b_copy = new Button(comp, 0);
		final Label l_filterno = new Label(comp, 0);
		final Sash separator_h = new Sash(comp, SWT.BORDER | SWT.HORIZONTAL);

		comp.setLayout(new FormLayout());
		log_label.setText("Log:");
		list_label.setText("Kits to parse (use Ctrl/Shift to select multiple files):");
		b_copy.setText("Copy to clipboard");

		/*
		 * Format Layout dir_label - dir savetemplate - opentemplate list log_label log
		 */

		FormData list_labeldata = new FormData(300, 20);
		list_labeldata.top = new FormAttachment(0, 5);
		list_labeldata.left = new FormAttachment(0, 5);
		list_label.setLayoutData(list_labeldata);

		FormData dir_layoutdata = new FormData(192, 20);
		dir_layoutdata.left = new FormAttachment(0, 5);
		dir_layoutdata.top = new FormAttachment(list_label, 5);
		t_filter1.setLayoutData(dir_layoutdata);
		t_filter1.setSize(200, t_filter1.getSize().y);

		FormData t_filter2_fd = new FormData(column2.getWidth() - 16, 20);
		t_filter2_fd.left = new FormAttachment(t_filter1, 0, SWT.RIGHT);
		t_filter2_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter2.setLayoutData(t_filter2_fd);

		FormData t_filter3_fd = new FormData(column3.getWidth() - 16, 20);
		t_filter3_fd.left = new FormAttachment(t_filter2, 0, SWT.RIGHT);
		t_filter3_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter3.setLayoutData(t_filter3_fd);

		FormData t_filter4_fd = new FormData(column4.getWidth() - 16, 20);
		t_filter4_fd.left = new FormAttachment(t_filter3, 0, SWT.RIGHT);
		t_filter4_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter4.setLayoutData(t_filter4_fd);

		FormData t_filter5_fd = new FormData(column5.getWidth() - 16, 20);
		t_filter5_fd.left = new FormAttachment(t_filter4, 0, SWT.RIGHT);
		t_filter5_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter5.setLayoutData(t_filter5_fd);

		FormData t_filter6_fd = new FormData(column6.getWidth() - 16, 20);
		t_filter6_fd.left = new FormAttachment(t_filter5, 0, SWT.RIGHT);
		t_filter6_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter6.setLayoutData(t_filter6_fd);

		FormData t_filter7_fd = new FormData(column7.getWidth() - 8, 20);
		t_filter7_fd.left = new FormAttachment(t_filter6, 0, SWT.RIGHT);
		t_filter7_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter7.setLayoutData(t_filter7_fd);

		FormData t_filter8_fd = new FormData(column8.getWidth() - 8, 20);
		t_filter8_fd.left = new FormAttachment(t_filter7, 0, SWT.RIGHT);
		t_filter8_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter8.setLayoutData(t_filter8_fd);

		FormData t_filter9_fd = new FormData(column9.getWidth() - 8, 20);
		t_filter9_fd.left = new FormAttachment(t_filter8, 0, SWT.RIGHT);
		t_filter9_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter9.setLayoutData(t_filter9_fd);

		FormData t_filter10_fd = new FormData(column10.getWidth() - 8, 20);
		t_filter10_fd.left = new FormAttachment(t_filter9, 0, SWT.RIGHT);
		t_filter10_fd.top = new FormAttachment(t_filter1, 0, SWT.TOP);
		t_filter10.setLayoutData(t_filter10_fd);

		FormData btn_go_layoutdata = new FormData(120, 25);
		btn_go_layoutdata.top = new FormAttachment(0, 5);
		btn_go_layoutdata.right = new FormAttachment(100, -5);
		b_copy.setLayoutData(btn_go_layoutdata);

		FormData btn_opendir_layoutdata = new FormData(50, 25);
		btn_opendir_layoutdata.top = new FormAttachment(0, 5);
		btn_opendir_layoutdata.right = new FormAttachment(b_copy, -5, SWT.LEFT);
		l_filterno.setLayoutData(btn_opendir_layoutdata);

		FormData listlayoutdata = new FormData(600, -1);
		listlayoutdata.bottom = new FormAttachment(separator_h, -5, SWT.TOP);
		listlayoutdata.top = new FormAttachment(t_filter1, 5, SWT.BOTTOM);
		listlayoutdata.left = new FormAttachment(comp, 5, SWT.LEFT);
		listlayoutdata.right = new FormAttachment(100, -5);
		tree.setLayoutData(listlayoutdata);

		FormData log_labeldata = new FormData(300, 20);
		log_labeldata.top = new FormAttachment(icon, 0, SWT.BOTTOM);
		log_labeldata.left = new FormAttachment(0, 5);
		log_label.setLayoutData(log_labeldata);

		FormData icon_fd = new FormData(150 * 9, 45);
		icon_fd.left = new FormAttachment(0, 200);
		icon_fd.top = new FormAttachment(separator_h, 5, SWT.BOTTOM);
		icon.setLayoutData(icon_fd);

		FormData loglayoutdata = new FormData(SWT.DEFAULT, 150);
		loglayoutdata.left = new FormAttachment(0, 5);
		loglayoutdata.right = new FormAttachment(100, -5);
		loglayoutdata.bottom = new FormAttachment(100, -5);
		loglayoutdata.top = new FormAttachment(log_label, 5, SWT.BOTTOM);
		log.setLayoutData(loglayoutdata);

		final FormData separatorlayoutdata = new FormData(-1, -1);
		separatorlayoutdata.bottom = new FormAttachment(50, 5);
		separatorlayoutdata.left = new FormAttachment(0, 0);
		separatorlayoutdata.right = new FormAttachment(100, 0);
		separatorlayoutdata.height = 3;
		separator_h.setLayoutData(separatorlayoutdata);
		separator_h.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				Rectangle sashRect = separator_h.getBounds();
				Rectangle shellRect = comp.getClientArea();
				int top = shellRect.height - sashRect.height - 100;
				e.y = Math.max(Math.min(e.y, top), 200);
				if (e.y != sashRect.y) {
					separatorlayoutdata.bottom = new FormAttachment(0, e.y);
				}
				comp.layout(true);
			}
		});

		b_copy.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				TreeItem[] selection = tree.getSelection();
				String copy = "";
				copy += "{{Version|" + Main.PR_version + "}}";
				copy += "\n{{safesubst:Weapon Table|Titel=Faction";
				for (int i = 0; i < selection.length; i++) {
					Kit kit = (Kit) Main.bf2objectDB.getByCode(selection[i].getText());
					copy += "\n|Titel" + String.format("%02d", i + 1) + " = {{Faction|" + kit.getCode().split("_")[0] + "}}";
					GenericFireArm[] weapons = kit.getWeaponsSorted();

					int j = 0;
					for (GenericFireArm w : weapons) {
						j++;
						String wname = "";
						String icon_name = "";
						if (w != null && w.getIcon() != null) {
							icon_name = w.getIcon().substring(w.getIcon().lastIndexOf("/") + 1, w.getIcon().length() - 4);
						}
						if (w == null)
							wname = "";
						else
							wname = w.getCode();
						copy += "\n|s" + (j) + String.format("%02d", i + 1) + "= " + wname + "|i" + (j) + String.format("%02d", i + 1) + "= " + icon_name
								+ "|v" + (j) + String.format("%02d", i + 1); // |s101=xxx|v101=|s1a01=
						if (Main.bf2objectDB.getByCode(selection[i].getText() + "_alt") != null) {
							Kit kitalt = (Kit) Main.bf2objectDB.getByCode(selection[i].getText() + "_alt");
							GenericFireArm[] weapons_alt = kitalt.getWeaponsSorted();
							GenericFireArm w_alt = null;
							System.out.println(kitalt.getCode());
							System.out.println("Weapon name " + wname);
							System.out.println(Arrays.toString(weapons_alt));
							System.out.println(weapons_alt.length);
							System.out.println(j);
							if (j >= weapons_alt.length) {
								System.out.println("Array too short");
								continue;
							}
							if (weapons_alt[j-1] == null)
								System.out.println("Alt weapon is null");
							
							if (j < weapons_alt.length && weapons_alt[j-1] != null && !wname.equals(weapons_alt[j-1].getCode())) {
								if (Main.bf2objectDB.getByCode(kitalt.getWeaponsSorted()[j-1].getCode()) != null) {
									w_alt = (GenericFireArm) Main.bf2objectDB.getByCode(kitalt.getWeaponsSorted()[j-1].getCode());
									System.out.println("Alt " + w_alt.getCode());
									copy += " |s" + j + "a" + String.format("%02d", i + 1) + "= " + w_alt.getCode();
									copy += "|v" + j + "a" + String.format("%02d", i + 1); // |v1a01=
									String icon_alt = "";
									if (w_alt != null && w_alt.getIcon() != null) {
										icon_alt = w_alt.getIcon().substring(w_alt.getIcon().lastIndexOf("/") + 1, w_alt.getIcon().length() - 4);
										copy += "|i" + j + "a" + String.format("%02d", i + 1) + "= " + icon_alt;
									}
								}
							}
						}
					}
				}
				copy += "\n}}";
				copy += "\n{{Navbox}}";
				Clipboard cb = new Clipboard(Display.getDefault());
				TextTransfer tt = TextTransfer.getInstance();
				cb.setContents(new Object[] { copy }, new TextTransfer[] { tt });
				cb.dispose();
			}
		});

		comp.addListener(SWT.Resize, new Listener() {

			@Override
			public void handleEvent(Event event) {
				icon.layout(true);
				icon.update();

			}

		});
		icon.addListener(SWT.Paint, new Listener() {
			@Override
			public void handleEvent(Event e) {
				if (tree.getSelection().length > 0)
					redrawIcon(tree, icon, tree.getSelection());
			}
		});

		tree.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				if (tree.getSelection().length > 0)
					redrawIcon(tree, icon, tree.getSelection());
			}
		});
		/*
		 * tree.addListener(SWT.MouseHover, new Listener() { public void handleEvent(Event event) { Point pt = new Point(event.x, event.y); TreeItem item =
		 * tree.getItem(pt); if (item != null) { for (int col = 1; col < tree.getColumnCount(); col++) { Rectangle rect = item.getBounds(col); if
		 * (rect.contains(pt)) { try { String name = item.getText(col); System.out.println(name); String filename = "db\\atlas\\Menu\\HUD\\Texture\\" +
		 * chooser.bF2ObjectDB.getByName(name).getIcon().substring(0, chooser.bF2ObjectDB.getByName(name).getIcon().lastIndexOf(".")) + ".png"; Image img = new
		 * Image(chooser.display, filename); // Image img = new Image(chooser.display, SWTUtils.convertToSWT(image_dds)); GC gc = new GC(icon, 0);
		 * gc.setBackground(icon.getBackground()); gc.fillRectangle(0, 0, icon.getClientArea().width, icon.getClientArea().height); gc.drawImage(img, 0, 0);
		 * gc.dispose(); } catch (SWTException | NullPointerException ex) { GC gc = new GC(icon, 0); gc.setBackground(icon.getBackground()); gc.fillRectangle(0,
		 * 0, icon.getClientArea().width, icon.getClientArea().height); gc.setForeground(chooser.display.getSystemColor(SWT.COLOR_RED));
		 * gc.setAntialias(SWT.ON); gc.drawLine(0, 0, icon.getClientArea().width, icon.getClientArea().height); gc.drawLine(icon.getClientArea().width, 0, 0,
		 * icon.getClientArea().height); gc.setForeground(chooser.display.getSystemColor(SWT.COLOR_BLACK)); gc.drawText("No image found", 0, 0, true);
		 * gc.dispose(); } } } } } });
		 */

		ModifyListener treefilter = new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				if (timer != null) {
					timer.cancel();
				}

				timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								final String[] filters = new String[] { t_filter1.getText(), t_filter2.getText(), t_filter3.getText(), t_filter4.getText(),
										t_filter5.getText(), t_filter6.getText(), t_filter7.getText(), t_filter8.getText(), t_filter9.getText(),
										t_filter10.getText() };
								refreshTree(tree, filters);
								l_filterno.setText(String.valueOf(tree.getItemCount()));
							}
						});
						timer.cancel();
					}
				}, 500);
			}

		};

		t_filter1.addModifyListener(treefilter);
		t_filter2.addModifyListener(treefilter);
		t_filter3.addModifyListener(treefilter);
		t_filter4.addModifyListener(treefilter);
		t_filter5.addModifyListener(treefilter);
		t_filter6.addModifyListener(treefilter);
		t_filter7.addModifyListener(treefilter);
		t_filter8.addModifyListener(treefilter);
		t_filter9.addModifyListener(treefilter);
		t_filter10.addModifyListener(treefilter);
		final String[] filters = new String[] { t_filter1.getText(), t_filter2.getText(), t_filter3.getText(), t_filter4.getText(), t_filter5.getText(),
				t_filter6.getText(), t_filter7.getText(), t_filter8.getText(), t_filter9.getText(), t_filter10.getText() };
		refreshTree(tree, filters);
		comp.layout();
		comp.getShell().setMinimumSize(tree.getSize().x, SWT.DEFAULT);
		l_filterno.setText(String.valueOf(tree.getItemCount()));
	}

	public static void redrawIcon(Tree tree, Canvas icon, TreeItem[] selection) {
		for (int col = 0; col < tree.getColumnCount() - 1; col++) {
			try {
				String name = selection[selection.length - 1].getText(col + 1);
				String filename = "db\\atlas\\Menu\\HUD\\Texture\\"
						+ ((GenericFireArm) Main.bf2objectDB.getByCode(name)).getIcon().substring(0,
								((GenericFireArm) Main.bf2objectDB.getByCode(name)).getIcon().lastIndexOf(".")) + ".png";
				Image img = new Image(Main.display, filename);
				// Image img = new Image(chooser.display,
				// SWTUtils.convertToSWT(image_dds));
				GC gc = new GC(icon, 0);
				gc.setBackground(icon.getBackground());
				gc.fillRectangle(col * 150, 0, 150, icon.getClientArea().height);
				gc.drawImage(img, col * 150, 0);
				gc.dispose();
			} catch (SWTException ex) {
				GC gc = new GC(icon, 0);
				gc.setBackground(icon.getBackground());
				gc.fillRectangle(col * 150, 0, 150, icon.getClientArea().height);
				gc.setForeground(Main.display.getSystemColor(SWT.COLOR_RED));
				gc.setAntialias(SWT.ON);
				gc.drawLine(col * 150, 0, (col + 1) * 150, icon.getClientArea().height);
				gc.drawLine((col + 1) * 150, 0, col * 150, icon.getClientArea().height);
				gc.setForeground(Main.display.getSystemColor(SWT.COLOR_BLACK));
				gc.drawText("No image found", col * 150, 0, true);

				gc.dispose();
			} catch (NullPointerException ex) {
				Image img = new Image(Main.display, "db\\atlas\\Menu\\HUD\\Texture\\Ingame\\Weapons\\Icons\\Hud\\Selection\\blank.png");
				// Image img = new Image(chooser.display,
				// SWTUtils.convertToSWT(image_dds));
				GC gc = new GC(icon, 0);
				gc.setBackground(icon.getBackground());
				gc.fillRectangle(col * 150, 0, 150 - 1, icon.getClientArea().height - 1);
				gc.drawImage(img, col * 150, 0);
				gc.drawRectangle(col * 150, 0, 150 - 1, icon.getClientArea().height - 1);
				gc.dispose();
			}
		}
	}

	public static void refreshTree(final Tree tree, final String[] filters) {
		try {
			tree.removeAll();
			// "(?!_alt_sp)(?!_sp)(?!_para)(?!_alt_para)(?!_alt_ziptie)(?!_ziptie)(?!_alt)"
			final Pattern[] patterns = new Pattern[filters.length];
			// patterns[0] = Pattern.compile(filters[0]);
			patterns[0] = Pattern.compile(filters[0] + "(?!_alt_sp)(?!_sp)(?!_para)(?!_alt_para)(?!_alt_ziptie)(?!_ziptie)(?!_alt)(?!_night)");
			System.out.println(patterns[0].toString());
			for (int i = 0; i < filters.length - 1; i++) {
				// filters[i] = filters[i].replace("*", "\\w*");
				// filters[i] = filters[i].replace("?", "\\w");
				patterns[i + 1] = Pattern.compile(filters[i + 1]);
			}
			new Thread(new Runnable() {
				@Override
				public void run() {
					Display.getDefault().syncExec(new Runnable() {
						@Override
						public void run() {

							for (int i = 0; i < kitdb.size(); i++) {
								GenericFireArm[] kitweapons = kitdb.get(i).getWeaponsSorted();
								Matcher[] matchers = new Matcher[filters.length];
								matchers[0] = patterns[0].matcher(kitdb.get(i).getCode());
								for (int j = 0; j < filters.length - 1; j++) {
									matchers[j + 1] = patterns[j + 1].matcher(String.valueOf(kitweapons[j]));
								}
								boolean test = true;
								for (int k = 0; k < matchers.length; k++)
									if (!matchers[k].find()) {
										test = false;
										break;
									}
								if (test) {
									TreeItem item = new TreeItem(tree, SWT.NONE);
									Kit k = (Kit) kitdb.get(i);

									GenericFireArm[] weapons = k.getWeaponsSorted();
									String[] wnames = new String[10];
									wnames[0] = k.getCode();
									for (int j = 1; j < weapons.length; j++)
										try {
											wnames[j + 1] = weapons[j].getCode();
										} catch (NullPointerException e) {
											wnames[j + 1] = "";
										}
									item.setText(wnames);

									/*
									Image[] icons = new Image[10];
									for (int j = 1; j < dbw.size(); j++) {
										try {
											String filename = "db\\atlas\\Menu\\HUD\\Texture\\"
													+ chooser.weaponDB.getByName(dbw.get(j)).getIcon()
															.substring(0, chooser.bF2ObjectDB.getByName(dbw.get(j)).getIcon().lastIndexOf(".")) + ".png";
											Image img = new Image(chooser.display, filename);
											icons[j] = img;
										} catch (SWTException | NullPointerException ex) {
											Image img = null;
											GC gc = new GC(img, 0);
											gc.setBackground(item.getBackground());
											gc.fillRectangle(0, 0, 150, 45);
											gc.setForeground(chooser.display.getSystemColor(SWT.COLOR_RED));
											gc.setAntialias(SWT.ON);
											gc.drawLine(0, 0, 150, 45);
											gc.drawLine(150, 0, 0, 45);
											gc.setForeground(chooser.display.getSystemColor(SWT.COLOR_BLACK));
											gc.drawText("No image found", 0, 0, true);
											gc.dispose();
											icons[j] = img;
										}
									}
									item.setImage(icons);
									*/
								}

							}
						}
					});
				}

			}).start();

		} catch (PatternSyntaxException e) {
		}
	}
}
