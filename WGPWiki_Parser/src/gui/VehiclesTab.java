package gui;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import common.Utils;
import common.WinRegistry;
import db.BF2Object;
import db.Kit;
import db.PlayerControlObject;
import db.Vehicle;

public class VehiclesTab {

	private static Text log;
	private static List<BF2Object> filelist = new ArrayList<BF2Object>();
	private static List<BF2Object> vehicles = new ArrayList<BF2Object>();

	public static void initUI(Composite comp) {
		comp.setLayout(new FormLayout());
		Button b_browse = new Button(comp, 0);
		final Combo c_vehicle = new Combo(comp, 0);
		log = new Text(comp, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.READ_ONLY | SWT.WRAP);
		Composite comp_sheet = new Composite(comp, SWT.BORDER);

		FormData b_browse_fd = new FormData(60, 25);
		b_browse_fd.bottom = new FormAttachment(100, -5);
		b_browse_fd.left = new FormAttachment(0, 5);
		b_browse.setLayoutData(b_browse_fd);
		b_browse.setText("Browse...");
		b_browse.setToolTipText("Vehicles folder");

		FormData c_vehicle_ld = new FormData(100, SWT.DEFAULT);
		c_vehicle_ld.top = new FormAttachment(0, 5);
		c_vehicle_ld.left = new FormAttachment(0, 5);
		c_vehicle.setLayoutData(c_vehicle_ld);
		c_vehicle.setText("Browse...");

		FormData log_ld = new FormData(200, SWT.DEFAULT);
		log_ld.top = new FormAttachment(c_vehicle, 5);
		log_ld.bottom = new FormAttachment(b_browse, -5, SWT.TOP);
		log_ld.left = new FormAttachment(0, 5);
		log.setLayoutData(log_ld);

		FormData comp_sheet_ld = new FormData();
		comp_sheet_ld.top = new FormAttachment(0, 5);
		comp_sheet_ld.bottom = new FormAttachment(100, -5);
		comp_sheet_ld.left = new FormAttachment(log, 5, SWT.RIGHT);
		comp_sheet_ld.right = new FormAttachment(100, -5);
		comp_sheet.setLayoutData(comp_sheet_ld);

		// Bulletin contents
		final Tree tree = new Tree(comp_sheet, SWT.MULTI | SWT.BORDER);
		/*
		 * Label l_name = new Label(comp_sheet, 0); Text t_name = new
		 * Text(comp_sheet, SWT.BORDER | SWT.WRAP | SWT.MULTI); Label l_code =
		 * new Label(comp_sheet, 0); final Text t_code = new Text(comp_sheet,
		 * SWT.BORDER | SWT.WRAP | SWT.MULTI); Label l_crew = new
		 * Label(comp_sheet, 0); final Text t_crew = new Text(comp_sheet,
		 * SWT.BORDER | SWT.WRAP | SWT.MULTI); Label l_passengers = new
		 * Label(comp_sheet, 0); final Text t_passengers = new Text(comp_sheet,
		 * SWT.BORDER | SWT.WRAP | SWT.MULTI); Label l_hitpoints = new
		 * Label(comp_sheet, 0); final Text t_hitpoints = new Text(comp_sheet,
		 * SWT.BORDER | SWT.WRAP | SWT.MULTI); Label l_hitpoints_crit = new
		 * Label(comp_sheet, 0); final Text t_hitpoints_crit = new
		 * Text(comp_sheet, SWT.BORDER | SWT.WRAP | SWT.MULTI); Label l_spinup =
		 * new Label(comp_sheet, 0); final Text t_spinup = new Text(comp_sheet,
		 * SWT.BORDER | SWT.WRAP | SWT.MULTI); Label l_extras = new
		 * Label(comp_sheet, 0); final Text t_extras = new Text(comp_sheet,
		 * SWT.BORDER | SWT.WRAP | SWT.MULTI); Label l_speed = new
		 * Label(comp_sheet, 0); final Text t_speed = new Text(comp_sheet,
		 * SWT.BORDER | SWT.WRAP | SWT.MULTI); Label l_tickets = new
		 * Label(comp_sheet, 0); final Text t_tickets = new Text(comp_sheet,
		 * SWT.BORDER | SWT.WRAP | SWT.MULTI);
		 * 
		 * // Label vsep1 = new Label(comp_sheet, SWT.SEPARATOR | SWT.VERTICAL);
		 * Label l_maps = new Label(comp_sheet, 0); Text t_maps = new
		 * Text(comp_sheet, SWT.BORDER | SWT.MULTI);
		 */
		final Text t_wikiout = new Text(comp_sheet, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.READ_ONLY | SWT.WRAP);

		// Bulletin grid layout

		comp_sheet.setLayout(new FormLayout());
		FormData tree_ld = new FormData();
		tree_ld.top = new FormAttachment(0, 5);
		tree_ld.bottom = new FormAttachment(50, -5);
		tree_ld.left = new FormAttachment(0, 5);
		tree_ld.right = new FormAttachment(100, -5);
		tree.setLayoutData(tree_ld);

		/*
		 * l_name.setText("Name:"); l_name.setLayoutData(new GridData(SWT.RIGHT,
		 * SWT.CENTER, false, false)); t_name.setLayoutData(new
		 * GridData(SWT.FILL, SWT.CENTER, true, false));
		 * 
		 * l_crew.setText("Crew:"); l_crew.setLayoutData(new GridData(SWT.RIGHT,
		 * SWT.CENTER, false, false)); t_crew.setLayoutData(new
		 * GridData(SWT.FILL, SWT.CENTER, true, false));
		 * 
		 * l_passengers.setText("Passengers:"); l_passengers.setLayoutData(new
		 * GridData(SWT.RIGHT, SWT.CENTER, false, false));
		 * t_passengers.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
		 * false));
		 * 
		 * l_hitpoints.setText("Hitpoints:"); l_hitpoints.setLayoutData(new
		 * GridData(SWT.RIGHT, SWT.CENTER, false, false));
		 * t_hitpoints.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
		 * false));
		 * 
		 * l_hitpoints_crit.setText("Critical Damage:");
		 * l_hitpoints_crit.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER,
		 * false, false)); t_hitpoints_crit.setLayoutData(new GridData(SWT.FILL,
		 * SWT.CENTER, true, false));
		 * 
		 * l_code.setText("Codename:"); l_code.setLayoutData(new
		 * GridData(SWT.RIGHT, SWT.CENTER, false, false));
		 * t_code.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
		 * false));
		 * 
		 * l_spinup.setText("Spinup time:"); l_spinup.setLayoutData(new
		 * GridData(SWT.RIGHT, SWT.CENTER, false, false));
		 * t_spinup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
		 * false));
		 * 
		 * l_extras.setText("Extras:"); l_extras.setLayoutData(new
		 * GridData(SWT.RIGHT, SWT.CENTER, false, false));
		 * t_extras.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
		 * false));
		 * 
		 * l_speed.setText("Speed:"); l_speed.setLayoutData(new
		 * GridData(SWT.RIGHT, SWT.CENTER, false, false));
		 * t_speed.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
		 * false));
		 * 
		 * l_tickets.setText("Ticket cost:"); l_tickets.setLayoutData(new
		 * GridData(SWT.RIGHT, SWT.CENTER, false, false));
		 * t_tickets.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true,
		 * false));
		 * 
		 * l_maps.setText("Exists on:"); l_maps.setLayoutData(new
		 * GridData(SWT.RIGHT, SWT.CENTER, false, false));
		 * t_maps.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		 */

		FormData wiki_ld = new FormData();
		wiki_ld.top = new FormAttachment(50, 5);
		wiki_ld.bottom = new FormAttachment(100, -5);
		wiki_ld.left = new FormAttachment(0, 5);
		wiki_ld.right = new FormAttachment(100, -5);
		t_wikiout.setLayoutData(wiki_ld);

		comp.layout();

		b_browse.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					Utils.openDirectorydialog("mods\\pr_edit\\objects\\vehicles\\", "Select vehicle folder",
							"Pick the folder where the extracted vehicles are stored.\nDefault is mods\\MODNAME\\objects\\vehicles\\");
				} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e1) {
					e1.printStackTrace();
				}
			}
		});

		c_vehicle.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					if (filelist.get(c_vehicle.getSelectionIndex()) == null)
						System.out.println(c_vehicle.getText());
					final BF2Object parent = filelist.get(c_vehicle.getSelectionIndex());
					/*
					 * t_code.setText(vehicle.getCode());
					 * t_passengers.setText(String
					 * .valueOf(vehicle.getPassengers()));
					 * t_crew.setText(String.valueOf(vehicle.getCrew()));
					 * t_hitpoints
					 * .setText(String.valueOf(vehicle.getHitpoints()));
					 * t_hitpoints_crit
					 * .setText(String.valueOf(vehicle.getHitpoints_crit()));
					 * t_hitpoints_crit
					 * .setText(String.valueOf(vehicle.getHitpoints_crit()));
					 * t_spinup.setText(String.valueOf(vehicle.getSpinup()));
					 * t_speed.setText(String.valueOf(vehicle.getSpeed()));
					 * t_tickets.setText(String.valueOf(vehicle.getTickets()));
					 */
					// t_maps.setText(String.valueOf(vehicle.getMaps()));
					t_wikiout.setText("");
					tree.removeAll();
					t_wikiout.insert(parent.getCode() + " ");
					t_wikiout.insert(Main.lang.getValue(parent.getCode(), parent.getCode()));
					/*
					 * if (vehicle.getExtras() != null)
					 * t_wikiout.insert("\nExtras: " + vehicle.getExtras()); if
					 * (vehicle.getName() != null) t_wikiout.insert("\nName: " +
					 * vehicle.getName()); if (vehicle.getSpeed() != null)
					 * t_wikiout.insert("\nSpeed: " + vehicle.getSpeed()); if
					 * (vehicle.getType() != null) t_wikiout.insert("\nType: " +
					 * vehicle.getType()); t_wikiout.insert("\nCrew: " +
					 * String.valueOf(vehicle.getCrew()));
					 * t_wikiout.insert("\nHP: " +
					 * String.valueOf(vehicle.getHitpoints()));
					 * t_wikiout.insert("\nHP_crit: " +
					 * String.valueOf(vehicle.getHitpoints_crit())); if
					 * (vehicle.getMaps() != null) t_wikiout.insert("\nMaps: " +
					 * Joiner.on("\t").join(vehicle.getMaps()));
					 * t_wikiout.insert("\nPassengers: " +
					 * String.valueOf(vehicle.getPassengers()));
					 * t_wikiout.insert("\nSpinup: " +
					 * String.valueOf(vehicle.getSpinup()));
					 */
					class Foo {
						public void loop(BF2Object parent, int level) {
							for (int x = 0; x < parent.getSubTemplates().size(); x++) {
								int sublevel = level + 1;
								BF2Object sub = Main.bf2objectDB.getByCode(parent.getSubTemplates().get(x));
								if (sub != null) {
									System.out.println(sub.getCode() + "---" + level);
									String text = "\n" + new String(new char[level]).replace("\0", "\t") + sub.getCode() + " " + sub.getClass().getSimpleName();
									t_wikiout.insert(text);

									if (sub.getSubTemplates().size() > 0) {
										try {
											//VehicleComponent subsub = chooser.vehiclecomponentDB.getByName(sub2.getSubTemplates().get(x));
											//VehicleComponent subsub = chooser.vehiclecomponentDB.getByName(sub2.getSubTemplates().get(x));
											loop(sub, sublevel);
										} catch (IndexOutOfBoundsException e) {
											System.out.println(sub.getCode());
										}
									}
								}
							}
						}
					}

					new Foo().loop(parent, 1);
					/*					for (int x = 0; x < vehicle.getSubTemplates().size(); x++) {
						//new Foo().loop(vehicle.getSubTemplates().get(x), 0);
						// This shows ALL sub objects in the wikiout
						BF2Object sub = Main.bf2objectDB.getByCode(vehicle.getSubTemplates().get(x));
						if (sub != null) {
							t_wikiout.insert("\n\t" + sub.getCode() + " " + sub.getClass().getSimpleName());
							for (int x2 = 0; x2 < Main.bf2objectDB.getByCode(sub.getCode()).getSubTemplates().size(); x2++) {
								BF2Object sub2 = Main.bf2objectDB.getByCode(sub.getSubTemplates().get(x2));
								if (sub2 != null) {
									t_wikiout.insert("\n\t\t" + sub2.getCode() + " " + sub2.getClass().getSimpleName());
									for (int x3 = 0; x3 < Main.bf2objectDB.getByCode(sub2.getCode()).getSubTemplates().size(); x3++) {
										BF2Object sub3 = Main.bf2objectDB.getByCode(sub2.getSubTemplates().get(x3));
										if (sub3 != null) {
											t_wikiout.insert("\n\t\t\t" + sub3.getCode() + " " + sub3.getClass().getSimpleName());
										}
									}
								}
							}
						}

					}
					*/
					// t_wikiout.insert("\nTickets: " + String.valueOf(vehicle.getTickets()));

					TreeItem treeItem0 = new TreeItem(tree, 0);
					treeItem0.setText("Level 0 Item " + parent.getCode());
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				}

			}
		});
		List<BF2Object> dbase = Main.bf2objectDB.getDB();

		// Verify that the detected PCO is NOT a child PCO by iterating over all
		// AddTemplates. If the v-name is found in an addtemplate, it will be
		// skipped.
		// TODO: Has to take into account the .tweak changes yet. Also, this
		// somehow seems like a dirty way of doing it.
		// Also only can check only one level of Addtemplates. E.g. Cupolas that
		// are addtemplated to turrets that are addtemplated to the main vehicle
		// don't get recognized.
		for (BF2Object v : dbase) {
			boolean exist = false;
			for (int i = 0; i < dbase.size(); i++) {
				if (dbase.get(i).getSubTemplates() != null)
					for (String template : dbase.get(i).getSubTemplates())
						if (v.getCode().equalsIgnoreCase(template)) {
							exist = true;
							break;
						}
			}
			if (exist == false) {
				// System.out.println(v.getClass().getName());
				if (v.getClass().equals(PlayerControlObject.class)) {
					filelist.add(v);
					vehicles.add(v);
				}
			}
		}
		Collections.sort(filelist, new Comparator<BF2Object>() { // Sort
					// alphabetically
					@Override
					public int compare(BF2Object v1, BF2Object v2) {
						String name1 = v1.getCode();
						String name2 = v2.getCode();
						return name1.compareTo(name2);
					}

				});
		String x = "";
		try {
			FileWriter f_debug = new FileWriter(new File("debug_VehicleTab.log"));
			for (BF2Object v : filelist) {
				c_vehicle.add(Main.lang.getValue(v.getCode(), v.getCode()));
				x += "| " + v.getCode() + " = " + Main.lang.getValue(v.getCode()) + "\n";
				// System.out.println(vname);
			}
			for (BF2Object v : filelist)
				f_debug.write(v.getCode() + " = " + Main.lang.getValue(v.getCode()) + "\n");
			f_debug.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(x);
		StringSelection stringSelection = new StringSelection(x);
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		clpbrd.setContents(stringSelection, null);

	}
}
