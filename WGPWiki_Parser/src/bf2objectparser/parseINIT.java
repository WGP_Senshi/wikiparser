package bf2objectparser;

import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.FileVisitResult.SKIP_SUBTREE;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.List;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTyping;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import db.BF2Object;
import db.BF2ObjectDB;
import db.Engine;
import db.GenericFireArm;
import db.Kit;
import db.LandingGear;
import db.PlayerControlObject;
import db.Projectile;
import db.RotationalBundle;
import gui.Main;

public class parseINIT extends SimpleFileVisitor<Path> {
	private File output;
	private static ObjectOutputStream out;
	protected static ArrayList<BF2Object> bf2objects;
	static int no = 1; // Dateizaehler
	static common.PBar bar;
	final static int READ_AHEAD_LIMIT = 500; // The maximum length of a "OT.create line. I'm pretty sure 500 characters are enough.

	protected static void log(Object aObject) {
		try {
			System.out.println(String.valueOf(aObject));
		} catch (IllegalArgumentException e) {
		}
	}

	parseINIT(String name) {
		try {
			this.output = new File(name);
			parseINIT.bf2objects = new ArrayList<BF2Object>();
			FileWriter fw_out = new FileWriter(output);
			// XStream xstream = new XStream(new StaxDriver()); // Would allow
			// better stream-based processing (memory), BUT creates horrible
			// single-line output.
			// Opting for the fancier Dom for now which works just fine.
			XStream xstream = new XStream(new DomDriver());
			xstream.alias("bf2object", BF2Object.class);
			xstream.alias("genericfirearm", GenericFireArm.class);
			xstream.alias("engine", Engine.class);
			xstream.alias("genericfirearm", GenericFireArm.class);
			xstream.alias("kit", Kit.class);
			xstream.alias("landinggear", LandingGear.class);
			xstream.alias("playercontrolobject", PlayerControlObject.class);
			xstream.alias("rotationalbundle", RotationalBundle.class);
			xstream.alias("projectile", Projectile.class);

			parseINIT.out = xstream.createObjectOutputStream(fw_out, "bf2objects");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void parseSource(File dir, List log) throws IOException {
		bar = new common.PBar(SWT.INDETERMINATE);
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		log.add(dateFormat.format(new Date()) + " : Processing BF2 objects. This might take a while.");
		log.getParent().update();

		// int count = Utils.countFilesInDirectory(dir);
		// bar.setMaximum(count);
		// bar.setMinimum(0);
		// bar.setSelection(0);
		bar.setText("Parsing files, please be patient.");
		parseINIT pW = new parseINIT("db/bf2objects.xml");
		// System.out.println(dir);
		Files.walkFileTree(dir.toPath(), pW);
		bar.setText("Writing " + bf2objects.size() + " database objects...");
		bar.setMaximum(bf2objects.size());
		for (int i = 0; i < bf2objects.size(); i++) {
			bar.setSelection(i);
			out.writeObject(bf2objects.get(i));
		}

		out.close();
		final ObjectMapper mapper = new ObjectMapper();
		File f_out_json = new File("db/bf2objects.json");
		BufferedWriter bw_json = new BufferedWriter(new FileWriter(f_out_json));
		//mapper.setSerializationInclusion(Include.NON_EMPTY).enableDefaultTyping(DefaultTyping.JAVA_LANG_OBJECT).writerWithDefaultPrettyPrinter()
		//		.writeValue(bw_json, bf2objects);
		/*mapper.writerFor(new TypeReference<java.util.List
		        <BF2Object>>() {
		        }).withDefaultPrettyPrinter().writeValue(bw_json, bf2objects);
		*/
		bar.setText("Reading " + bf2objects.size() + " database objects...");
		Main.bf2objectDB = BF2ObjectDB.readDB();
		log.add(dateFormat.format(new Date()) + " : BF2Object database has been updated!");
		bar.dispose();
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
		if (dir.getFileName().toString().equalsIgnoreCase("hud"))
			return SKIP_SUBTREE;
		if (dir.getFileName().toString().equalsIgnoreCase("tr_scripts"))
			return SKIP_SUBTREE;
		return CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		processFile(file, null, null);
		return CONTINUE;
	}

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
		return CONTINUE;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) {
		System.err.println(exc);
		return CONTINUE;
	}

	protected static int getBF2ObjectIndex(BF2Object obj) {
		if (!bf2objects.isEmpty() && obj != null) {
			// System.out.println("BFObjectssize " + bf2objects.size());
			for (int i = 0; i < bf2objects.size(); i++) {
				// System.out.println(i);
				if (bf2objects.get(i).getCode().equalsIgnoreCase(obj.getCode())) {
					return i;
				}
			}
		}
		return -999;
	}

	protected static BF2Object getBF2Object(String code) {
		if (!bf2objects.isEmpty() && code != null) {
			for (int i = 0; i < bf2objects.size(); i++) {
				if (bf2objects.get(i).getCode().equalsIgnoreCase(code)) {
					return bf2objects.get(i);
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * @param file
	 * @param vargs
	 * @param currentobj
	 *            -- Usually !null during "include" calls. The specified object will immediately be opened instead of waiting for a "create"
	 * @param modus
	 * @throws IOException
	 */
	protected static void processFile(Path file, String[] vargs, BF2Object currentobj) throws IOException {
		no++;
		bar.setSelection(no);
		// Verify that the file is a correct file suffix
		PathMatcher matchercon = FileSystems.getDefault().getPathMatcher("glob:*.{con}");
		Path name = file.getFileName();
		if (name != null && (matchercon.matches(name) || currentobj != null)) {
			// System.out.println(file);
			BufferedReader scanner = new BufferedReader(new FileReader(file.toFile()));
			try {
				String[] tokens = { "x" };
				while (tokens != null) {
					tokens = prepLine(scanner);
					currentobj = processLine(scanner, tokens, vargs, file, currentobj);
				}
			} finally {
				scanner.close();
				if (currentobj != null) {
					int index = getBF2ObjectIndex(currentobj);
					// if (currentobj.getClass() == Kit.class)
					// System.out.println(Arrays.toString(currentobj.getSubTemplates().toArray()));
					if (index != -999)
						bf2objects.set(index, currentobj);
					else
						bf2objects.add(currentobj);
				}
			}
		}

	};

	/**
	 * Prepares a line for parsing: Removes leading whitespaces, splits line into tokens separated by whitespaces and returns tokens.
	 */
	protected static String[] prepLine(BufferedReader scanner) throws IOException {
		String line = scanner.readLine();
		if (line == null) // Eject if end of file
			return null;
		line = line.trim();
		line = line.toLowerCase();
		// String[] tokens = line.split("\\s"); // split on all spaces
		/*
		 * This is some voodoo regex to split on spaces, except if there's double quotes going around, e.g. strings with spaces.
		 * The non-escaped regex is \s(?!(\S+\s+)*\S+\\")|\s(?=") . Only marginally easier to read...
		 */
		String[] tokens = line.split("\\s(?!(\\S+\\s+)*\\S+\\\\\")|\\s(?=\\\")");
		for (int x = 0; x < tokens.length; x++)
			tokens[x] = tokens[x].replace("\"", "");
		// System.out.println(line);
		// System.out.println(Arrays.toString(tokens));
		return tokens;
	}

	/**
	 * Processes a single .con/ .tweak line
	 * 
	 * @throws IOException
	 */
	protected static BF2Object processLine(BufferedReader scanner, String[] tokens, String[] vargs, Path path, BF2Object currentobj) throws IOException {

		if (tokens == null)
			return currentobj;
		tokens = varg_check(tokens, vargs);
		if (tokens[0].equalsIgnoreCase("beginrem"))
			processremblock(scanner, tokens, path);
		else if (tokens[0].equalsIgnoreCase("endif"))
			return currentobj;
		else if (tokens[0].equalsIgnoreCase("if") || tokens[0].equalsIgnoreCase("elseif"))
			processIf(scanner, tokens, vargs, path, currentobj);

		else if (tokens[0].equalsIgnoreCase("include") || tokens[0].equalsIgnoreCase("run")) {
			if (tokens.length > 2)
				processInclude(tokens[1], Arrays.copyOfRange(tokens, 2, tokens.length), path, currentobj);
			else
				processInclude(tokens[1], null, path, currentobj);
		}

		// Detect creation of new objects and trigger proper parsers.
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.create")) {
			if (currentobj != null) {
				int index = getBF2ObjectIndex(currentobj);
				if (index != -999)
					bf2objects.set(index, currentobj);
				else
					bf2objects.add(currentobj);
			}
			if (tokens.length < 3) {
				System.err.println("ERROR: File " + path + " has a malformed create call. Look for: " + Arrays.toString(tokens));
				return null;
			}
			// tokens[1] == classname
			// tokens[2] == object codename
			if (tokens[1].equalsIgnoreCase("PlayerControlObject")) {
				currentobj = new PlayerControlObject(tokens[2]);
			} else if (tokens[1].equalsIgnoreCase("GenericProjectile")) {
				currentobj = new Projectile(tokens[2]);
			} else if (tokens[1].equalsIgnoreCase("GenericFireArm")) {
				currentobj = new GenericFireArm(tokens[2]);
			} else if (tokens[1].equalsIgnoreCase("RotationalBundle")) {
				currentobj = new RotationalBundle(tokens[2]);
			} else if (tokens[1].equalsIgnoreCase("Engine")) {
				currentobj = new Engine(tokens[2]);
			} else if (tokens[1].equalsIgnoreCase("LandingGear")) {
				currentobj = new LandingGear(tokens[2]);
			} else if (tokens[1].equalsIgnoreCase("Kit")) {
				currentobj = new Kit(tokens[2]);
			}
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.activeSafe")) {
			if (currentobj != null) {
				int index = getBF2ObjectIndex(currentobj);
				if (index != -999)
					bf2objects.set(index, currentobj);
				else
					bf2objects.add(currentobj);
			}
			if (tokens.length < 3) {
				System.err.println("ERROR: File " + path + " has a malformed activeSafe call. Look for: " + Arrays.toString(tokens));
				return null;
			}

			currentobj = getBF2Object(tokens[2]);
			// if (currentobj == null)
			// System.out.println("WARNING: There was an attempt to activesafe " + tokens[2] + " BEFORE it was being created in file " + path); // This will pop
			// up often unless all objects are parsed, so we skip it for now. TODO
			return currentobj;
		} else if (currentobj != null) {
			if (currentobj.getClass() == db.GenericFireArm.class)
				currentobj = bf2objectparser.parseGenericFireArm.parse(tokens, (GenericFireArm) currentobj);
			else if (currentobj.getClass() == db.Kit.class)
				currentobj = bf2objectparser.parseKit.parse(tokens, (Kit) currentobj);
			else if (currentobj.getClass() == db.Projectile.class) {
				System.out.println(currentobj.getCode());
				currentobj = bf2objectparser.parseProjectile.parse(tokens, (Projectile) currentobj, vargs);
			}
			else if (currentobj.getClass() == db.Engine.class)
				currentobj = bf2objectparser.parseEngine.parse(tokens, (Engine) currentobj);
			else if (currentobj.getClass() == db.RotationalBundle.class)
				currentobj = bf2objectparser.parseRotBundle.parse(tokens, (RotationalBundle) currentobj);
			else if (currentobj.getClass() == db.PlayerControlObject.class)
				currentobj = bf2objectparser.parsePCO.parse(tokens, (PlayerControlObject) currentobj);
		}
		return currentobj;

	}

	protected static void processremblock(BufferedReader scanner, String[] tokens, Path path) throws IOException {
		while (tokens != null) {
			tokens = prepLine(scanner);
			if (tokens == null) {
				log("WARNING: A comment block in " + path.toString()
						+ " is being started with 'beginrem' but not being closed with 'endrem' until the end of file has been reached!");
				break;
			}
			if (tokens[0].equalsIgnoreCase("endrem")) {
				break;
			}
		}

	}

	/**
	 * Figures out if any of the tokens include a v_arg and replaces it with the content of currently active v_args. Necessary when v_args are handed over in
	 * two sequential include calls include x.con 1 2 3 -> x.con -> include y.con v_arg2 v_arg3 -> varg_check replaces v_arg2 and v_arg3
	 * 
	 * @param tokens
	 * @param varg
	 * @return
	 */
	private static String[] varg_check(String[] tokens, String[] varg) {
		if (varg == null || !tokens[0].equalsIgnoreCase("include"))
			return tokens;
		for (int x = 2; x < tokens.length; x++) {
			for (int y = 0; y < varg.length; y++) {
				if (tokens[x].equalsIgnoreCase("v_arg" + String.valueOf(y + 1)))
					tokens[x] = varg[y];
			}
		}
		return tokens;
	}

	protected static BF2Object processIf(BufferedReader scanner, String[] tokens, String[] vargs, Path path, BF2Object currentobj) throws IOException {
		ArrayList<Boolean> subifs = new ArrayList<Boolean>();

		while (tokens != null) {
			if (tokens[0].equalsIgnoreCase("if") || tokens[0].equalsIgnoreCase("elseif")) {
				if (tokens[0].equalsIgnoreCase("elseif")) {
					if (subifs.size() == 0) {
						System.out.println("ERROR: An elseif condition begins in " + path + ", but there was no prior 'if'.");
						return currentobj;
					} else
						subifs.remove(subifs.size() - 1); // One level closes...
				}
				if (!tokens[1].startsWith("v_arg")) {
					// System.err.println("ERROR: Conditions are only supported with variable names 'v_arg'! " + tokens[1] + " == " + tokens[2] +
					// " is not valid.");
					return currentobj;
				}
				try {
					int argno = Integer.parseInt(tokens[1].substring(5));
					boolean valid = true; // Are outer if-conditions true? If not, we skip the inner ones.
					for (int x = 0; x < subifs.size(); x++)
						if (subifs.get(x) == false)
							valid = false;
					// System.out.println(path + " " + Arrays.toString(tokens) + " --- " + Arrays.toString(vargs));
					if (tokens.length < 4) {
						System.err.println(
								"ERROR: There are only " + tokens.length + " tokens, while at least 4 are needed for this to work:" + Arrays.toString(tokens));
						return currentobj;
					}
					if (vargs == null || vargs.length < argno) {
						if (tokens[3].equalsIgnoreCase("BF2Editor"))
							return currentobj;
						System.out.println("Path: " + path);
						System.out.println("Tokens: " + Arrays.toString(tokens));
						if (vargs == null)
							System.out.println("WARNING: There are null vargs, while at least " + argno + " is needed for this to work:"
									+ Arrays.toString(tokens) + " --- " + Arrays.toString(vargs));
						else {
							System.out.println("Vargs: " + Arrays.toString(vargs));
							System.out.println("WARNING: There are only " + vargs.length + " vargs, while at least " + argno + " are needed for this to work:"
									+ Arrays.toString(tokens) + " --- " + Arrays.toString(vargs));
						}
						valid = false;
						//return currentobj;
					}
					if (valid && tokens[3].equalsIgnoreCase(vargs[argno - 1])) { // Varg equality check
						subifs.add(true); // ... and another level opens true
						while (tokens != null) {
							tokens = prepLine(scanner);
							if (tokens == null) {
								log("WARNING: An if block in " + path
										+ " is being started with 'if' but not being closed with 'endif' until the end of file has been reached!");
								return currentobj;
							}
							if (tokens[0].equalsIgnoreCase("if") || tokens[0].equalsIgnoreCase("elseif") || tokens[0].equalsIgnoreCase("endif"))
								break;
							currentobj = processLine(scanner, tokens, vargs, path, currentobj);
						}
					} else {
						subifs.add(false); // ... and another level opens false
						while (tokens != null) {
							tokens = prepLine(scanner);
							if (tokens[0].equalsIgnoreCase("if") || tokens[0].equalsIgnoreCase("elseif") || tokens[0].equalsIgnoreCase("endif"))
								break;
						}
					}
				} catch (IndexOutOfBoundsException e) {

					System.err.println("Tokens: " + Arrays.toString(tokens));
					System.err.println("Vargs: " + Arrays.toString(vargs));
					e.printStackTrace();
					System.exit(-1);
				}
			} else if (tokens[0].equalsIgnoreCase("endif")) {
				if (subifs.size() == 1)
					return currentobj;
				else {
					subifs.remove(subifs.size() - 1);
					tokens = prepLine(scanner);
				}

			} else
				tokens = prepLine(scanner);
		}
		return currentobj;
	}

	protected static void processInclude(String inclpath, String[] vargs, Path path, BF2Object currentobj) throws IOException {
		File newpath = new File(path.toString().substring(0, path.toString().lastIndexOf(File.separator)) + File.separator + inclpath);
		try {
			processFile(newpath.toPath(), vargs, currentobj);
		} catch (FileNotFoundException e) {
			System.out.println("INFO: The following file was referenced by an 'include' call in " + path + ", but does not exist: " + inclpath);
			// This is not fatal, but usually indicates a typo or sloppiness on behalf of the mod
		}

	}

	static int pint(String in) {
		return Integer.parseInt(in);
	}

	static float pfloat(String in) {
		Float retval = null;
		try {
			retval = Float.parseFloat(in);
		} catch (NumberFormatException e) {
			System.err.println("Malformed float found: " + in + "! Please check ");
			in = in.replaceAll("[^\\d.]", "");
			try {
				retval = Float.parseFloat(in);
			} catch (NumberFormatException e2) {
				e2.printStackTrace();
			}
		}
		return retval;
	}

	static boolean pbool(String in) {
		if (Integer.parseInt(in) == 1)
			return true;
		else
			return false;
	}

}