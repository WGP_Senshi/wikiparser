package bf2objectparser;

import java.io.IOException;
import java.util.Arrays;

import db.GenericFireArm;

public class parseGenericFireArm extends parseINIT {

	public parseGenericFireArm(String name) {
		super(name);
		System.err.println("FATAL: Never ever use constructors of object parsers.");
		System.exit(1);
		// TODO Never ever use this...
	}

	static GenericFireArm parse(String[] tokens, GenericFireArm component) throws IOException {
//		 System.out.println("X: " + Arrays.toString(tokens));
		if (tokens[0].equalsIgnoreCase("ObjectTemplate.weaponHud.hudName")) {
			component.setName(tokens[1]);
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.weaponHud.hasRangeFinder")) {
			if (Integer.valueOf(tokens[1]) == 1)
				component.setRangefinder(true);
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.geometry")) {
			component.setGeometry(tokens[1]);
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.fire.roundsPerMinute")) {
			component.setRPM(Integer.valueOf(tokens[1]));
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.fire.addFireRate")) {
			component.addFirerate(Integer.valueOf(tokens[1]));
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.ammo.nrOfMags")) {
			component.setMagno(Integer.valueOf(tokens[1]));
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.ammo.magSize")) {
			try {
				component.setMagsize(Integer.valueOf(tokens[1]));
			} catch (NumberFormatException e) {
				System.err.println("Should be an integer! " + Arrays.toString(tokens));
				component.setMagsize(0);
			}
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.ammo.reloadTime")) {
			component.setReload(Float.valueOf(tokens[1]));
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.zoom.addZoomFactor")) {
			component.addZoom(Float.valueOf(tokens[1]));
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.projectileTemplate")) {
			component.setProjectilename(tokens[1]);
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.delayToUse")) {
			component.setSetup(Float.valueOf(tokens[1]));
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.velocity")) {
			try {
				component.setMuzzle(Float.valueOf(tokens[1]));
			} catch (NumberFormatException e) {
				System.err.println(component.getCode() + " is fucked up: " + tokens[1] + " should be a float!");
				component.setMuzzle(Float.valueOf(tokens[1].replaceAll("[^\\d.]", "")));
			}
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.addTemplate")) {
			component.addSubTemplate(tokens[1]);
		} else if (tokens[0].equalsIgnoreCase("objecttemplate.itemindex"))
			component.setIndex(Integer.parseInt(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.weaponhud.selecticon"))
			component.setIcon(tokens[1].replace("\\", "/"));

		else if (tokens[0].equalsIgnoreCase("objecttemplate.deviation.minDev"))
			component.setMindev(Float.valueOf(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.deviation.setTurnDev")) {
			float[] arr = new float[tokens.length - 2];
			for (int i = 2; i < tokens.length; i++)
				arr[i - 2] = Float.valueOf(tokens[i]);
			component.setTurndev(arr);
		} else if (tokens[0].equalsIgnoreCase("objecttemplate.deviation.setSpeeddev")) {
			float[] arr = new float[tokens.length - 2];
			for (int i = 2; i < tokens.length; i++)
				arr[i - 2] = Float.valueOf(tokens[i]);
			component.setSpeeddev(arr);
		} else if (tokens[0].equalsIgnoreCase("objecttemplate.deviation.setMiscDev")) {
			float[] arr = new float[tokens.length - 2];
			for (int i = 2; i < tokens.length; i++)
				arr[i - 2] = Float.valueOf(tokens[i]);
			component.setMiscdev(arr);
		} else if (tokens[0].equalsIgnoreCase("objecttemplate.deviation.devModStand"))
			component.setDevmodstand(Float.valueOf(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.deviation.devModCrouch"))
			component.setDevmodcrouch(Float.valueOf(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.deviation.devModLie"))
			component.setDevmodlie(Float.valueOf(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.deviation.devModZoom"))
			component.setDevmodzoom(Float.valueOf(tokens[1]));

		else if (tokens[0].equalsIgnoreCase("objecttemplate.recoil.hasRecoilForce")) {
			if (tokens[1].equalsIgnoreCase("1"))
				component.setHasRecoilForce(true);
			else
				component.setHasRecoilForce(false);
		} else if (tokens[0].equalsIgnoreCase("objecttemplate.recoil.recoilForceUp"))
			component.setRecoilForceUp(Arrays.copyOfRange(tokens, 1, tokens.length));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.recoil.recoilForceLeftRight"))
			component.setRecoilForceLeftRight(Arrays.copyOfRange(tokens, 1, tokens.length));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.recoil.zoomModifier"))
			component.setRecoilZoommodifier(Float.valueOf(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.recoil.goBackOnRecoil"))
			if (tokens[1].equalsIgnoreCase("1"))
				component.setGoBackOnRecoil(true);
			else
				component.setGoBackOnRecoil(false);
		else if (tokens[0].equalsIgnoreCase("objecttemplate.recoil.cameraRecoilSpeed"))
			component.setCameraRecoilSpeed(Float.valueOf(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.recoil.cameraRecoilSize"))
			component.setCameraRecoilSize(Float.valueOf(tokens[1]));

		return component;
	}
}
