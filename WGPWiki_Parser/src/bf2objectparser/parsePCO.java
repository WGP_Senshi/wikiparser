package bf2objectparser;

import java.io.IOException;

import db.PlayerControlObject;

public class parsePCO extends parseINIT {

	public parsePCO(String name) {
		super(name);
		System.err.println("FATAL: Never ever use constructors of object parsers.");
		System.exit(1);
	}

	static PlayerControlObject parse(String[] tokens, PlayerControlObject component) throws IOException {
		if (tokens[0].equalsIgnoreCase("ObjectTemplate.addTemplate"))
			component.addSubTemplate(tokens[1]);
		// Armor
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.castsDynamicShadow"))
			component.setCastsdynamicshadow(pbool(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.maxHitPoints"))
			component.setMaxhitpoints(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.HitPoints"))
			component.setHitpoints(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.defaultMaterial"))
			component.setDefaultmaterial(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.hplostwhileupsidedown"))
			component.setHplostwhileupsidedown(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.hpLostWhileInWater"))
			component.setHplostwhileinwater(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.hpLostWhileInDeepWater"))
			component.setHplostwhileindeepwater(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.hpLostWhileCriticalDamage"))
			component.setHplostwhilecriticaldamage(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.waterDamageDelay"))
			component.setWaterdamagedelay(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.deepwaterdamagedelay"))
			component.setDeepwaterdamagedelay(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.waterlevel"))
			component.setWaterlevel(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.explosionforce"))
			component.setExplosionforce(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.explosionforcemax"))
			component.setExplosionforcemax(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.explosiondamage"))
			component.setExplosiondamage(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.explosionradius"))
			component.setExplosionradius(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.explosionMaterial"))
			component.setExplosionmaterial(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.wreckexplosionforce"))
			component.setExplosionforce(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.wreckexplosionforcemax"))
			component.setExplosionforcemax(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.wreckexplosiondamage"))
			component.setExplosiondamage(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.wreckexplosionradius"))
			component.setExplosionradius(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.wreckExplosionMaterial"))
			component.setExplosionmaterial(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.wreckHitPoints"))
			component.setWreckhitpoints(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.timeToStayAsWreck"))
			component.setTimetostayaswreck(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.armor.criticalDamage"))
			component.setCriticaldamage(pfloat(tokens[1]));

		// VEHICLE HUD
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.vehicleHud.hudName"))
			component.setHudName(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.vehicleHud.typeIcon"))
			component.setTypeicon(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.vehicleHud.miniMapIcon"))
			component.setMinimapicon(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.vehicleHud.spottedIcon"))
			component.setSpottedicon(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.vehicleHud.miniMapIconLeaderSize"))
			component.setMinimapiconleadersize(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.vehicleHud.guiIndex"))
			component.setGuiIndex(pint(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.vehicleHud.vehicleIconPos")) {
			String[] tokens2 = tokens[1].split("/");
			float[] pos = { pfloat(tokens2[0]), pfloat(tokens2[1]) };
			component.setVehicleiconpos(pos);
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.vehicleHud.useSelectionIcons"))
			component.setUseselectionicons(pbool(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.vehicleHud.vehicleIcon"))
			component.setVehicleicon(tokens[1]);

		//Physics
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.drag"))
			component.setDrag(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.mass"))
			component.setMass(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.gravityModifier"))
			component.setGravitymodifier(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.floaterMod"))
			component.setFloaterMod(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.hasMobilePhysics"))
			component.setHasmobilephysics(pbool(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.hasCollisionPhysics"))
			component.setHascollisionphysics(pbool(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.physicsType"))
			component.setPhysicstype(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.exitSpeedMod"))
			component.setExitspeedmod(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setVehicleType"))
			component.setVehicletype(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.listenerObstruction"))
			component.setListenerobstruction(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.groundContactVolume"))
			component.setGroundcontactvolume(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.damagedAmbientSoundLimit"))
			component.setDamagedambientsoundlimit(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.hasRestrictedExit"))
			component.setHasrestrictedexit(pbool(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.sprintRecoverTime"))
			component.setSprintRecoverTime(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.sprintDissipationTime"))
			component.setSprintDissipationTime(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.sprintLimit"))
			component.setSprintLimit(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.sprintFactor"))
			component.setSprintFactor(pfloat(tokens[1]));
		return component;
	}
}
