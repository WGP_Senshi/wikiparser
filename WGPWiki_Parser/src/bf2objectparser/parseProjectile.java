package bf2objectparser;

import java.io.IOException;
import java.util.Arrays;

import db.Projectile;

public class parseProjectile extends parseINIT {

	public parseProjectile(String name) {
		super(name);
		System.err.println("FATAL: Never ever use constructors of object parsers.");
		System.exit(1);
	}

	static Projectile parse(String[] tokens, Projectile component, String[] vargs) throws IOException {
		if (tokens[0].equalsIgnoreCase("objecttemplate.detonation.explosionmaterial")) // Explosive Params
			component.setExpl_mat(Integer.parseInt(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.detonation.explosionradius"))
			component.setExpl_radius(Float.parseFloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.detonation.explosionforce"))
			component.setExpl_force(Float.parseFloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.detonation.explosiondamage"))
			component.setExpl_dmg(Float.parseFloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.detonation.maxdepthforexplosion"))
			component.setExpl_maxdepth(Integer.parseInt(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.gravitymodifier")) // Flight and regular damage physics
			component.setGrav_mod(Float.parseFloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.material"))
			component.setMat(Integer.parseInt(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.timetolive")) {
			if (tokens[1].contains("/")) {
				String[] tokens2 = tokens[1].split("/");
				component.setTTL(Float.parseFloat(tokens2[1]));
			} else
				component.setTTL(Float.parseFloat(tokens[1]));
		} else if (tokens[0].equalsIgnoreCase("objecttemplate.tracertemplate"))
			component.setTracer_name(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("objecttemplate.tracerinterval"))
			if (tokens[1].toLowerCase().startsWith("v_arg")) {
				System.out.println(tokens[1]);
				System.out.println(tokens[1].substring(5));
				System.out.println(Arrays.toString(vargs));
				String varg = "0";
				try {
					varg = vargs[Integer.parseInt(tokens[1].substring(5))-1];
				} catch (NullPointerException e) {
					System.err.println(component.getCode() + "has an unresolved v_arg call...sloppy.");
				}
				component.setTracer_interval(Integer.parseInt(varg));
			} else
				component.setTracer_interval(Integer.parseInt(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.mindamage"))
			component.setDmg_min(Float.parseFloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.damage"))
			component.setDmg(Float.parseFloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.disttostartlosedamage"))
			component.setDisttostartlosedmg(Integer.parseInt(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.disttomindamage"))
			component.setDisttomindmg(Integer.parseInt(tokens[1]));
		return component;

	}

}