package bf2objectparser;

import java.io.IOException;

import db.Kit;

public class parseKit extends parseINIT {

	public parseKit(String name) {
		super(name);
		System.err.println("FATAL: Never ever use constructors of object parsers.");
		System.exit(1);
	}

	static Kit parse(String[] tokens, Kit component) throws IOException {
		// System.out.println("X: " + Arrays.toString(tokens));
		if (tokens[0].equalsIgnoreCase("objecttemplate.abilityinvehiclematerial"))
			component.setAbilityinvehiclematerial(Integer.parseInt(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.abilityinvehiclestrength"))
			component.setAbilityinvehiclestrength(Float.parseFloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.abilityinvehicleradius"))
			component.setAbilityinvehicleradius(Float.parseFloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.abilityrestorerate"))
			component.setAbilityrestorerate(Float.parseFloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("objecttemplate.ability.hashealingability")) {
			if (tokens[1].equals("1"))
				component.setHashealingability(true);
			else
				component.setHashealingability(false);
		} else if (tokens[0].equalsIgnoreCase("objecttemplate.ability.hasrepairingability")) {
			if (tokens[1].equals("1"))
				component.setHasrepairability(true);
			else
				component.setHasrepairability(false);
		} else if (tokens[0].equalsIgnoreCase("objecttemplate.vehiclehud.hudname"))
			component.setHudname(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("objecttemplate.vehiclehud.minimapicon"))
			component.setMinimapicon(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("objecttemplate.vehiclehud.vehicleicon"))
			component.setVehicleicon(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("objecttemplate.addtemplate"))
			component.addSubTemplate(tokens[1]); // Usually these are the kits associated weapons.

		return component;
	}
}
