package bf2objectparser;

import java.io.IOException;
import java.util.Arrays;

import db.Engine;

public class parseEngine extends parseINIT {

	public parseEngine(String name) {
		super(name);
		System.err.println("FATAL: Never ever use constructors of object parsers.");
		System.exit(1);
	}

	static Engine parse(String[] tokens, Engine component) throws IOException {
		try {
			if (tokens[0].equalsIgnoreCase("ObjectTemplate.addTemplate"))
				component.addSubTemplate(tokens[1]);

			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setMinRotation")) {
				String[] subtokens = tokens[1].split("/");
				float[] values = new float[subtokens.length];
				for (int i = 0; i < subtokens.length; i++)
					values[i] = Float.valueOf(subtokens[i]);
				component.setMinRotation(values);
			} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setMaxRotation")) {
				String[] subtokens = tokens[1].split("/");
				float[] values = new float[subtokens.length];
				for (int i = 0; i < subtokens.length; i++)
					values[i] = Float.valueOf(subtokens[i]);
				component.setMaxRotation(values);
			} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setMaxSpeed")) {
				String[] subtokens = tokens[1].split("/");
				float[] values = new float[subtokens.length];
				for (int i = 0; i < subtokens.length; i++)
					values[i] = Float.valueOf(subtokens[i]);
				component.setMaxSpeed(values);
			} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setAcceleration")) {
				String[] subtokens = tokens[1].split("/");
				float[] values = new float[subtokens.length];
				for (int i = 0; i < subtokens.length; i++)
					values[i] = Float.valueOf(subtokens[i]);
				component.setAcceleration(values);
			} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setTorque"))
				component.setTorque(Float.valueOf(tokens[1]));
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setDifferential"))
				component.setDifferential(Float.valueOf(tokens[1]));
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setNumberOfGears"))
				component.setNumberofgears(Integer.valueOf(tokens[1]));
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setGearUp"))
				component.setGearup(Float.valueOf(tokens[1]));
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setGearDown"))
				component.setGeardown(Float.valueOf(tokens[1]));
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setGearRatios")) {
				String[] subtokens = tokens[1].split(" ");
				float[] values = new float[subtokens.length];
				for (int i = 0; i < subtokens.length; i++)
					values[i] = Float.valueOf(subtokens[i]);
				component.setGearratios(values);
			} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setAutomaticReset"))
				if (Float.valueOf(tokens[1]) == 1)
					component.setAutomaticreset(true);
				else
					component.setAutomaticreset(false);
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.snapToZeroOnNoInput"))
				if (Float.valueOf(tokens[1]) == 1)
					component.setSnaptozeroonnoinput(true);
				else
					component.setSnaptozeroonnoinput(false);
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.restoreRotationOnExit"))
				if (Float.valueOf(tokens[1]) == 1)
					component.setRestorerotationonexit(true);
				else
					component.setRestorerotationonexit(false);
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setEngineType"))
				component.setEnginetype(tokens[1]);
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.trackTurnAcceleration"))
				component.setTrackturnacceleration(Float.valueOf(tokens[1]));
			else if (tokens[0].equalsIgnoreCase("ObjectTemplate.trackTurnSpeed"))
				component.setTrackturnspeed(Float.valueOf(tokens[1]));

			// TODO: Add more parser stuff here.
		} catch (ArrayIndexOutOfBoundsException e) {
			System.err.println("ERROR: Tokens: " + Arrays.toString(tokens));
			System.err.println("ERROR: " + component.getCode() + " is missing a value on above step. This might not cause symptoms, but it's very likely a mod bug."); 
		}
		return component;
	}

}
