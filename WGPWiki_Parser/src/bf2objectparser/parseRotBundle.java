package bf2objectparser;

import java.io.IOException;

import db.RotationalBundle;

public class parseRotBundle extends parseINIT {

	public parseRotBundle(String name) {
		super(name);
		System.err.println("FATAL: Never ever use constructors of object parsers.");
		System.exit(1);
	}

	static RotationalBundle parse(String[] tokens, RotationalBundle component) throws IOException {
		if (tokens[0].equalsIgnoreCase("ObjectTemplate.addTemplate"))
			component.addSubTemplate(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.floaterMod"))
			component.setFloaterMod(pfloat(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.hasMobilePhysics"))
			component.setHasMobilePhysics(pbool(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.hasCollisionPhysics"))
			component.setHasCollisionPhysics(pbool(tokens[1]));
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.physicsType"))
			component.setPhysicsType(tokens[1]);
		else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setMinRotation")) {
			String[] subtokens = tokens[1].split("/");
			float[] values = new float[subtokens.length];
			for (int i = 0; i < subtokens.length; i++)
				values[i] = pfloat(subtokens[i]);
			component.setMinRotation(values);
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setMaxRotation")) {
			String[] subtokens = tokens[1].split("/");
			float[] values = new float[subtokens.length];
			for (int i = 0; i < subtokens.length; i++)
				values[i] = pfloat(subtokens[i]);
			component.setMaxRotation(values);
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setMaxSpeed")) {
			String[] subtokens = tokens[1].split("/");
			float[] values = new float[subtokens.length];
			for (int i = 0; i < subtokens.length; i++)
				values[i] = pfloat(subtokens[i]);
			component.setMaxSpeed(values);
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.setAcceleration")) {
			String[] subtokens = tokens[1].split("/");
			float[] values = new float[subtokens.length];
			for (int i = 0; i < subtokens.length; i++)
				values[i] = pfloat(subtokens[i]);
			component.setAcceleration(values);
		} else if (tokens[0].equalsIgnoreCase("ObjectTemplate.rememberExcessInput"))
			component.setRememberExcessInput(pbool(tokens[1]));
		return component;

	}

}
