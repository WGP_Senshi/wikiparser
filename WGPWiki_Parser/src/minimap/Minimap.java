package minimap;

import static java.nio.file.FileVisitResult.CONTINUE;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageInputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.List;

import common.PBar;

public class Minimap extends SimpleFileVisitor<Path> {
	static PBar bar;

    public static void parseSource(File file, List log) {
    	bar = new common.PBar(SWT.INDETERMINATE);
        Minimap mm = new Minimap();
        try {
			Files.walkFileTree(file.toPath(), mm);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			bar.dispose();
		}
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attr) throws IOException {
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:*.{png}");
        PathMatcher matcher2 = FileSystems.getDefault().getPathMatcher("glob:*.{dds}");
        Path name = file.getFileName();
        if (name != null)
            if (matcher.matches(name) || matcher2.matches(name)) {
                String fname = common.Utils.getName_nosuffix(name.toString()).toLowerCase();
                if (fname.equals("ingamemap")) {
                    Path map_name = file.getParent().getParent().getParent().getFileName(); // asad_khal
                	System.out.println(map_name.getFileName() + " --- " + fname);
                	String target = "db\\minimaps\\" + map_name + "_minimap";
                    target += ".png";
                    if (!new File("db\\minimaps\\").exists())
                        new File("db\\minimaps\\").mkdirs();

                    BufferedImage image_dds = null;
                    ddsreader.DDSImageReaderSpi xspi = new ddsreader.DDSImageReaderSpi();
                    ddsreader.DDSImageReader imageReader = new ddsreader.DDSImageReader(xspi);
                    try {
                        imageReader.setInput(new FileImageInputStream(new File(file.toString())));
                        image_dds = imageReader.read(0);
                        // image_dds = ImageIO.read(file);
                    } catch (IOException ex) {
                        System.out.println("Failed to load: " + file);
                    }
                    if (new File(target).exists())
                        new File(target).delete();
                    ImageIO.write(image_dds, "PNG", new File(target));
                    // Files.copy(file, new File(target).toPath(), java.nio.file.StandardCopyOption.REPLACE_EXISTING);
                }
                String[] tokens = fname.split("_");
                if (tokens.length >= 3) {
                    if (tokens[0].equals("mapoverview") && tokens[1].equals("gpm")) {
                        Path map_name = file.getParent().getParent().getFileName(); // asad_khal
                    	bar.setText("Processing " + gui.Main.lang.getValue(map_name.toString(), map_name.toString()));
                    	bar.setSelection(100);
                        String target = "db\\minimaps\\" + map_name + "_" + tokens[1]; // asad_khal_gpm
                        if (tokens[2].equals("cq"))
                            target += "_aas"; // asad_khal_gpm_aas
                        else if (tokens[2].equals("coop"))
                            target += "_coop";
                        else if (tokens[2].equals("insurgency"))
                            target += "_insurgency";
                        else if (tokens[2].equals("skirmish"))
                            target += "_skirmish";
                        else if (tokens[2].equals("vehicles"))
                            target += "_vehicles";
                        else if (tokens[2].equals("cnc"))
                            target += "_cnc";

                        if (tokens[3].equals("16")) {
                            target += "_inf";
                        } else if (tokens[3].equals("32")) {
                            target += "_alt";
                        } else if (tokens[3].equals("64")) {
                            target += "_std";
                        } else if (tokens[3].equals("128")) {
                            target += "_lrg";
                        }

                        target += ".png";
                        if (!new File("db\\minimaps\\").exists())
                            new File("db\\minimaps\\").mkdirs();

                        BufferedImage image_dds = null;
                        ddsreader.DDSImageReaderSpi xspi = new ddsreader.DDSImageReaderSpi();
                        ddsreader.DDSImageReader imageReader = new ddsreader.DDSImageReader(xspi);
                        try {
                            imageReader.setInput(new FileImageInputStream(new File(file.toString())));
                            image_dds = imageReader.read(0);
                            // image_dds = ImageIO.read(file);
                        } catch (IOException ex) {
                            System.out.println("Failed to load: " + file);
                        }
                        if (new File(target).exists())
                            new File(target).delete();
                        ImageIO.write(image_dds, "PNG", new File(target));
                        // Files.copy(file, new File(target).toPath(), java.nio.file.StandardCopyOption.REPLACE_EXISTING);
                    }
                }
            }
        return CONTINUE;
    }

}
