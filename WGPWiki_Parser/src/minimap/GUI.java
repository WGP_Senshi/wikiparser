package minimap;

import gui.Main;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;

import javax.imageio.stream.FileImageInputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import parsers.FlagSpawner;
import db.VehicleSpawner;

public class GUI {

    static Image image;

    public static float[] translatePosition(Rectangle imgsize, int mapsize, float[] value) {
        float scale = (float) imgsize.height / mapsize;
        for (int i = 0; i < value.length; i++) {
            value[i] = value[i] + mapsize / 2; // Corrects BF2 offest that has 0/0 in middle of map instead of edge
            value[i] = value[i] * scale;
        }
        value[2] = imgsize.height - value[2];
        return value;
    }

    public static void init(final String mapname, String gamemode, final int mapsize, java.util.List<VehicleSpawner> vehicles,
            final java.util.List<FlagSpawner> flags) throws IOException {

        final Shell shell = new Shell(gui.Main.mainshell, SWT.TITLE | SWT.APPLICATION_MODAL | SWT.RESIZE | SWT.CLOSE | SWT.MAX);
        final Canvas canvas = new Canvas(shell, SWT.BORDER | SWT.NO_REDRAW_RESIZE | SWT.NO_BACKGROUND | SWT.V_SCROLL | SWT.H_SCROLL);
        Label list_label = new Label(shell, 0);
        list_label.setText("Possible AAS Routes:");
        Button b_save = new Button(shell, 0);
        b_save.setText("Save image");
        b_save.setToolTipText("Saves the currently displayed image to a file.");
        final List list = new List(shell, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
        final java.util.List<AASRoute> routes = determineAASRoutes(flags);

        if (routes != null)
            for (int i = 1; i <= routes.size(); i++) {
                list.add("Route " + i);
            }
        Label b_label = new Label(shell, 0);
        b_label.setText("Show:");
        Button b_flags = new Button(shell, SWT.CHECK);
        b_flags.setText("Flags");
        Button b_radius = new Button(shell, SWT.CHECK);
        b_radius.setText("Radius");
        Button b_value = new Button(shell, SWT.CHECK);
        Label vsep = new Label(shell, SWT.SEPARATOR | SWT.VERTICAL);
        b_value.setText("Value");

        image = loadMinimap(mapname);

        final Rectangle imgsize = image.getBounds();
        GC gc = new GC(image);
        for (FlagSpawner f : flags) {
            float[] posarr = { f.getPosX(), f.getPosY(), f.getPosZ() };
            int radius = f.getRadius();
            posarr = translatePosition(imgsize, mapsize, posarr);

            int realx = (int) posarr[0];
            int realz = (int) posarr[2];
            gc.drawOval(Math.round(realx - ((float) radius / 2)), Math.round(realz - ((float) radius / 2)), radius, radius);
        }
        gc.dispose();

        shell.setText(mapname + " " + gamemode + " Minimap");
        shell.setLayout(new FormLayout());

        FormData vsep_ld = new FormData();
        vsep_ld.top = new FormAttachment(shell, 5, SWT.TOP);
        vsep_ld.left = new FormAttachment(0, 200);
        vsep_ld.bottom = new FormAttachment(100, -5);
        vsep.setLayoutData(vsep_ld);

        FormData b_label_ld = new FormData();
        b_label_ld.top = new FormAttachment(0, 5);
        b_label_ld.left = new FormAttachment(shell, 5, SWT.LEFT);
        b_label.setLayoutData(b_label_ld);

        FormData b_save_ld = new FormData();
        b_save_ld.top = new FormAttachment(0, 5);
        b_save_ld.right = new FormAttachment(vsep, -5, SWT.LEFT);
        b_save.setLayoutData(b_save_ld);

        FormData b_flags_ld = new FormData();
        b_flags_ld.top = new FormAttachment(b_label, 5, SWT.BOTTOM);
        b_flags_ld.left = new FormAttachment(shell, 5, SWT.LEFT);
        b_flags.setLayoutData(b_flags_ld);

        FormData b_radius_ld = new FormData();
        b_radius_ld.top = new FormAttachment(b_flags, 5, SWT.BOTTOM);
        b_radius_ld.left = new FormAttachment(shell, 5, SWT.LEFT);
        b_radius.setLayoutData(b_radius_ld);

        FormData b_value_ld = new FormData();
        b_value_ld.top = new FormAttachment(b_radius, 5, SWT.BOTTOM);
        b_value_ld.left = new FormAttachment(shell, 5, SWT.LEFT);
        b_value.setLayoutData(b_value_ld);

        FormData list_label_ld = new FormData();
        list_label_ld.top = new FormAttachment(b_value, 5, SWT.BOTTOM);
        list_label_ld.left = new FormAttachment(shell, 5, SWT.LEFT);
        list_label.setLayoutData(list_label_ld);

        FormData list_ld = new FormData();
        list_ld.top = new FormAttachment(list_label, 5, SWT.BOTTOM);
        list_ld.left = new FormAttachment(shell, 5);
        list_ld.right = new FormAttachment(vsep, -5);
        list_ld.bottom = new FormAttachment(100, -5);
        list.setLayoutData(list_ld);

        FormData canvas_ld = new FormData(512, 512);
        canvas_ld.top = new FormAttachment(shell, 5, SWT.TOP);
        canvas_ld.left = new FormAttachment(vsep, 5, SWT.RIGHT);
        canvas_ld.right = new FormAttachment(100, -5);
        canvas_ld.bottom = new FormAttachment(100, -5);
        canvas.setLayoutData(canvas_ld);

        final Point origin = new Point(0, 0);
        final ScrollBar hBar = canvas.getHorizontalBar();
        hBar.addListener(SWT.Selection, new Listener() {
            @Override
			public void handleEvent(Event e) {
                int hSelection = hBar.getSelection();
                int destX = -hSelection - origin.x;
                Rectangle rect = image.getBounds();
                canvas.scroll(destX, 0, 0, 0, rect.width, rect.height, false);
                origin.x = -hSelection;
            }
        });
        final ScrollBar vBar = canvas.getVerticalBar();
        vBar.addListener(SWT.Selection, new Listener() {
            @Override
			public void handleEvent(Event e) {
                int vSelection = vBar.getSelection();
                int destY = -vSelection - origin.y;
                Rectangle rect = image.getBounds();
                canvas.scroll(0, destY, 0, 0, rect.width, rect.height, false);
                origin.y = -vSelection;
            }
        });

        canvas.addListener(SWT.Resize, new Listener() {
            @Override
			public void handleEvent(Event e) {
                Rectangle rect = image.getBounds();
                Rectangle client = canvas.getClientArea();
                int hbar_size = hBar.getSize().y + 4;
                int vbar_size = vBar.getSize().x + 4;
                hBar.setMaximum(rect.width);
                vBar.setMaximum(rect.height);
                hBar.setThumb(Math.min(rect.width, client.width));
                vBar.setThumb(Math.min(rect.height, client.height));
                int hPage = rect.width - client.width;
                int vPage = rect.height - client.height;
                int hSelection = hBar.getSelection();
                int vSelection = vBar.getSelection();
                if (hSelection >= hPage) {
                    if (hPage <= 0)
                        hSelection = 0;
                    origin.x = -hSelection;
                }
                if (vSelection >= vPage) {
                    if (vPage <= 0)
                        vSelection = 0;
                    origin.y = -vSelection;
                }
                if (client.width > rect.width) {
                    canvas.setSize(rect.width + hbar_size, client.height + vbar_size);
                }
                if (client.height > rect.height) {
                    canvas.setSize(client.width, rect.height + vbar_size);
                }
                canvas.redraw();
            }
        });

        canvas.addListener(SWT.Paint, new Listener() {
            @Override
			public void handleEvent(Event e) {
                GC gc = e.gc;
                gc.drawImage(image, origin.x, origin.y);
                /*
                 * Rectangle rect = image.getBounds(); Rectangle client = canvas.getClientArea(); int marginWidth = client.width - rect.width; if (marginWidth >
                 * 0) { gc.fillRectangle(rect.width, 0, marginWidth, client.height); } int marginHeight = client.height - rect.height; if (marginHeight > 0) {
                 * gc.fillRectangle(0, rect.height, client.width, marginHeight); }
                 */
            }
        });

        b_save.addListener(SWT.Selection, new Listener() {
            @Override
			public void handleEvent(Event event) {
                ImageLoader loader = new ImageLoader();
                loader.data = new ImageData[] { image.getImageData() };
                String save_file = openSaveDialog(shell);
                if (save_file == null)
                    return;
                if (save_file.endsWith(".png"))
                    loader.save(save_file, SWT.IMAGE_PNG);
                else if (save_file.endsWith(".jpg"))
                    loader.save(save_file, SWT.IMAGE_JPEG);

            }

        });

        list.addListener(SWT.Selection, new Listener() {
            @Override
			public void handleEvent(Event e) {
                image = loadMinimap(mapname);
                int[] listSelection = list.getSelectionIndices();

                java.util.List<FlagSpawner> flag1 = new ArrayList<FlagSpawner>();
                java.util.List<FlagSpawner> flag2 = new ArrayList<FlagSpawner>();

                if (listSelection.length == 1) {
                    for (int i = 0; i < routes.get(listSelection[0]).links.size(); i++) {
                        flag1.add(flags.get(routes.get(listSelection[0]).links.get(i)[0]));
                        flag2.add(flags.get(routes.get(listSelection[0]).links.get(i)[1]));
                    }
                } else {
                    for (int j = 0; j < listSelection.length; j++) {
                        for (int i = 0; i < routes.get(listSelection[j]).links.size(); i++) {
                            flag1.add(flags.get(routes.get(listSelection[j]).links.get(i)[0]));
                            flag2.add(flags.get(routes.get(listSelection[j]).links.get(i)[1]));
                        }
                    }
                }
                GC gc = new GC(image);
                java.util.List<String> done = new ArrayList<String>();
                for (int i = 0; i < flag1.size(); i++) {
                    FlagSpawner f1 = flag1.get(i);
                    FlagSpawner f2 = flag2.get(i);

                    boolean done1 = false;
                    boolean done2 = false;
                    if (done.contains(f1.getName()))
                        done1 = true;
                    else
                        done.add(f1.getName());

                    if (done.contains(f2.getName()))
                        done2 = true;
                    else
                        done.add(f2.getName());

                    float[] posarr1 = { f1.getPosX(), f1.getPosY(), f1.getPosZ() };
                    float[] posarr2 = { f2.getPosX(), f2.getPosY(), f2.getPosZ() };
                    int radius1 = f1.getRadius();

                    int radius2 = f2.getRadius();
                    posarr1 = translatePosition(imgsize, mapsize, posarr1);
                    posarr2 = translatePosition(imgsize, mapsize, posarr2);

                    int realx1 = (int) posarr1[0];
                    int realz1 = (int) posarr1[2];
                    int realx2 = (int) posarr2[0];
                    int realz2 = (int) posarr2[2];
                    gc.setForeground(Main.display.getSystemColor(SWT.COLOR_RED));
                    Font font = new Font(Main.display, "Arial", 12, SWT.BOLD);
                    gc.setAntialias(SWT.ON);
                    if (f1.getSGID() == 0 && !done1) {
                        gc.drawOval(Math.round(realx1 - ((float) radius1 / 2)), Math.round(realz1 - ((float) radius1 / 2)), 15, 15);
                        gc.setForeground(Main.display.getSystemColor(SWT.COLOR_BLACK));
                        gc.setFont(font);
                        //gc.drawText(String.valueOf(f1.getSGID()), realx1 + 5, realz1, true);
                        gc.drawText(Main.lang.getValue(f1.getName()), realx1 + 5, realz1, true);
                    }
                    if (f2.getSGID() == 0 && !done2) {
                        gc.drawOval(Math.round(realx2 - ((float) radius2 / 2)), Math.round(realz2 - ((float) radius2 / 2)), 15, 15);
                        gc.setForeground(Main.display.getSystemColor(SWT.COLOR_BLACK));
                        gc.setFont(font);
                        // gc.drawText(String.valueOf(f2.getSGID()), realx2 + 5, realz2, true);
                        gc.drawText(Main.lang.getValue(f2.getName()), realx2 + 5, realz2, true);
                    }
                    if (f1.getSGID() != 0 && f2.getSGID() != 0) {
                        if (!done1)
                            gc.drawOval(Math.round(realx1 - ((float) radius1 / 2)), Math.round(realz1 - ((float) radius1 / 2)), radius1, radius1);
                        if (!done2)
                            gc.drawOval(Math.round(realx2 - ((float) radius2 / 2)), Math.round(realz2 - ((float) radius2 / 2)), radius2, radius2);
                        // gc.setLineWidth(2);
                        gc.drawLine(realx1, realz1, realx2, realz2);
                        gc.setForeground(Main.display.getSystemColor(SWT.COLOR_BLACK));
                        gc.setFont(font);
                        if (!done1)
                            // gc.drawText(String.valueOf(f1.getSGID()), realx1 + 5, realz1, true);
                        	gc.drawText(Main.lang.getValue(f1.getName()), realx1 + 5, realz1, true);
                        if (!done2)
                            // gc.drawText(String.valueOf(f2.getSGID()), realx2 + 5, realz2, true);
                        	gc.drawText(Main.lang.getValue(f2.getName()), realx2 + 5, realz2, true);

                    }

                }
                gc.dispose();
                canvas.redraw();
            }

        });
        // canvas.addPaintListener(new PaintListener() {
        // public void paintControl(PaintEvent e) {
        // e.gc.drawImage(image, 0, 0);
        // Rectangle bounds = image.getBounds();
        // e.gc.drawLine(0,0,bounds.width, bounds.height);
        // for (FlagSpawner f : flags)
        // e.gc.drawOval((int) f.getPosX(), (int) f.getPosZ(), 20, 20);
        // }
        // });

        shell.pack();
        shell.open();
        // shell.setSize(shell.computeSize(SWT.DEFAULT, 640, true));

        shell.addDisposeListener(new DisposeListener() {

            @Override
            public void widgetDisposed(DisposeEvent e) {
                //Main.b_minimap.setEnabled(true);

            }
        });
    }

    protected static String openSaveDialog(Shell shell) {
        final FileDialog dialog_save = new FileDialog(shell, SWT.SAVE);

        if (Main.props.getProperty("ImagePath") != null)
            dialog_save.setFilterPath(Main.props.getProperty("ImagePath"));

        dialog_save.setFilterExtensions(new String[] { "*.png", "*.jpg" });
        dialog_save.setOverwrite(true);
        String dir_chosen = dialog_save.open();
        if (dir_chosen != null) {
            Main.props.setProperty("ImagePath", dir_chosen);
            Main.saveSettings();
        }
        return dir_chosen;
    }

    /**
     * Figures out what AAS routes exist (if any) and returns them as an arrlist.
     * 
     * @return
     */
    private static java.util.List<AASRoute> determineAASRoutes(java.util.List<FlagSpawner> flags) {
        java.util.List<AASRoute> routes = new ArrayList<AASRoute>();
        java.util.List<Integer> sgids = new ArrayList<Integer>();
        /*
         * This segment figures out how many AAS Routes exist
         */
        int noroutes;
        for (FlagSpawner f : flags)
            if (String.valueOf(f.getSGID()).length() == 2) { // If SGID only has two numbers, e.g. on Op Barracuda
                if (String.valueOf(f.getSGID()).charAt(String.valueOf(f.getSGID()).length() - 1) != 0) // Only flags where the second digit is >0 are important
                    sgids.add(f.getSGID());
                sgids.add(Integer.parseInt(String.valueOf(String.valueOf(f.getSGID()).charAt(String.valueOf(f.getSGID()).length() - 1))));

            } else if (String.valueOf(f.getSGID()).length() == 3)
                sgids.add(Integer.parseInt(String.valueOf(String.valueOf(f.getSGID()).charAt(String.valueOf(f.getSGID()).length() - 1))));

        if (sgids.size() > 0)
            noroutes = Collections.max(sgids);
        else
            noroutes = 1; // If every SGID is only single-digit, there is only one possible route
        for (int i = 0; i < noroutes; i++) {
            routes.add(new AASRoute(flags, i));
        }
        return routes;
    }

    private static Image loadMinimap(String mapname) {
        String minimappath = Main.props.getProperty("MapPath") + "\\" + mapname + "\\Hud\\Minimap\\";
        if (new File(minimappath + "ingameMap.dds").exists())
            minimappath = minimappath + "ingameMap.dds";
        else if (new File(minimappath + "ingameMap.bmp").exists())
            minimappath = minimappath + "ingameMap.bmp";
        else if (new File(minimappath + "ingameMap.tif").exists())
            minimappath = minimappath + "ingameMap.tif";
        else if (new File(minimappath + "ingameMap.jpg").exists())
            minimappath = minimappath + "ingameMap.jpg";
        else if (new File(minimappath + "ingameMap.png").exists())
            minimappath = minimappath + "ingameMap.png";
        else if (new File(minimappath + "ingameMap.gif").exists())
            minimappath = minimappath + "ingameMap.gif";
        // final Image image = new Image(parsers.chooser.display, minimappath);

        File file = new File(minimappath);
        BufferedImage image_dds = null;
        ddsreader.DDSImageReaderSpi xspi = new ddsreader.DDSImageReaderSpi();
        ddsreader.DDSImageReader imageReader = new ddsreader.DDSImageReader(xspi);
        try {
            imageReader.setInput(new FileImageInputStream(file));
            image_dds = imageReader.read(0);
            // image_dds = ImageIO.read(file);
        } catch (IOException ex) {
            System.out.println("Failed to load: " + file.getName());
        }

        return new Image(Main.display, SWTUtils.convertToSWT(image_dds));
    }

    public static void openDirectorydialog(Shell shell, Text dir) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException,
            IOException, InterruptedException {
        final DirectoryDialog dialog_parsedir = new DirectoryDialog(shell);
        dialog_parsedir.setMessage("Pick the folder where your level files");
        String dir_chosen = dialog_parsedir.open();
        if (dir_chosen != null) {
            dir.setText(dir_chosen);
        }
    }
}
