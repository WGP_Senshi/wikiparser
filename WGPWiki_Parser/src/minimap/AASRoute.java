package minimap;

import java.util.ArrayList;
import java.util.List;

import parsers.FlagSpawner;

public class AASRoute {

    public List<int[]> links; // Two-length-array

    /**
     * Determines which flags belong to the specified routeindex and contains a list of indices (int).
     * Single-digit SGID: Single-link according flag number. If more than one flag carry the same number, all of them must be captured before the higher/lower flag number can be capped.
     * Two-digit: First number indicates order (as single-digit), second one determines - if multiple same first-digits exist - how many should be randomly selected 
     *     E.g. 3 flags start with "3", but all of them have "1" as the second number. Only one will get picked.
     * Three-digit: Full-fledged AASv4 gamemode: http://www.realitymod.com/forum/f189-modding-tutorials/93932-aasv4-game-mode.html
     *     First digit: Sequence order (same as single-digit)
     *     Second digit: Same as two-digit, how many to randomize
     *     Third digit: Specifies the route it belongs to. When the map loads, only one route will be loaded.
     * 
     * Additional rules apply (making parsing much more difficult...):
     *      Flag types can be mixed
     *      Main bases SHOULD carry the numbers 1 and -1, but sometimes they also have 0 and 1. And sometimes there aren't even any mainbases at all.
     *          
     *     */
    public AASRoute(List<FlagSpawner> flags, int routeno) {
        this.links = new ArrayList<int[]>();
        routeno = routeno + 1;
        for (int j = 0; j < flags.size(); j++) {
            if (String.valueOf(flags.get(j).getSGID()).length() == 1) { // Search first flag if there's only one digit
                int start = flags.get(j).getSGID();
                for (int k = 0; k < flags.size(); k++) { // Search following flag
                    int plusone = flags.get(k).getSGID();
                    if (plusone == start + 1) {
                        System.out.println("Link SGIDs: " + start + " " + plusone);
                        int[] retval = { j, k };
                        links.add(retval);
                    }

                }
            } else if (String.valueOf(flags.get(j).getSGID()).length() == 3) // AASv4, three digits 
                if (Integer.parseInt(String.valueOf(flags.get(j).getSGID()).substring(2)) != routeno) // retrieves last number
                    continue;
            int start = Integer.parseInt(String.valueOf(flags.get(j).getSGID()).substring(0, 1)); // retrieves first number
            for (int k = 0; k < flags.size(); k++) { // Search following flag
                if (String.valueOf(flags.get(k).getSGID()).length() == 1) { // True for simple AAS with single number
                    if (Integer.parseInt(String.valueOf(flags.get(k).getSGID())) != routeno)
                        continue;
                } else if (String.valueOf(flags.get(k).getSGID()).length() == 3)
                    if (Integer.parseInt(String.valueOf(flags.get(k).getSGID()).substring(2)) != routeno) // retrieves last number
                        continue;
                int plusone = Integer.parseInt(String.valueOf(flags.get(k).getSGID()).substring(0, 1));
                if (plusone == start + 1) { // If routenumber actually increases by 1

                    System.out.println("Link SGIDs: " + flags.get(j).getSGID() + "  " + flags.get(k).getSGID());
                    // if (flags.get(j).getSGID() == 0 || flags.get(k).getSGID() == 0)
                    // continue;
                    int[] retval = { j, k };
                    links.add(retval);
                }
            }
        }
    }
}
